/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import org.shadanakar.eve.emt.helpers.Constants;

import java.util.Date;

public class EveIndustryJob {
    private final long id;
    private final long assemblyLineId;
    private final long containerId;
    private final EveType installedItemType;
    private final boolean installedItemCopy;
    private final LocalItemInfo installedLocalItemInfo;
//    private final EveLocation installedItemLocation;
    private final int installedItemLicensedProductionRunsRemaining;
    private final long outputLocationId;
    private final long installerId;
    private final int jobRuns;
    private final Integer licensedProductionRuns;
    private final EveType outputType;
    private final LocalBlueprintInfo outputBlueprintInfo;
    private final int activityId;
    private final EveType containerType;
    private final boolean completed;
    private final int completedStatus;
    private final boolean completedSuccessfully;
    private final Date installTime;
    private final Date beginProductionTime;
    private final Date endProductionTime;
    private final Date pauseProductionTime;

    // This is longest line of the code I have ever written.
    public EveIndustryJob(long id, long assemblyLineId, long containerId, EveType installedItemType, boolean installedItemCopy,
                          LocalItemInfo installedLocalItemInfo, /* EveLocation installedItemLocation, */int installedItemLicensedProductionRunsRemaining,
                          long outputLocationId, long installerId, int jobRuns, Integer licensedProductionRuns, EveType outputType,
                          LocalBlueprintInfo outputBlueprintInfo, int activityId, EveType containerType, boolean completed,
                          int completedStatus, boolean completedSuccessfully, Date installTime, Date beginProductionTime,
                          Date endProductionTime, Date pauseProductionTime) {
        this.id = id;
        this.assemblyLineId = assemblyLineId;
        this.containerId = containerId;
        this.installedItemType = installedItemType;
        this.installedItemCopy = installedItemCopy;
        this.installedLocalItemInfo = installedLocalItemInfo;
//        this.installedItemLocation = installedItemLocation;
        this.installedItemLicensedProductionRunsRemaining = installedItemLicensedProductionRunsRemaining;
        this.outputLocationId = outputLocationId;
        this.installerId = installerId;
        this.jobRuns = jobRuns;
        this.licensedProductionRuns = licensedProductionRuns;
        this.outputType = outputType;
        this.outputBlueprintInfo = outputBlueprintInfo;
        this.activityId = activityId;
        this.containerType = containerType;
        this.completed = completed;
        this.completedStatus = completedStatus;
        this.completedSuccessfully = completedSuccessfully;
        this.installTime = installTime;
        this.beginProductionTime = beginProductionTime;
        this.endProductionTime = endProductionTime;
        this.pauseProductionTime = pauseProductionTime;
    }

    public long getId() {
        return id;
    }

    public long getAssemblyLineId() {
        return assemblyLineId;
    }

    public long getContainerId() {
        return containerId;
    }

    public EveType getInstalledItemType() {
        return installedItemType;
    }

    public boolean isInstalledItemCopy() {
        return installedItemCopy;
    }

    public LocalItemInfo getInstalledLocalItemInfo() {
        return installedLocalItemInfo;
    }

//    public EveLocation getInstalledItemLocation() {
//        return installedItemLocation;
//    }

    public int getInstalledItemLicensedProductionRunsRemaining() {
        return installedItemLicensedProductionRunsRemaining;
    }

    public long getOutputLocationId() {
        return outputLocationId;
    }

    public long getInstallerId() {
        return installerId;
    }

    public int getJobRuns() {
        return jobRuns;
    }

    public Integer getLicensedProductionRuns() {
        return licensedProductionRuns;
    }

    public EveType getOutputType() {
        return outputType;
    }

    public LocalBlueprintInfo getOutputBlueprintInfo() {
        return outputBlueprintInfo;
    }

    public int getActivityId() {
        return activityId;
    }

    public EveType getContainerType() {
        return containerType;
    }

    public boolean isCompleted() {
        return completed;
    }

    public int getCompletedStatus() {
        return completedStatus;
    }

    public boolean isCompletedSuccessfully() {
        return completedSuccessfully;
    }

    public Date getInstallTime() {
        return installTime;
    }

    public Date getBeginProductionTime() {
        return beginProductionTime;
    }

    public Date getEndProductionTime() {
        return endProductionTime;
    }

    public Date getPauseProductionTime() {
        return pauseProductionTime;
    }

    public boolean isDelivered() {
        return completed;
    }

    public ActivityType getActivityType() {
        switch (activityId) {
            case Constants.ACTIVITYID_INVENTION:
                return ActivityType.INVENTION;
            case Constants.ACTIVITYID_MANUFACTURING:
                return ActivityType.MANUFACTURING;
            default:
                return null;
        }
    }
}
