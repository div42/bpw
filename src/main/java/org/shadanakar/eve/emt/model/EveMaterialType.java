/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class EveMaterialType {
    private int productTypeId;
    private EveType materialType;
    private float quantity;
    private float damagePerJob;
    private boolean recycle;

    public EveMaterialType(int productTypeId, EveType materialType, float quantity, float damagePerJob, boolean recycle) {
        this.productTypeId = productTypeId;
        this.materialType = materialType;
        this.quantity = quantity;
        this.damagePerJob = damagePerJob;
        this.recycle = recycle;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public EveType getMaterialType() {
        return materialType;
    }

    public float getQuantity() {
        return quantity;
    }

    public float getDamagePerJob() {
        return damagePerJob;
    }

    public boolean isRecycle() {
        return recycle;
    }

    public EveMaterialType setQuantity(float newQuantity) {
        return new EveMaterialType(productTypeId, materialType, newQuantity, damagePerJob, recycle);
    }
}
