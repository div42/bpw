/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class LocalItemInfo {
    private long id;
    private boolean expanded;
    private Integer materialLevel;
    private Integer productivityLevel;
    private boolean containerIgnored;
    private String customName;

    public LocalItemInfo(long id) {
        this.id = id;
        this.expanded = false;
        this.materialLevel = null;
        this.productivityLevel = null;
        this.customName = null;
    }

    /**
     * Copy constructor.
     */
    public LocalItemInfo(LocalItemInfo that) {
        this.id = that.id;
        this.expanded = that.expanded;
        this.materialLevel = that.materialLevel;
        this.productivityLevel = that.productivityLevel;
        this.containerIgnored = that.containerIgnored;
        this.customName = that.customName;
    }

    public long getId() {
        return id;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public Integer getMaterialLevel() {
        return materialLevel;
    }

    public void setMaterialLevel(Integer materialLevel) {
        this.materialLevel = materialLevel;
    }

    public Integer getProductivityLevel() {
        return productivityLevel;
    }

    public void setProductivityLevel(Integer productivityLevel) {
        this.productivityLevel = productivityLevel;
    }

    public String dump() {
        return "[id=" + id + ", me=" + materialLevel + ", pe=" + productivityLevel +
                ", expanded=" + expanded + ", containerIgnored=" + containerIgnored + ", customName=" + customName + "]";
    }

    public boolean isContainerIgnored() {
        return containerIgnored;
    }

    public void setContainerIgnored(boolean containerIgnored) {
        this.containerIgnored = containerIgnored;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }
}
