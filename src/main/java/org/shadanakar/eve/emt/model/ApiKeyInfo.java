/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class ApiKeyInfo {
    private String key;
    private String vCode;

    public ApiKeyInfo(String key, String vCode) {
        this.key = key;
        this.vCode = vCode;
    }

    public String getKey() {
        return key;
    }

    public String getvCode() {
        return vCode;
    }

    @Override
    public int hashCode() {
        return key.hashCode()*37 + vCode.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != ApiKeyInfo.class) return false;
        ApiKeyInfo that = (ApiKeyInfo) obj;
        return this.key.equals(that.key) && this.vCode.equals(that.vCode);
    }

    @Override
    public String toString() {
        return "[key=" + key + ", vCode="+vCode+"]";
    }
}
