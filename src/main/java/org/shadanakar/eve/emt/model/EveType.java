/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

/**
 * $Id$
 */
public class EveType {
    private final int id;
    private final EveGroup group;
    private final String name;
    private final Double mass;
    private final Double volume;
    private final Integer portionSize;
    private final EveBlueprint blueprint;
    private final Integer marketGroupId;
    private final int metaLevel;

    public EveType(int id, EveGroup group, String name, Double mass, Double volume, Integer portionSize, EveBlueprint blueprint, Integer marketGroupId, int metaLevel) {
        this.id = id;
        this.group = group;
        this.name = name;
        this.mass = mass;
        this.volume = volume;
        this.portionSize = portionSize;
        this.blueprint = blueprint;
        this.marketGroupId = marketGroupId;
        this.metaLevel = metaLevel;
    }

    public String getName() {
        return name;
    }

    public EveGroup getGroup() {
        return group;
    }

    public Integer getId() {
        return id;
    }

    public Double getMass() {
        return mass;
    }

    public Double getVolume() {
        return volume;
    }

    public EveBlueprint getBlueprint() {
        return blueprint;
    }

    public boolean isBlueprint() {
        return group.isBlueprint();
    }

    public Integer getPortionSize() {
        return portionSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EveType eveType = (EveType) o;

        return id == eveType.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    public Integer getMarketGroupId() {
        return marketGroupId;
    }

    @Override
    public String toString() {
        return "EveType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mass=" + mass +
                ", volume=" + volume +
                ", portionSize=" + portionSize +
                ", marketGroupId=" + marketGroupId +
                ", metaLevel=" + metaLevel +
                '}';
    }

    public int getMetaLevel() {
        return metaLevel;
    }
}
