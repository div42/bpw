/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class EveDecryptorInfo {
    private final EveType decryptorType;
    private final float probabilityMultiplier;
    private final float meModifier;
    private final float peModifier;
    private final float maxRunModifier;

    public EveDecryptorInfo(EveType decryptorType, float probabilityMultiplier, float meModifier, float peModifier, float maxRunModifier) {
        this.decryptorType = decryptorType;
        this.probabilityMultiplier = probabilityMultiplier;
        this.meModifier = meModifier;
        this.peModifier = peModifier;
        this.maxRunModifier = maxRunModifier;
    }

    public EveType getDecryptorType() {
        return decryptorType;
    }

    public float getProbabilityMultiplier() {
        return probabilityMultiplier;
    }

    public float getMeModifier() {
        return meModifier;
    }

    public float getPeModifier() {
        return peModifier;
    }

    public float getMaxRunModifier() {
        return maxRunModifier;
    }
}
