/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class JobMaterialModel {
    private EveType eveType;
    private float price;
    private float quantityRequired;
    private int quantityPresent;
    private int quantityToManufacture;
    private float quantityInProduction;

    public JobMaterialModel(EveType eveType, float price, float quantityRequired, int quantityPresent, int quantityToManufacture, float quantityInProduction) {
        this.eveType = eveType;
        this.price = price;
        this.quantityRequired = quantityRequired;
        this.quantityPresent = quantityPresent;
        this.quantityToManufacture = quantityToManufacture;
        this.quantityInProduction = quantityInProduction;
    }

    public EveType getEveType() {
        return eveType;
    }

    public float getQuantityRequired() {
        return quantityRequired;
    }

    public float getVolumeRequired() {
        return quantityRequired * ((float) eveType.getVolume().doubleValue());
    }

    public int getQuantityPresent() {
        return quantityPresent;
    }

    public float getQuantityMissing() {
        return Math.max(0f, quantityRequired - (quantityInProduction + quantityPresent + quantityToManufacture));
    }

    public float getVolumeMissing() {
        return Math.max(0f, (quantityRequired - (quantityInProduction + quantityPresent + quantityToManufacture))*(float)eveType.getVolume().doubleValue());
    }

    public int getQuantityToManufacture() {
        return quantityToManufacture;
    }

    public float getQuantityInProduction() {
        return quantityInProduction;
    }

    public float getPrice() {
        return price;
    }
}
