/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class EveBlueprint {
    private final Integer id;
    private final EveType productType;
    private final Integer productionTime;
    private final Integer techLevel;
    private final Integer researchCopyTime;
    private final Integer researchTechTime;
    private final Integer productivityModifier;
    private final Integer materialModifier;
    private final Integer wasteFactor;
    private final Integer maxProductionLimit;
    private final float chanceOfInvention;

    public EveBlueprint(Integer id, EveType productType, Integer productionTime, Integer techLevel, Integer researchCopyTime, Integer researchTechTime, Integer productivityModifier, Integer materialModifier, Integer wasteFactor, Integer maxProductionLimit, float chanceOfInvention) {
        this.id = id;
        this.productType = productType;
        this.productionTime = productionTime;
        this.techLevel = techLevel;
        this.researchCopyTime = researchCopyTime;
        this.researchTechTime = researchTechTime;
        this.productivityModifier = productivityModifier;
        this.materialModifier = materialModifier;
        this.wasteFactor = wasteFactor;
        this.maxProductionLimit = maxProductionLimit;
        this.chanceOfInvention = chanceOfInvention;
    }

    public Integer getId() {
        return id;
    }

    public EveType getProductType() {
        return productType;
    }

    public Integer getProductionTime() {
        return productionTime;
    }

    public Integer getTechLevel() {
        return techLevel;
    }

    public Integer getProductivityModifier() {
        return productivityModifier;
    }

    public Integer getMaterialModifier() {
        return materialModifier;
    }

    public Integer getWasteFactor() {
        return wasteFactor;
    }

    public Integer getMaxProductionLimit() {
        return maxProductionLimit;
    }

    public float getResearchTechTime() {
        return researchTechTime;
    }

    public float getChanceOfInvention() {
        return chanceOfInvention;
    }

    public Integer getResearchCopyTime() {
        return researchCopyTime;
    }
}
