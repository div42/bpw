/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import javax.annotation.Nonnull;
import java.util.*;

/**
 * $Id$
 */
public class EveAssets {
    private Date currentTime;
    private List<EveItemNode> nodes;
    private Map<String, EveItemNode> nodesMap;
    private Map<Long, EveItemNode> nodesByItemIdMap;
    private Date cachedUntil;


    public EveAssets(Date currentTime, Date cachedUntil, List<EveItemNode> nodes, Map<String, EveItemNode> nodesMap) {
        this.currentTime = currentTime;
        this.cachedUntil = cachedUntil;
        this.nodes = nodes;
        this.nodesMap = nodesMap;
        this.nodesByItemIdMap = new HashMap<>();
        for (EveItemNode node : nodesMap.values()) {
            for (EveItemNode.Entry entry : node.getItemInfoList()) {
                nodesByItemIdMap.put(entry.getId(), node);
            }
        }
    }

    public List<EveItemNode> getNodes() {
        return Collections.unmodifiableList(nodes);
    }

    public EveItemNode getNodeById(String id) {
        return nodesMap.get(id);
    }

    public Date getCurrentTime() {
        return currentTime;
    }

    @Nonnull
    public Date getCachedUntil() {
        return cachedUntil == null ? new Date(0) : cachedUntil;
    }

    public List<EveItemNode> findItemsOfSameType(final EveItemNode node, final LocalBlueprintInfo localBlueprintInfo) {
        return Lists.newArrayList(Iterables.filter(nodesMap.values(), new Predicate<EveItemNode>() {
            @Override
            public boolean apply(@Nullable EveItemNode entry) {
                return entry != null &&
                        (entry.getTypeId().equals(node.getTypeId())) &&
                        (entry.isBlueprintCopy()) &&
                        (entry.calculateMaterialLevel(localBlueprintInfo).getValue() == node.calculateMaterialLevel(localBlueprintInfo).getValue()) &&
                        (entry.calculateProductivityLevel(localBlueprintInfo).getValue() == node.calculateProductivityLevel(localBlueprintInfo).getValue()) &&
                        (entry.calculateRuns(localBlueprintInfo) == node.calculateRuns(localBlueprintInfo));
            }
        }));
    }

    public int getTotalQuantityByType(Integer typeId) {
        int totalQuantity = 0;
        for (EveItemNode item : nodesMap.values()) {
            if (item.getType().getId().equals(typeId)) {
                totalQuantity += item.getQuantity();
            }
        }

        return totalQuantity;
    }

    public Set<EveType> findAllBlueprintTypes() {
        Set<EveType> types = new HashSet<>();
        for (EveItemNode eveItemNode : nodesMap.values()) {
            if (eveItemNode.isBlueprint()) {
                types.add(eveItemNode.getType());
            }
        }

        return types;
    }

    public EveItemNode getNodeByItemId(long itemId) {
        return nodesByItemIdMap.get(itemId);
    }

    public long secondsToNextUpdate(Date now) {
        return cachedUntil == null ? (-now.getTime()) : (cachedUntil.getTime() - now.getTime()) / 1000;
    }
}
