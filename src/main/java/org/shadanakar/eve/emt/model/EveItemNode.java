/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import org.apache.commons.lang.StringUtils;
import org.shadanakar.eve.emt.helpers.BlueprintHelper;

import java.util.*;

/**
 * $Id$
 */
public class EveItemNode {
    private final String id; // todo alternative: use min(itemInfoList.ids) of type Long
    private final EveType type;
    private final EveLocation location;
    private final int quantity;
    private final Integer rawQuantity;
    private final List<Entry> itemInfoList;
    private final List<EveItemNode> children;
    private boolean container;

    public EveItemNode(EveType type, EveLocation location, Integer rawQuantity, List<Entry> itemInfoList, boolean container, List<EveItemNode> children) {
        this.type = type;
        this.location = location;
        this.rawQuantity = rawQuantity;
        this.itemInfoList = itemInfoList;
        this.container = container;
        this.children = children;
        int quantity = 0;
        for (Entry entry : itemInfoList) {
            quantity += entry.getQuantity();
        }
        this.quantity = quantity;
        this.id = StringUtils.join(getItemIds(), "|");
    }

    public String getId() {
        return id;
    }

    public List<EveItemNode> getChildren() {
        return children != null ? Collections.unmodifiableList(children) : null;
    }

    public boolean isNotEmptyContainer() {
        return !(children == null || children.isEmpty());
    }

    public String getDisplayName(boolean displayQuantity, LocalBlueprintInfo localBlueprintInfo) {
        String name = isNameCustomisable() && getCustomName() != null ? getCustomName() : type.getName();
        return name +
                (location != null ? " ["+location.getName()+"]" : "") +
                (isBlueprint() ? " [" + calculateMaterialLevel(localBlueprintInfo).getValue() + ", " + calculateProductivityLevel(localBlueprintInfo).getValue() +
                        (isBlueprintOriginal() ? "]" : ", " + calculateRuns(localBlueprintInfo) + "] (copy)")
                        : "") +
                (displayQuantity && quantity > 1 ? " ("+quantity+")" : "") +
                (isContainerIgnored() ? " (ignored)" : "");
    }

    public String getSearchName() {
        String name = isNameCustomisable() && getCustomName() != null ? getCustomName() : type.getName();
        return (name + (location != null ? " ["+location.getName()+"]" : "") + (isContainerIgnored() ? " (ignored)" : "")).toLowerCase();
    }


    public Integer getTypeId() {
        return type.getId();
    }

    public LevelInfo calculateMaterialLevel(LocalBlueprintInfo localBlueprintInfo) {
        if (!isBlueprint()) {
            throw new RuntimeException("This is not blueprint. Stop it.");
        }

        return BlueprintHelper.calculateMaterialLevel(
                localBlueprintInfo, type.getBlueprint(),
                isBlueprintOriginal() ? BlueprintType.Original : BlueprintType.Copy);
    }

    public LevelInfo calculateProductivityLevel(LocalBlueprintInfo localBlueprintInfo) {
        if (!isBlueprint()) {
            throw new RuntimeException("This is not blueprint. Stop it.");
        }

        return BlueprintHelper.calculateProductivityLevel(
                localBlueprintInfo, type.getBlueprint(),
                isBlueprintOriginal() ? BlueprintType.Original : BlueprintType.Copy);
    }

    public float calculateWasteFactor(LocalBlueprintInfo localBlueprintInfo) {
        LevelInfo materialLevel = calculateMaterialLevel(localBlueprintInfo);
        int me = materialLevel.getValue();

        return BlueprintHelper.calculateWasteFactor(type.getBlueprint(), me);
    }

    public int calculateRuns(LocalBlueprintInfo localBlueprintInfo) {
        if (!isBlueprint()) {
            throw new RuntimeException("This is not blueprint. Stop it.");
        }

        return BlueprintHelper.calculateRuns(localBlueprintInfo, type.getBlueprint());
    }

    public EveType getType() {
        return type;
    }

    public EveLocation getLocation() {
        return location;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Integer getRawQuantity() {
        return rawQuantity;
    }

    public List<Entry> getItemInfoList() {
        return Collections.unmodifiableList(itemInfoList);
    }

    public boolean isBlueprintOriginal() {
        return isBlueprint() && (rawQuantity == null || rawQuantity == -1);
    }

    public boolean isBlueprintCopy() {
        return isBlueprint() && rawQuantity != null && rawQuantity == -2;
    }

    public boolean isBlueprint() {
        return type.isBlueprint();
    }

    public Set<Long> getItemIds() {
        Set<Long> ids = new HashSet<>(itemInfoList.size());
        for (Entry itemInfo : itemInfoList) {
            ids.add(itemInfo.getId());
        }

        return ids;
    }

    public EveItemNode subset(Set<Long> itemIds) {
        List<Entry> newItemInfoList = new ArrayList<>(itemIds.size());
        for (Entry itemInfo : itemInfoList) {
            if(itemIds.contains(itemInfo.getId())) {
                newItemInfoList.add(itemInfo);
            }
        }
        return new EveItemNode(type, location, rawQuantity, newItemInfoList, container, children);
    }

    public EveItemNode merge(EveItemNode that) {
        List<Entry> mergedItemInfoList = new ArrayList<>();
        mergedItemInfoList.addAll(itemInfoList);
        mergedItemInfoList.addAll(that.itemInfoList);
        return new EveItemNode(type, location, rawQuantity, mergedItemInfoList, container, children);
    }

    public boolean isExpanded() {
        return itemInfoList.size() == 1 && itemInfoList.get(0).localItemInfo.isExpanded();
    }

    public LocalItemInfo setExpanded(boolean expanded) {
        if (itemInfoList.size() != 1) {
            throw new IllegalStateException("Cannot set expanded property to multiple item node.");
        }
        LocalItemInfo localItemInfo = itemInfoList.get(0).localItemInfo;
        localItemInfo.setExpanded(expanded);

        return localItemInfo;
    }

    public boolean isContainerIgnored() {
        return itemInfoList.size() == 1 && itemInfoList.get(0).localItemInfo.isContainerIgnored();
    }

    public LocalItemInfo setContainerIgnored(boolean value) {
        if (itemInfoList.size() != 1) {
            throw new IllegalStateException("Cannot set containerIgnored property to multiple item node (this is not container).");
        }
        LocalItemInfo localItemInfo = itemInfoList.get(0).localItemInfo;
        localItemInfo.setContainerIgnored(value);

        return localItemInfo;
    }

    public boolean isContainer() {
        return container;
    }

    public boolean isNameCustomisable() {
        int marketGroupId = type.getMarketGroupId();
        return isContainer() &&
                (marketGroupId == 1657 || marketGroupId==1651 || marketGroupId==1658);
    }

    public String getCustomName() {
        return isNameCustomisable() ? itemInfoList.get(0).localItemInfo.getCustomName() : null;
    }

    public LocalItemInfo setCustomName(String customName) {
        if (!isNameCustomisable()) {
            throw new IllegalStateException("Cannot set custom name: invalid item (not container or it's type does not support rename)");
        }

        LocalItemInfo localItemInfo = itemInfoList.get(0).localItemInfo;
        localItemInfo.setCustomName(customName);

        return localItemInfo;
    }

    public BlueprintType getBlueprintType() {
        return isBlueprintOriginal() ? BlueprintType.Original : BlueprintType.Copy;
    }

    public EveItemNode setChildren(List<EveItemNode> children) {
        return new EveItemNode(type, location, rawQuantity, itemInfoList, container, children);
    }

    public static class Entry {
        private LocalItemInfo localItemInfo;
        private AssetItemInfo assetItemInfo;

        public Entry(LocalItemInfo localItemInfo, AssetItemInfo assetItemInfo) {
            this.localItemInfo = localItemInfo;
            this.assetItemInfo = assetItemInfo;
        }

        public Long getId() {
            return assetItemInfo.getId();
        }

        public Integer getQuantity() {
            return assetItemInfo.getQuantity();
        }
    }
}
