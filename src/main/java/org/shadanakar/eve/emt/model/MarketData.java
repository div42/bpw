/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.helpers.Constants;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class MarketData {
    private static final Log log = LogFactory.getLog(MarketData.class);
    private final Date snapshotTime;
    private final Map<Integer, TypeMarketData> marketDataMap;

    public MarketData(Date snapshotTime, Map<Integer, TypeMarketData> marketDataMap) {
        this.snapshotTime = snapshotTime;
        this.marketDataMap = marketDataMap;
        // sort them...
        for (TypeMarketData typeMarketData : marketDataMap.values()) {
            typeMarketData.sort();
        }
    }

    public Date getSnapshotTime() {
        return snapshotTime;
    }

    public Map<Integer, TypeMarketData> getMarketDataMap() {
        return marketDataMap;
    }

    public float estimatePrice(MarketOrderType type, EveType eveType, float quantityRequired) {
        TypeMarketData typeMarketData = marketDataMap.get(eveType.getId());
        if(typeMarketData == null) {
            return Float.NaN; // lets try this...
        }

        quantityRequired = Math.max(quantityRequired, Constants.MIN_QUANTITY_FOR_PRICE_ESTIMATES);

        List<MarketDataOrderLine> orders = typeMarketData.getOrders(type);
        // taking quantity into account

        int quantity = (int) Math.ceil(quantityRequired);
        int quantityCalculated = 0;
        float averagePrice = 0;

        for (MarketDataOrderLine order : orders) {
            int quantityNeeded = quantity - quantityCalculated;
            if (quantityNeeded <= 0) {
                break;
            }

            if (order.getMinVolume() > quantityNeeded) { // to avoid confusions also know as scam
                continue;
            }

            long qty = Math.min(quantityNeeded, order.getQuantity());
            averagePrice = ((averagePrice*quantityCalculated) + (qty*order.getPrice()))/(quantityCalculated + qty);
            quantityCalculated += qty;
        }

        if (quantity - quantityCalculated > 0) {
            log.warn("Material: ['"+eveType.getName()+"', typeId="+eveType.getId()+"] quantity required have not been met by the market. Estimate might be inaccurate. quantity needed: " + quantity + ", total quantity on the market: " + quantityCalculated);
        }

        return averagePrice;
    }

    public enum Source { EVE_MARKETDATA_COM, LOCAL_CACHE }
}
