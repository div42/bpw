/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * This class represents group of jobs of the same time.
 * That is job of the same ActivityType with same Productivity and Material Levels
 */
public class JobGroup {
    private final ActivityType activityType;
    private final EveType blueprintType;
    private final int materialLevel;
    private final int productivityLevel;
    private final int numberOfRuns;
    private final EveDecryptorInfo decryptor;
    private final EveType baseItemType;
    private final List<JobModel> jobs;
    private final Set<Long> excludedBlueprintIds;

    public JobGroup(ActivityType activityType, EveType blueprintType, int materialLevel, int productivityLevel, int numberOfRuns, EveDecryptorInfo decryptor, EveType baseItemType, List<JobModel> jobs) {
        this(activityType, blueprintType, materialLevel, productivityLevel, numberOfRuns, decryptor, baseItemType, jobs, Collections.<Long>emptySet());
    }

    public JobGroup(ActivityType activityType, EveType blueprintType, int materialLevel, int productivityLevel, int numberOfRuns, EveDecryptorInfo decryptor, EveType baseItemType, List<JobModel> jobs, Set<Long> excludedBlueprintIds) {
        this.activityType = activityType;
        this.blueprintType = blueprintType;
        this.materialLevel = materialLevel;
        this.productivityLevel = productivityLevel;
        this.numberOfRuns = numberOfRuns;
        this.decryptor = decryptor;
        this.baseItemType = baseItemType;
        this.jobs = jobs;
        this.excludedBlueprintIds = excludedBlueprintIds;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public EveType getBlueprintType() {
        return blueprintType;
    }

    public int getProductivityLevel() {
        return productivityLevel;
    }

    public int getMaterialLevel() {
        return materialLevel;
    }

    public int getNumberOfRuns() {
        return numberOfRuns;
    }

    public List<JobModel> getAllJobs() {
        return Collections.unmodifiableList(jobs);
    }

    public Iterable<JobModel> getJobsFiltered() {
        return Iterables.filter(getAllJobs(), new Predicate<JobModel>() {
            @Override
            public boolean apply(@Nullable JobModel input) {
                return input != null && !excludedBlueprintIds.contains(input.getItemId());
            }
        });
    }

    public int getQuantityTotal() {
        return jobs.size();
    }

    public int getQuantityFiltered() {
        return jobs.size() - excludedBlueprintIds.size();
    }

    public EveDecryptorInfo getDecryptor() {
        return decryptor;
    }

    @Override
    public String toString() {
        return "JobGroup{" +
                "activityType=" + activityType +
                ", blueprintType=" + blueprintType +
                ", productivityLevel=" + productivityLevel +
                ", materialLevel=" + materialLevel +
                ", numberOfRuns=" + numberOfRuns +
                ", decryptor=" + (decryptor == null ? null : decryptor.getDecryptorType().getName()) +
                ", jobs.size()=" + jobs.size() +
                '}';
    }

    public static JobGroup createInventionGroup(List<JobModel> jobModels) {
        if (jobModels == null || jobModels.isEmpty()) {
            return null;
        }

        JobModel modelJob = jobModels.get(0);

        return new JobGroup(ActivityType.INVENTION,
                modelJob.getBlueprintType(), modelJob.getMaterialLevel(), modelJob.getProductivityLevel(),
                modelJob.getNumberOfRuns(), modelJob.getDecryptor(), modelJob.getBaseItemType(), jobModels);
    }

//    public JobGroup setJobs(List<JobModel> jobModels) {
//        return new JobGroup(activityType, blueprintType, materialLevel, productivityLevel, numberOfRuns, decryptor, jobModels);
//    }

    public JobGroup setExcludedBlueprintIds(Set<Long> excludedBlueprintIds) {
        return new JobGroup(activityType, blueprintType, materialLevel, productivityLevel, numberOfRuns, decryptor, baseItemType, jobs, excludedBlueprintIds);
    }

    public EveType getBaseItemType() {
        return baseItemType;
    }
}
