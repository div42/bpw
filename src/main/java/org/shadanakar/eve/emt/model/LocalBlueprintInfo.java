/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class LocalBlueprintInfo {
    private int id;
    private Integer materialLevel;
    private Integer productivityLevel;
    private Integer runs;

    public LocalBlueprintInfo(int id) {
        this.id = id;
        this.materialLevel = null;
        this.productivityLevel = null;
    }

    public LocalBlueprintInfo(LocalBlueprintInfo that) {
        this.id = that.id;
        this.materialLevel = that.materialLevel;
        this.productivityLevel = that.productivityLevel;
        this.runs = that.runs;
    }

    public void setMaterialLevel(Integer materialLevel) {
        this.materialLevel = materialLevel;
    }

    public void setProductivityLevel(Integer productivityLevel) {
        this.productivityLevel = productivityLevel;
    }

    public int getId() {
        return id;
    }

    public Integer getMaterialLevel() {
        return materialLevel;
    }

    public Integer getProductivityLevel() {
        return productivityLevel;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != LocalBlueprintInfo.class) {
            return false;
        }

        LocalBlueprintInfo that = (LocalBlueprintInfo) obj;
        return this.id==that.id
                && ((this.productivityLevel == null && that.productivityLevel == null) || (this.productivityLevel != null && this.productivityLevel.equals(that.productivityLevel)))
                && ((this.materialLevel == null && that.materialLevel == null) || (this.materialLevel != null && this.materialLevel.equals(that.materialLevel)))
                && ((this.runs == null && that.runs == null) || (this.runs != null && this.runs.equals(that.runs)));
    }

    public boolean equalsFuzzy(LocalBlueprintInfo that, int defaultME, int defaultPE, int defaultRuns) {
        return this.id == that.id
                && equalsFuzzy(this.materialLevel, that.materialLevel, defaultME)
                && equalsFuzzy(this.productivityLevel, that.productivityLevel, defaultPE)
                && equalsFuzzy(this.runs, that.runs, defaultRuns);
    }

    private boolean equalsFuzzy(Integer one, Integer two, int defaultValue) {
        return ( (one == null || one == defaultValue) && (two == null || two == defaultValue) )
                || (one != null && one.equals(two));
    }

    public String dump() {
        return "(id=" + id + ", me="+materialLevel+", pe="+productivityLevel+", runs="+runs+")";
    }

    public Integer getRuns() {
        return runs;
    }

    public void setRuns(Integer runs) {
        this.runs = runs;
    }
}
