/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import com.google.common.base.Predicate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pivot.collections.Sequence;
import org.apache.pivot.wtk.content.TreeBranch;
import org.apache.pivot.wtk.content.TreeNode;
import org.apache.pivot.wtk.media.Image;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.helpers.ImageLoader;
import org.shadanakar.eve.emt.ui.model.ApplicationData;

import javax.annotation.Nullable;
import java.util.*;

public class ItemTreeModel {
    @SuppressWarnings("UnusedDeclaration")
    private static final Log log = LogFactory.getLog(ItemTreeModel.class);

    private final TreeBranch pivotTree;
    private final Map<String, String> pathMap;
    private final Map<String, String> pathByNodeIdMap;
    private final List<Sequence.Tree.Path> expandedPaths;

    public ItemTreeModel(TreeBranch pivotTree, Map<String, String> pathMap, List<Sequence.Tree.Path> expandedPaths) {
        this.pivotTree = pivotTree;
        this.pathMap = pathMap;
        this.pathByNodeIdMap = new HashMap<>(pathMap.size());
        this.expandedPaths = expandedPaths;

        for (Map.Entry<String, String> entry : pathMap.entrySet()) {
            pathByNodeIdMap.put(entry.getValue(), entry.getKey());
        }
    }

    public TreeBranch getPivotTree() {
        return pivotTree;
    }

    public static ItemTreeModel create(final ApplicationData applicationData, final String searchString, final TypeFilter filter, final boolean hideScheduledBpcs) {
        final EveAssets assets = applicationData.getAssets();
        final TreeBranch root = new TreeBranch();
        final Map<String, String> pathMap= new HashMap<>();
        final List<Sequence.Tree.Path> expandedPaths = new ArrayList<>();
        final String searchStringLowercase = searchString.toLowerCase();

        constructSubtree(applicationData, pathMap, expandedPaths, root, new Sequence.Tree.Path(), assets.getNodes(), hideScheduledBpcs, new Predicate<EveItemNode>() {
            @Override
            public boolean apply(@Nullable EveItemNode input) {
                String searchName = input != null ? input.getSearchName() : "";
                return checkFilter(filter, input) && searchName.contains(searchStringLowercase);
            }
        });

        return new ItemTreeModel(root, pathMap, expandedPaths);
    }

    private static void constructSubtree(ApplicationData applicationData, Map<String, String> pathMap, List<Sequence.Tree.Path> expandedPaths, TreeBranch root, Sequence.Tree.Path path,
                                         List<EveItemNode> items, boolean hideScheduledBpcs, Predicate<EveItemNode> predicate) {
        // increment this only if added to tree
        int currentIndex= 0;
        for (EveItemNode node : items) {
            if(hideScheduledBpcs) {
                Set<Long> itemIds = node.getItemIds();
        // hideScheduledBpcs && item.isBlueprintCopy() && applicationData.getPlan().isScheduled(item.getItemIds())
                Set<Long> scheduledItemIds = applicationData.getPlan().getScheduledItemIds();
                itemIds.removeAll(scheduledItemIds);
                if (itemIds.isEmpty()) {
                    continue;
                } else {
                    node = node.subset(itemIds);
                }
            }

            LocalBlueprintInfo localBlueprintInfo = null;

            if (node.isBlueprint()) {
                localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfoNoException(node.getTypeId());
            }

            String displayName = node.getDisplayName(true, localBlueprintInfo);

            Sequence.Tree.Path currentPath = new Sequence.Tree.Path(path);
            currentPath.add(currentIndex);

            Image icon = ImageLoader.getInstance().loadImage16x16(node.getTypeId());

            if (node.isNotEmptyContainer()) {
                TreeBranch newRoot = new TreeBranch(icon, displayName);
                constructSubtree(applicationData, pathMap, expandedPaths, newRoot, currentPath, node.getChildren(), hideScheduledBpcs, predicate);
                if (!newRoot.isEmpty()) {
                    root.add(newRoot);
                    pathMap.put(currentPath.toString(), node.getId());
                    ++currentIndex;
                    if(node.isExpanded()) {
                        expandedPaths.add(currentPath);
                    }
                } else {
                    if (predicate.apply(node) ) {
                        root.add(new TreeNode(icon, displayName));
                        pathMap.put(currentPath.toString(), node.getId());
                        ++currentIndex;
                    }
                }
            } else if (predicate.apply(node)) {
                TreeNode treeNode = new TreeNode(icon, displayName);
                root.add(treeNode);
                pathMap.put(currentPath.toString(), node.getId());
                ++currentIndex;
            }
        }

    }

    private static boolean checkFilter(TypeFilter filter, EveItemNode item) {
        switch (filter) {
            case Blueprints:
                return item.isBlueprint();
            case BPOs:
                return item.isBlueprintOriginal();
            case BPCs:
                return item.isBlueprintCopy();
        }

        return true;
    }

    public String getNodeIdByPath(Sequence.Tree.Path path) {
        return path != null ? pathMap.get(path.toString()) : null;
    }

    public List<Sequence.Tree.Path> getExpandedPaths() {
        return expandedPaths;
    }

    public Sequence.Tree.Path getPathByItemId(String nodeId) {
        return parsePathFromString(pathByNodeIdMap.get(nodeId));
    }

    private Sequence.Tree.Path parsePathFromString(String string) {
        if (string == null) return null;

        String tokens[] = string.substring(1, string.length()-1).split(",");
        Sequence.Tree.Path path = new Sequence.Tree.Path();
        for (String token : tokens) {
            path.add(new Integer(token.trim()));
        }

        return path;
    }
}
