/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.util.*;

public class IndustryJobModel {
    private final Date currentTime;
    private final Date cachedUntil;
    private final Map<Long, EveIndustryJob> jobsById;
    private final Map<Long, EveIndustryJob> jobByItemId;
    private final Map<Integer, List<EveIndustryJob>> listByBlueprintTypeId;
    private final Map<Integer, List<EveIndustryJob>> listByActivityType;

    private Set<Long> jobIdsNotDeliveredBeforeAssetsUpdate = new HashSet<>();

    public IndustryJobModel(Date currentTime, Date cachedUntil, Map<Long, EveIndustryJob> jobsById, Map<Integer, List<EveIndustryJob>> listByActivityType) {
        this.currentTime = currentTime;
        this.cachedUntil = cachedUntil;
        this.jobsById = jobsById;
        this.listByActivityType = listByActivityType;
        jobByItemId = new HashMap<>();
        listByBlueprintTypeId = new HashMap<>();
        for (EveIndustryJob eveIndustryJob : jobsById.values()) {
            jobByItemId.put(eveIndustryJob.getInstalledLocalItemInfo().getId(), eveIndustryJob);
            int blueprintTypeId = eveIndustryJob.getInstalledItemType().getId();
            if (!listByBlueprintTypeId.containsKey(blueprintTypeId)) {
                listByBlueprintTypeId.put(blueprintTypeId, new ArrayList<EveIndustryJob>());
            }
            listByBlueprintTypeId.get(blueprintTypeId).add(eveIndustryJob);
        }
    }

    public List<EveIndustryJob> getJobListByActivityType(int activityType) {
        if (listByActivityType.containsKey(activityType)) {
            return Collections.unmodifiableList(listByActivityType.get(activityType));
        } else {
            return Collections.emptyList();
        }
    }

    public EveIndustryJob getJobById(long jobId) {
        return jobsById.get(jobId);
    }

    public Date getCurrentTime() {
        return currentTime;
    }

    @Nonnull
    public Date getCachedUntil() {
        return cachedUntil == null ? new Date(0) : cachedUntil;
    }

    public Iterable<EveIndustryJob> getAllJobs() {
        Iterable<EveIndustryJob> initial = Collections.emptyList();
        for (List<EveIndustryJob> eveIndustryJobs : listByActivityType.values()) {
            initial = Iterables.concat(initial, Collections.unmodifiableList(eveIndustryJobs));
        }

        return initial;
    }

    public EveIndustryJob getJobByItemId(long itemId) {
        return jobByItemId.get(itemId);
    }

    public List<EveIndustryJob> getJobsByBlueprintTypeId(int blueprintTypeId) {
        return listByBlueprintTypeId.containsKey(blueprintTypeId) ?
                Collections.unmodifiableList(listByBlueprintTypeId.get(blueprintTypeId))
                : Collections.<EveIndustryJob>emptyList();
    }

    public long secondsToNextUpdate(Date now) {
        return cachedUntil == null ? (-now.getTime()) : (cachedUntil.getTime() - now.getTime()) / 1000;
    }

    public Set<Long> getJobIdsNotDeliveredBeforeAssetsUpdate() {
        return Collections.unmodifiableSet(jobIdsNotDeliveredBeforeAssetsUpdate);
    }

    public void setJobIdsNotDeliveredBeforeAssetsUpdate(Set<Long> jobIdsNotDeliveredBeforeAssetsUpdate) {
        this.jobIdsNotDeliveredBeforeAssetsUpdate = jobIdsNotDeliveredBeforeAssetsUpdate;
    }

    public boolean isInitialized() {
        return cachedUntil != null;
    }
}
