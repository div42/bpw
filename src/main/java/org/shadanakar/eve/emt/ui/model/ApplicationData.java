/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.model;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.db.EveDbHelper;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.helpers.BlueprintHelper;
import org.shadanakar.eve.emt.helpers.Constants;
import org.shadanakar.eve.emt.helpers.DecisionMaker;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.UserSettings;
import org.shadanakar.eve.emt.utils.PerformanceLogger;

import javax.annotation.Nullable;
import java.sql.SQLException;
import java.util.*;

public class ApplicationData {
    private static final Log log = LogFactory.getLog(ApplicationData.class);

    public static class PlanCalculationResults {

        private final List<JobMaterialModel> jobMaterials;
        private final List<JobWorkflowItem> workflowItems;
        private final List<JobGroup> jobGroups;
        private final Set<Long> excludedJobIds;
        private final Set<Long> notDeliveredJobIds;

        public PlanCalculationResults(List<JobMaterialModel> jobMaterials, List<JobWorkflowItem> workflowItems, List<JobGroup> jobGroups, Set<Long> excludedJobIds, Set<Long> notDeliveredJobIds) {
            this.jobMaterials = jobMaterials;
            this.workflowItems = workflowItems;
            this.jobGroups = jobGroups;
            this.excludedJobIds = excludedJobIds;
            this.notDeliveredJobIds = notDeliveredJobIds;
        }

        public List<JobMaterialModel> getJobMaterials() {
            return Collections.unmodifiableList(jobMaterials);
        }

        public List<JobWorkflowItem> getWorkflowItems() {
            return Collections.unmodifiableList(workflowItems);
        }

        public Set<Long> getExcludedJobIds() {
            return Collections.unmodifiableSet(excludedJobIds);
        }

        public Set<Long> getNotDeliveredJobIds() {
            return Collections.unmodifiableSet(notDeliveredJobIds);
        }

        public List<JobGroup> getJobGroups() {
            return Collections.unmodifiableList(jobGroups);
        }
    }

    private UserSettings userSettings = new UserSettings();

    private EveAssets assets = new EveAssets(new Date(0), null, Collections.<EveItemNode>emptyList(), Collections.<String, EveItemNode>emptyMap());
    private PlanModel plan = null;
    private MarketData marketData = new MarketData(new Date(0), Collections.<Integer, TypeMarketData>emptyMap());
    private IndustryJobModel industryJobs = new IndustryJobModel(new Date(0), null, Collections.<Long, EveIndustryJob>emptyMap(), Collections.<Integer, List<EveIndustryJob>>emptyMap());
    private MarketOrdersModel marketOrder = new MarketOrdersModel();

    public EveAssets getAssets() {
        return assets;
    }

    public void setAssets(EveAssets assets) {
        this.assets = assets;
    }

    public PlanModel getPlan() {
        return plan;
    }

    public void setPlan(PlanModel plan) {
        this.plan = plan;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public IndustryJobModel getIndustryJobs() {
        return industryJobs;
    }

    public void setIndustryJobs(IndustryJobModel industryJobs) {
        // this is for workaround...
        industryJobs.setJobIdsNotDeliveredBeforeAssetsUpdate(this.industryJobs.getJobIdsNotDeliveredBeforeAssetsUpdate());
        this.industryJobs = industryJobs;
    }

    private static final class JobMaterialModelMutable {
        final EveType eveType;
        float quantityRequired;
        final int quantityPresent;
        int quantityToManufacture;
        float quantityInProduction;

        JobMaterialModelMutable(EveType eveType, int quantityPresent) {
            this.eveType = eveType;
            this.quantityRequired = 0;
            this.quantityPresent = quantityPresent;
            this.quantityToManufacture = 0;
        }

        public JobMaterialModelMutable(EveType eveType, float quantityRequired, int quantityPresent) {
            this.eveType = eveType;
            this.quantityRequired = quantityRequired;
            this.quantityPresent = quantityPresent;
            this.quantityToManufacture = 0;
        }
    }

    public PlanCalculationResults calculatePlanBillOfMaterials(DecisionMaker decisionMaker) throws SQLException {
        if (assets == null || plan == null) {
            return new PlanCalculationResults(
                    Collections.<JobMaterialModel>emptyList(), Collections.<JobWorkflowItem>emptyList(),
                    Collections.<JobGroup>emptyList(), Collections.<Long>emptySet(), Collections.<Long>emptySet());
        }

        try (PerformanceLogger ignored = new PerformanceLogger(log, "calculatePlanBillOfMaterials")) {
            Map<Integer, JobMaterialModelMutable> materials = new LinkedHashMap<>();
            List<JobWorkflowItem> workflowItems = new ArrayList<>();
            Pair<List<JobGroup>, Set<Long>> jobGroupsAndExcludedJobIds = filterIndustryJobs(plan.getJobGroups());
            List<JobGroup> jobGroups = jobGroupsAndExcludedJobIds.getFirst();
            Set<Long> excludedJobIds = jobGroupsAndExcludedJobIds.getSecond();
            Set<Long> notDeliveredBeforeAssetsUpdateJobIds = industryJobs.getJobIdsNotDeliveredBeforeAssetsUpdate();

            // this map contains materialTypeId => correction value (in other words how many materials are on the fly)
            Map<Integer, Float> industryJobsCorrectionMap = calculateIndustryJobsCorrections(excludedJobIds, notDeliveredBeforeAssetsUpdateJobIds);

            // RAM reserve - equals to the max(bpc runs) for manufacturing jobs involving it
            Map<Integer, Integer> ramReserveMap = new HashMap<>();

            for (JobGroup jobGroupModel : jobGroups) {
                List<EveMaterialType> jobMaterials = Collections.emptyList();
                switch(jobGroupModel.getActivityType()) {
                    case INVENTION:
                        jobMaterials = calculateInventionMaterials(jobGroupModel);
                        break;
                    case MANUFACTURING:
                        jobMaterials = calculateManufacturingMaterials(jobGroupModel);
                        break;
                }

                // and now we are ready to do addition!
                int runs = jobGroupModel.getNumberOfRuns();

                for (EveMaterialType jobMaterial : jobMaterials) {
                    EveType materialType = jobMaterial.getMaterialType();
                    if (!materials.containsKey(materialType.getId())) {
                        materials.put(materialType.getId(), new JobMaterialModelMutable(materialType, assets.getTotalQuantityByType(materialType.getId())));
                    }

                    if (isRam(materialType)) {
                        int previousReserve = ramReserveMap.containsKey(materialType.getId()) ? ramReserveMap.get(materialType.getId()) : 0;
                        if (runs > previousReserve) {
                            ramReserveMap.put(materialType.getId(), runs);
                        }
                    }

                    JobMaterialModelMutable material = materials.get(materialType.getId());
                    material.quantityRequired += jobMaterial.getQuantity();
                }

                // bill of material per group -> saving to workflow
                int quantity = jobGroupModel.getActivityType() == ActivityType.MANUFACTURING
                        ? jobGroupModel.getNumberOfRuns()*jobGroupModel.getQuantityTotal() : jobGroupModel.getQuantityTotal();

                workflowItems.add(new JobWorkflowItem(jobGroupModel.getActivityType(),
                        jobGroupModel.getBlueprintType(),
                        jobGroupModel.getProductivityLevel(),
                        quantity,
                        new ArrayList<>(jobMaterials)));
            }

            // Round ups in FINAL list
            for (Map.Entry<Integer, JobMaterialModelMutable> entry : materials.entrySet()) {
                entry.getValue().quantityRequired = (float) Math.ceil(entry.getValue().quantityRequired);
            }

            // industry jobs corrections
            for (JobMaterialModelMutable jobMaterialModelMutable : materials.values()) {
                int id = jobMaterialModelMutable.eveType.getId();
                if (industryJobsCorrectionMap.containsKey(id)) {
                    jobMaterialModelMutable.quantityInProduction = industryJobsCorrectionMap.get(id);
                }
            }

            // now for composites we are going to make buy vs manufacture decisions
            List<Pair<EveType, Integer>> expandedMaterialBlueprintTypes = expandJobMaterialTypes(decisionMaker, materials, ramReserveMap);

            // final conversion
            List<JobMaterialModel> result = new ArrayList<>(materials.size());
            for (JobMaterialModelMutable jobMaterialModelMutable : materials.values()) {
                float price = marketData.estimatePrice(MarketOrderType.Sell, jobMaterialModelMutable.eveType,
                        Math.max(jobMaterialModelMutable.quantityRequired + jobMaterialModelMutable.quantityToManufacture, 1));

                result.add(new JobMaterialModel(jobMaterialModelMutable.eveType, price, jobMaterialModelMutable.quantityRequired, jobMaterialModelMutable.quantityPresent,
                        jobMaterialModelMutable.quantityToManufacture, jobMaterialModelMutable.quantityInProduction));
            }

            // workflow calculation and stuff
            for (Pair<EveType,Integer> blueprintTypeAndPL : expandedMaterialBlueprintTypes) {
                for (JobWorkflowItem workflowItem : workflowItems) {
                    int materialTypeId = blueprintTypeAndPL.getFirst().getBlueprint().getProductType().getId();
                    EveMaterialType materialTypeById = workflowItem.getMaterialTypeById(materialTypeId);
                    if (materialTypeById != null) {
                        workflowItem.addDependingItem(new JobWorkflowItem(ActivityType.MANUFACTURING,
                                blueprintTypeAndPL.getFirst(), blueprintTypeAndPL.getSecond(), materialTypeById.getQuantity(), null));
                    }
                }
            }

            return new PlanCalculationResults(result, workflowItems, jobGroups, excludedJobIds, notDeliveredBeforeAssetsUpdateJobIds);
        }
    }

    private Pair<List<JobGroup>, Set<Long>> filterIndustryJobs(List<JobGroup> jobGroups) {
        Set<Long> excludedJobId = new HashSet<>();
        Date assetsLoadTime = assets.getCurrentTime();
        List<JobGroup> result = new ArrayList<>();
        for (JobGroup jobGroup : jobGroups) {
//            List<JobModel> filteredJobModels = new ArrayList<>();
            Set<Long> excludedBlueprintIds = new HashSet<>();
            for (JobModel jobModel : jobGroup.getAllJobs()) {
                EveIndustryJob jobByItemId = industryJobs.getJobByItemId(jobModel.getItemId());
                if (jobByItemId != null && jobByItemId.getInstallTime().before(assetsLoadTime)) {
                    excludedJobId.add(jobByItemId.getId());
                    excludedBlueprintIds.add(jobModel.getItemId());
//                } else {
//                    filteredJobModels.add(jobModel);
                }
            }

            result.add(jobGroup.setExcludedBlueprintIds(excludedBlueprintIds));

//            if (filteredJobModels.size() < jobGroup.getJobs().size()) {
//                if (!filteredJobModels.isEmpty()) {
//                    result.add(jobGroup.setJobs(filteredJobModels));
//                }
//            } else {
//                result.add(jobGroup);
//            }
        }
        return Pair.create(result, excludedJobId);
    }

    /**
     * This method calculates correction map based on current industryJobs and on when AssetList was last updated.
     *
     * @return map(typeId, correctionValue)
     * @param excludedJobIds list of already delivered job ids for method to ignore.
     * @param notDeliveredBeforeAssetsUpdateJobIds list of not delivered before asset update job ids.
     */
    private Map<Integer, Float> calculateIndustryJobsCorrections(final Set<Long> excludedJobIds, final Set<Long> notDeliveredBeforeAssetsUpdateJobIds) throws SQLException {
        // Industry values. Only for manufacturing jobs for starters...
        List<EveIndustryJob> manufacturingJobs = industryJobs.getJobListByActivityType(Constants.ACTIVITYID_MANUFACTURING);

        final Date assetsUpdateTime = assets.getCurrentTime();

        Iterable<EveIndustryJob> jobsThatCount = Iterables.filter(manufacturingJobs, new Predicate<EveIndustryJob>() {
            @Override
            public boolean apply(@Nullable EveIndustryJob input) {
                return input != null && !excludedJobIds.contains(input.getId()) &&
                        (notDeliveredBeforeAssetsUpdateJobIds.contains(input.getId()) ||
                                (input.getBeginProductionTime().before(assetsUpdateTime) && input.getEndProductionTime().after(assetsUpdateTime)));
            }
        });

        Map<Integer, Float> correctionMap = new HashMap<>();
        // we have jobs that count... lets extract corrections
        for (EveIndustryJob eveIndustryJob : jobsThatCount) {
            EveType productType = eveIndustryJob.getInstalledItemType().getBlueprint().getProductType();
            int count = eveIndustryJob.getJobRuns()*productType.getPortionSize();
            if (correctionMap.containsKey(productType.getId())) {
                count += correctionMap.get(productType.getId());
            }
            correctionMap.put(productType.getId(), (float) count);

            // now.. resources
            LocalItemInfo localItemInfo = eveIndustryJob.getInstalledLocalItemInfo();
            if (plan.isItemScheduled(localItemInfo.getId())) {
                JobModel fakeJob = new JobModel(null, localItemInfo.getId(), eveIndustryJob.getInstalledItemType(), ActivityType.MANUFACTURING, localItemInfo.getMaterialLevel(), localItemInfo.getProductivityLevel(), eveIndustryJob.getJobRuns(), null, null);
                JobGroup jobGroup = new JobGroup(ActivityType.MANUFACTURING, eveIndustryJob.getInstalledItemType(), localItemInfo.getMaterialLevel(), localItemInfo.getProductivityLevel(), eveIndustryJob.getJobRuns(), null, null, Arrays.asList(fakeJob));
                List<EveMaterialType> materialTypes = calculateManufacturingMaterials(jobGroup);
                for (EveMaterialType materialType : materialTypes) {
                    float materialCount = materialType.getQuantity();
                    int materialTypeId = materialType.getMaterialType().getId();
                    if (correctionMap.containsKey(materialTypeId)) {
                        materialCount += correctionMap.get(materialTypeId);
                    }
                    correctionMap.put(materialTypeId, materialCount);
                }
            }
        }

        return correctionMap;
    }

    public List<EveMaterialType> calculateInventionMaterials(JobGroup jobGroupModel) throws SQLException {
        List<EveMaterialType> eveMaterialTypes = new ArrayList<>(EveDbHelper.getInstance().getInventionMaterialTypes(jobGroupModel.getBlueprintType().getId()));
        // I'm not sure if this information can be found in eve dump
        // resetting count of reusable components (data interfaces) to 0
        eveMaterialTypes = Lists.newArrayList(Lists.transform(eveMaterialTypes, new Function<EveMaterialType, EveMaterialType>() {
            @Nullable
            @Override
            public EveMaterialType apply(@Nullable EveMaterialType mat) {
                if (mat != null && mat.getMaterialType().getGroup().getId() == Constants.GROUPID_DATAINTERFACES) {
                    return mat.setQuantity(0);
                }

                return mat;
            }
        }));

        // add decryptor (if selected)
        if (jobGroupModel.getDecryptor() != null) {
            eveMaterialTypes.add(new EveMaterialType(jobGroupModel.getBlueprintType().getId(), jobGroupModel.getDecryptor().getDecryptorType(), 1, 1, false));
        }

        // add base item type (if there is one)
        if(jobGroupModel.getBaseItemType() != null) {
            eveMaterialTypes.add(new EveMaterialType(jobGroupModel.getBlueprintType().getId(), jobGroupModel.getBaseItemType(), 1, 1, false));
        }

        // apply correct multiplier
        for (int i=0; i<eveMaterialTypes.size(); ++i) {
            EveMaterialType curr = eveMaterialTypes.get(i);
            eveMaterialTypes.set(i, curr.setQuantity(curr.getQuantity()*jobGroupModel.getQuantityFiltered()));
        }

        return eveMaterialTypes;

    }

    private List<Pair<EveType, Integer>> expandJobMaterialTypes(final DecisionMaker decisionMaker, final Map<Integer, JobMaterialModelMutable> jobMaterials, Map<Integer, Integer> ramReserveMap) throws SQLException {
        // copy list
        List<JobMaterialModelMutable> originalList = new ArrayList<>(jobMaterials.values());
        List<Pair<EveType, Integer>> expandedMaterialBlueprintTypes = new ArrayList<>();

        for (JobMaterialModelMutable materialType : originalList) {
            EveType eveType = materialType.eveType;
            int materialTypeId = eveType.getId();
            EveType materialBlueprintType = EveDbHelper.getInstance().findBlueprintTypeByItemTypeId(materialTypeId);
            if (materialBlueprintType != null) {
                // lets make a decision

                // R.A.M. - need some extra for those
                if (isRam(eveType)) {
                    materialType.quantityRequired += ramReserveMap.get(materialTypeId) + 1;
                }
                float quantityToExpand = Math.max(materialType.quantityRequired - materialType.quantityInProduction - materialType.quantityPresent, 0);

                LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfo(materialBlueprintType.getId());
                EveBlueprint blueprint = materialBlueprintType.getBlueprint();
                List<JobModel> jobs = Collections.emptyList();
                int productivityLevel = BlueprintHelper.calculateProductivityLevel(localBlueprintInfo, blueprint, BlueprintType.Copy).getValue();

                if (0 != quantityToExpand && DecisionMaker.Decision.Manufacture == decisionMaker.makeDecision(eveType, quantityToExpand)) {
                    // this is where all the fun happens
                    // register this decision
                    expandedMaterialBlueprintTypes.add(Pair.create(materialBlueprintType, productivityLevel));

                    JobGroup fakeJobGroup = new JobGroup(ActivityType.MANUFACTURING, materialBlueprintType,
                            BlueprintHelper.calculateMaterialLevel(localBlueprintInfo, blueprint, BlueprintType.Copy).getValue(),
                            productivityLevel, 1, null, null, jobs);
                    List<EveMaterialType> subMaterialsPerItem = calculateManufacturingMaterials(fakeJobGroup);

                    materialType.quantityToManufacture = (int) quantityToExpand;

                    // do not forget about original quantities
                    for (EveMaterialType subMaterialType : subMaterialsPerItem) {
                        int id = subMaterialType.getMaterialType().getId();
                        if (jobMaterials.containsKey(id)) {
                            JobMaterialModelMutable oldMaterialType = jobMaterials.get(id);
                            oldMaterialType.quantityRequired += (quantityToExpand * subMaterialType.getQuantity());
                        } else {
                            float newQuantity = quantityToExpand * subMaterialType.getQuantity();
                            jobMaterials.put(id, new JobMaterialModelMutable(subMaterialType.getMaterialType(), newQuantity, assets.getTotalQuantityByType(subMaterialType.getMaterialType().getId())));
                        }
                    }
                } else if (quantityToExpand + materialType.quantityInProduction != 0) {
                    // it is in production already
                    expandedMaterialBlueprintTypes.add(Pair.create(materialBlueprintType, productivityLevel));
                }
            }
        }

        return expandedMaterialBlueprintTypes;
    }

    private boolean isRam(EveType type) {
        return Constants.ramTypeIds.contains(type.getId());
    }

    public Set<Integer> findAllProductionTypeIds() throws SQLException {
        // 1. we are going to fetch all blueprints
        Set<EveType> blueprintTypes = assets.findAllBlueprintTypes();
        Set<Integer> typeIds = new HashSet<>();
        for (EveType blueprintType : blueprintTypes) {
            addTypeIdsForBlueprint(blueprintType, typeIds);
            // t2 variation of t1 blueprints if any...
            List<EveType> inventionOutputs = EveDbHelper.getInstance().findInventionOutputs(blueprintType);
            for (EveType inventionOutput : inventionOutputs) {
                addTypeIdsForBlueprint(inventionOutput, typeIds);
            }

            // base item types for t1 invention if any
            List<EveType> baseTypes = EveDbHelper.getInstance().findMetaGroupItemsByParentTypeId(blueprintType.getBlueprint().getProductType().getId());
            typeIds.addAll(Lists.newArrayList(Iterables.transform(baseTypes, new Function<EveType, Integer>() {
                @Nullable
                @Override
                public Integer apply(@Nullable EveType input) {
                    return null == input ? null : input.getId();
                }
            })));
        }

        // 2. add all decryptors
        typeIds.addAll(EveDbHelper.getInstance().findAllDecryptorIds());

        // todo: we will need to do something about jobs

        return typeIds;
    }

    private void addTypeIdsForBlueprint(EveType blueprintType, Set<Integer> typeIds) throws SQLException {
        typeIds.add(blueprintType.getBlueprint().getProductType().getId());
        List<EveMaterialType> manufacturingMaterials = EveDbHelper.getInstance().getMaterialTypes(blueprintType.getBlueprint().getProductType().getId());
        typeIds.addAll(transformMaterialTypesToTypeIds(manufacturingMaterials));
        List<EveMaterialType> extraMaterials = EveDbHelper.getInstance().getExtraMaterialTypes(blueprintType.getId());
        typeIds.addAll(transformMaterialTypesToTypeIds(extraMaterials));

        List<EveMaterialType> inventionMaterials = EveDbHelper.getInstance().getInventionMaterialTypes(blueprintType.getId());
        typeIds.addAll(transformMaterialTypesToTypeIds(inventionMaterials));
    }

    private Set<Integer> transformMaterialTypesToTypeIds(List<EveMaterialType> manufacturingMaterials) {
        Set<Integer> ids = new HashSet<>(manufacturingMaterials.size());
        for (EveMaterialType manufacturingMaterial : manufacturingMaterials) {
            ids.add(manufacturingMaterial.getMaterialType().getId());
        }

        return ids;
    }

    private List<EveMaterialType> calculateManufacturingMaterials(final JobGroup jobGroup) throws SQLException {
        try (PerformanceLogger ignored = new PerformanceLogger(log, "calculateManufacturingMaterials(" + jobGroup.toString() + ")")) {
            final EveType blueprintType = jobGroup.getBlueprintType();
            final EveBlueprint blueprint = blueprintType.getBlueprint();
            final EveType productType = blueprint.getProductType();
            List<EveMaterialType> materialTypes = EveDbHelper.getInstance().getMaterialTypes(productType.getId());

            // we are going to copy this in order to be able to remove stuff from this list.
            List<EveMaterialType> extraMaterials = new ArrayList<>(EveDbHelper.getInstance().getExtraMaterialTypes(blueprintType.getId()));

            // 1. processing materialTypes... applying waste in progress
            float wasteCoefficient = BlueprintHelper.calculateWasteFactor(blueprint, jobGroup.getMaterialLevel());

            // calculate recycled stuff if applicable
            Map<Integer, Integer> recycledMaterials = new HashMap<>();
            for (EveMaterialType type : extraMaterials) {
                if(type.isRecycle()) {
                    List<EveMaterialType> recycleMaterials = EveDbHelper.getInstance().getMaterialTypes(type.getMaterialType().getId());
                    for (EveMaterialType recycleMaterial : recycleMaterials) {
                        int materialId = recycleMaterial.getMaterialType().getId();
                        int previousQuantity = recycledMaterials.containsKey(materialId) ? recycledMaterials.get(materialId) : 0;
                        recycledMaterials.put(materialId, previousQuantity + (int) recycleMaterial.getQuantity());
                    }
                }
            }

            // 0 -> 1, special case to support fake job group for price estimates
            int totalNumberOfRuns = jobGroup.getQuantityTotal() == 0 ? 1 : jobGroup.getNumberOfRuns()*jobGroup.getQuantityFiltered();

            final List<EveMaterialType> preliminaryList = new ArrayList<>();

            for (final EveMaterialType type: materialTypes) {
                int quantity = (int) type.getQuantity();

                // apply recycled stuff.
                if (recycledMaterials.containsKey(type.getMaterialType().getId())) {
                    quantity -= recycledMaterials.get(type.getMaterialType().getId());
                }

                int wasteQuantity = Math.round(quantity*wasteCoefficient);

                EveMaterialType extraType = Iterables.find(extraMaterials, new Predicate<EveMaterialType>() {
                    @Override
                    public boolean apply(@Nullable EveMaterialType input) {
                        return input != null && input.getMaterialType() == type.getMaterialType();
                    }
                }, null);

                float totalQuantity = quantity + wasteQuantity;
                if(extraType != null) {
                    totalQuantity += extraType.getQuantity() * extraType.getDamagePerJob();
                    extraMaterials.remove(extraType);
                }

                // only if it resulted quantity is positive
                if (totalQuantity > 0f) {
                    preliminaryList.add(new EveMaterialType(productType.getId(), type.getMaterialType(), totalNumberOfRuns*totalQuantity, 1, false));
                }
            }

            // Just add what is left in extra collection
            for (EveMaterialType type : extraMaterials) {
                if (type.getDamagePerJob() != 0) {
                    preliminaryList.add(new EveMaterialType(type.getProductTypeId(), type.getMaterialType(),
                            totalNumberOfRuns*(type.getQuantity()*type.getDamagePerJob()), 1, false));
                }
            }

            return preliminaryList;
        }
    }

    public void setMarketData(MarketData marketData) {
        this.marketData = marketData;
    }

    public MarketData getMarketData() {
        return marketData;
    }

    public Float calculateItemManufacturingPrice(EveType itemType) throws SQLException {
        EveType blueprintType = EveDbHelper.getInstance().findBlueprintTypeByItemTypeId(itemType.getId());
        if (null == blueprintType) { // there is no blueprint for that
            return null;
        }

        LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfo(blueprintType.getId());
        EveBlueprint blueprint = blueprintType.getBlueprint();

        List<JobModel> jobs = Collections.emptyList();

        JobGroup fakeJobGroup = new JobGroup(ActivityType.MANUFACTURING, blueprintType,
                BlueprintHelper.calculateMaterialLevel(localBlueprintInfo, blueprint, BlueprintType.Copy).getValue(),
                BlueprintHelper.calculateProductivityLevel(localBlueprintInfo, blueprint, BlueprintType.Copy).getValue(), 1, null, null, jobs);

        List<EveMaterialType> eveMaterialTypes = calculateManufacturingMaterials(fakeJobGroup);
        Float res = 0f;
        for (EveMaterialType eveMaterialType : eveMaterialTypes) {
            float pricePerItem = marketData.estimatePrice(MarketOrderType.Sell, eveMaterialType.getMaterialType(), eveMaterialType.getQuantity());
            res += pricePerItem*eveMaterialType.getQuantity();
        }

        return res;
    }

    public Float calculateItemManufacturingTimeSec(EveType itemType) throws SQLException {
        EveType blueprintType = EveDbHelper.getInstance().findBlueprintTypeByItemTypeId(itemType.getId());
        if (null == blueprintType) { // there is no blueprint for that
            return null;
        }

        LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfo(blueprintType.getId());
        EveBlueprint blueprint = blueprintType.getBlueprint();

        return BlueprintHelper.calculateProductionTime(
                BlueprintHelper.calculateProductivityLevel(localBlueprintInfo, blueprint, BlueprintType.Copy).getValue(),
                blueprint);
    }

    public DecisionMaker getDecisionMaker() {
        return new DecisionMaker() {
            @Override
            public Decision makeDecision(EveType type, float quantity) {
                try {
                    float itemBuyPrice = marketData.estimatePrice(MarketOrderType.Sell, type, quantity);
                    Float itemManufacturingPrice = calculateItemManufacturingPrice(type);
                    if (itemManufacturingPrice == null || itemManufacturingPrice >= itemBuyPrice) {
                        log.info("Decision is BUY for " + type.getName() + ": manufacturingPrice=" +itemManufacturingPrice + ", buyPrice="+itemBuyPrice);
                        return Decision.Buy;
                    } else {
                        log.info("Decision is Manufacture for " + type.getName() + ": manufacturingPrice=" +itemManufacturingPrice + ", buyPrice="+itemBuyPrice);
                        return Decision.Manufacture;
                    }
                } catch (SQLException ex) {
                    log.error("Error while making buy/manufacture decision about " + type.getName(), ex);
                    return Decision.Buy;
                }
            }

        };
    }
}
