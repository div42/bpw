/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui;

import nu.xom.ParsingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pivot.beans.BXMLSerializer;
import org.apache.pivot.collections.Map;
import org.apache.pivot.collections.Sequence;
import org.apache.pivot.serialization.SerializationException;
import org.apache.pivot.wtk.*;
import org.apache.pivot.wtk.Window;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.helpers.*;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.model.ApplicationData;
import org.shadanakar.eve.emt.utils.PerformanceLogger;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * $Id$
 */
public class BpwApplication implements Application {
    private static final Log log = LogFactory.getLog(BpwApplication.class);
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    private final TreeViewBranchListener.Adapter treeViewBranchListener = new TreeViewBranchListener.Adapter() {
        @Override
        public void branchExpanded(TreeView treeView, Sequence.Tree.Path path) {
            String id = visibleTree.getNodeIdByPath(path);
            LocalItemInfo localItemInfo = applicationData.getAssets().getNodeById(id).setExpanded(true);
            ApplicationHelper.scheduleSave(localItemInfo, null);
        }

        @Override
        public void branchCollapsed(TreeView treeView, Sequence.Tree.Path path) {
            String id = visibleTree.getNodeIdByPath(path);
            log.info("path: " + path.toString() + ", itemId= " + id);
            LocalItemInfo localItemInfo = applicationData.getAssets().getNodeById(id).setExpanded(false);

            ApplicationHelper.scheduleSave(localItemInfo, null);
        }
    };
    private ProgressListener progressListener = new ProgressListener() {
        public double total;

        @Override
        public void progressStarted(long total) {
            this.total = total;
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    progressBar.setPercentage(0);
                    progressBar.setVisible(true);
                }
            });
        }

        @Override
        public void progressUpdate(final long soFar) {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    if (total > 0) {
                        progressBar.setPercentage(soFar / total);
                    } else {
                        progressBar.setPercentage(0.5); // this is illusion
                    }
                }
            });
        }

        @Override
        public void progressFinished() {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisible(false);
                }
            });
        }

        @Override
        public void progressAborted(final Exception e) {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisible(false);
                    displayError(e);
                }
            });
        }
    };
    private Window window = null;
    private PushButton resumeUpdatesButton;
    private PushButton pauseUpdatesButton;
    private PushButton reloadAssetsButton;
    private PushButton settingsButton;
    private PushButton importFromClipboardButton;
    private ApplicationData applicationData = new ApplicationData();

    private TextInput searchTextInput = null;
    private TreeView treeView = null;
    private Label statusLabel = null;
    private Meter progressBar = null;
    private Label timeToNextIndustryJobsUpdateLabel = null;
    private Label timeToNextAssetsUpdateLabel = null;
    private ItemTreeModel visibleTree = null;
    private ListButton filterListButton = null;
    private Checkbox hideScheduledBpcsCheckbox = null;
    private BlueprintInfoPaneHelper blueprintInfoPaneHelper = new BlueprintInfoPaneHelper();
    private JobPaneHelper jobPaneHelper = new JobPaneHelper();

    private final JobManager jobManager;
    private final AssetManager assetManager;
    private final MarketDataManager marketDataManager;
    private final IndustryJobsManager industryJobsManager;

    private final MyAssetListener assetListener = new MyAssetListener();
    private final MyMarketDataListener marketDataListener = new MyMarketDataListener();
    private final MyIndustryJobsListener industryJobsListener = new MyIndustryJobsListener();

    private Timer timer;

    public BpwApplication() {
        jobManager = new JobManager(applicationData);
        assetManager = new AssetManager(applicationData);
        marketDataManager = new MarketDataManager(applicationData);
        industryJobsManager = new IndustryJobsManager(applicationData);
    }

    public void startup(Display display, Map<String, String> properties) throws Exception {
        final BXMLSerializer bxmlSerializer = new BXMLSerializer();
        window = (Window) bxmlSerializer.readObject(BpwApplication.class, "application.bxml");
        window.open(display);

        // toolbar
        resumeUpdatesButton = (PushButton) bxmlSerializer.getNamespace().get("resumeUpdatesButton");
        pauseUpdatesButton = (PushButton) bxmlSerializer.getNamespace().get("pauseUpdatesButton");
        reloadAssetsButton = (PushButton) bxmlSerializer.getNamespace().get("reloadAssetsButton");
        settingsButton = (PushButton) bxmlSerializer.getNamespace().get("settingsButton");
        importFromClipboardButton = (PushButton) bxmlSerializer.getNamespace().get("importFromClipboardButton");

        treeView = (TreeView) bxmlSerializer.getNamespace().get("treeView");
        searchTextInput = (TextInput) bxmlSerializer.getNamespace().get("searchTextInput");
        hideScheduledBpcsCheckbox = (Checkbox) bxmlSerializer.getNamespace().get("hideScheduledBpcsCheckbox");
        statusLabel = (Label) bxmlSerializer.getNamespace().get("statusLabel");
        progressBar = (Meter) bxmlSerializer.getNamespace().get("progressBar");
        timeToNextIndustryJobsUpdateLabel = (Label) bxmlSerializer.getNamespace().get("timeToNextIndustryJobsUpdateLabel");
        timeToNextAssetsUpdateLabel = (Label) bxmlSerializer.getNamespace().get("timeToNextAssetsUpdateLabel");
        filterListButton = (ListButton) bxmlSerializer.getNamespace().get("filter");
        filterListButton.setSelectedIndex(applicationData.getUserSettings().getFilterIndex());

        jobPaneHelper.init(this, bxmlSerializer.getNamespace(), applicationData, jobManager);
        blueprintInfoPaneHelper.init(this, window, bxmlSerializer, jobPaneHelper, jobManager, applicationData);


        initializeListeners();

        initialize();
    }

    private void initializeListeners() {
        assetManager.getAssetsListeners().add(assetListener);

        assetManager.getProgressListeners().add(progressListener);
        marketDataManager.getMarketDataListeners().add(marketDataListener);
        marketDataManager.getMarketDataProgressUpdateListeners().add(progressListener);
        industryJobsManager.getIndustryJobsListeners().add(industryJobsListener);
        industryJobsManager.getIndustryJobsProgressUpdateListeners().add(progressListener);

        searchTextInput.getTextInputContentListeners().add(new TextInputContentListener.Adapter() {
            @Override
            public void textChanged(TextInput textInput) {
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        updateTree();
                    }
                });
            }
        });

        treeView.getTreeViewSelectionListeners().add(new TreeViewSelectionListener.Adapter() {
            @Override
            public void selectedPathsChanged(TreeView treeView, Sequence<Sequence.Tree.Path> previousSelectedPaths) {
                Sequence.Tree.Path path = treeView.getSelectedPath();
                if (path != null) {
                    String itemId = visibleTree.getNodeIdByPath(path);
                    if (!itemId.equals(blueprintInfoPaneHelper.getCurrentItemId())) {
                        EveItemNode item = applicationData.getAssets().getNodeById(itemId);
                        if (item.isBlueprint()) {
                            blueprintInfoPaneHelper.update(item);
                        }
                    }
                }
            }
        });

        treeView.getTreeViewBranchListeners().add(treeViewBranchListener);

        filterListButton.getListButtonSelectionListeners().add(new ListButtonSelectionListener.Adapter() {
            @Override
            public void selectedItemChanged(ListButton listButton, Object previousSelectedItem) {
                applicationData.getUserSettings().setFilterIndex(listButton.getSelectedIndex());
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        updateTree();
                    }
                });
                UserSettingsHelper.saveSettings(applicationData.getUserSettings());
            }
        });

        reloadAssetsButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        marketDataManager.updateMarketData();
                    }
                });
            }
        });

        resumeUpdatesButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                applicationData.getUserSettings().setUpdatePaused(false);
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        UserSettingsHelper.saveSettings(applicationData.getUserSettings());
                    }
                });
                startTimer();
            }
        });

        pauseUpdatesButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                applicationData.getUserSettings().setUpdatePaused(true);
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        UserSettingsHelper.saveSettings(applicationData.getUserSettings());
                    }
                });
                stopTimer();
            }
        });

        settingsButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                openSettingsDialog();
            }
        });

        importFromClipboardButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                openImportFromClipboardDialog();
            }
        });

        hideScheduledBpcsCheckbox.getButtonStateListeners().add(new ButtonStateListener() {
            @Override
            public void stateChanged(Button button, Button.State previousState) {
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        updateTree();
                    }
                });
            }
        });

        treeView.setMenuHandler(new MenuHandler.Adapter() {
            @Override
            public boolean configureContextMenu(Component component, Menu menu, int x, int y) {
                Sequence.Tree.Path nodePath = treeView.getNodeAt(y);
                final String nodeId = visibleTree.getNodeIdByPath(nodePath);
                final EveItemNode node = applicationData.getAssets().getNodeById(nodeId);
                if (node.isContainer()) {
                    Menu.Section menuSection = new Menu.Section();
                    menu.getSections().add(menuSection);
                    if (node.isContainerIgnored()) {
                        Menu.Item stopIgnoring = new Menu.Item("Stop ignoring");
                        stopIgnoring.setAction(new Action() {
                            @Override
                            public void perform(Component source) {
                                LocalItemInfo localItemInfo = node.setContainerIgnored(false);
                                ApplicationHelper.scheduleSave(localItemInfo, new Callback<Integer>() {
                                    @Override
                                    public void handle(Integer ignored) throws Exception {
                                        assetManager.reloadLocalAssets();
                                    }
                                });
                            }
                        });
                        menuSection.add(stopIgnoring);
                    } else {
                        Menu.Item ignore = new Menu.Item("Ignore");
                        ignore.setAction(new Action() {
                            @Override
                            public void perform(Component source) {
                                LocalItemInfo localItemInfo = node.setContainerIgnored(true);
                                ApplicationHelper.scheduleSave(localItemInfo, new Callback<Integer>() {
                                    @Override
                                    public void handle(Integer ignored) throws Exception {
                                        assetManager.reloadLocalAssets();
                                    }
                                });
                            }
                        });
                        menuSection.add(ignore);
                    }

                    if (node.isNameCustomisable()) {
                        Menu.Item rename = new Menu.Item("Rename...");
                        rename.setAction(new Action() {
                            @Override
                            public void perform(Component source) {
                                final String oldName = node.getCustomName();
                                openRenameDialog(oldName, new Callback<String>() {
                                    @Override
                                    public void handle(final String newName) {
                                        if (newName != null && !newName.equals(oldName)) {
                                            ApplicationHelper.getExecutor().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    LocalItemInfo localItemInfo = node.setCustomName(newName);
                                                    ApplicationHelper.scheduleSave(localItemInfo, new Callback<Integer>() {
                                                        @Override
                                                        public void handle(Integer ignored) throws SQLException, ParsingException, ParseException, IOException {
                                                            assetManager.reloadLocalAssets();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                        menuSection.add(rename);
                    }
                }
                return false;
            }
        });
    }

    private void openRenameDialog(String oldName, final Callback<String> executeOnSuccess) {
        try {
            final BXMLSerializer bxmlSerializer = new BXMLSerializer();
            final Dialog settingDialog = (Dialog) bxmlSerializer.readObject(BpwApplication.class, "dialog-rename.bxml");
            final TextInput nameInput = (TextInput) bxmlSerializer.getNamespace().get("nameInput");
            if (oldName != null) {
                nameInput.setText(oldName);
            }

            settingDialog.open(window, new DialogStateListener.Adapter() {
                @Override
                public void dialogClosed(Dialog dialog, boolean modal) {
                    if (dialog.getResult()) {
                        try {
                            executeOnSuccess.handle(nameInput.getText());
                        } catch (Exception e) {
                            log.error("Error", e);
                        }
                    }
                }
            });
        } catch (SerializationException | IOException e) {
            log.error("Unable to open dialog", e);
        }
    }

    private void initialize() {
        disableReloadActions();
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!Files.exists(Constants.applicationPath)) {
                        Files.createDirectory(Constants.applicationPath);
                    }

                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            statusLabel.setText("Checking for updates...");
                        }
                    });

                    if (HttpHelper.isFileUpdated(Constants.hostingPropertiesUrl, Constants.applicationPath, Constants.hostingPropertiesFileName)) {
                        HttpHelper.loadFileFromWeb(Constants.hostingPropertiesUrl, Constants.applicationPath, Constants.hostingPropertiesFileName, progressListener);
                    }

                    String eveDbUrl;
                    String typesZipUrl;
                    try (Reader reader = new FileReader(Constants.applicationPath.resolve(Constants.hostingPropertiesFileName).toFile())) {
                        ResourceBundle bundle = new PropertyResourceBundle(reader);
                        eveDbUrl = bundle.getString("eve.db.url");
                        typesZipUrl = bundle.getString("icons.zip.url");
                    }

                    if (HttpHelper.isFileUpdated(eveDbUrl, Constants.applicationPath, Constants.eveDbArchiveFileName)) {
                        ApplicationContext.queueCallback(new Runnable() {
                            @Override
                            public void run() {
                                statusLabel.setText("Downloading database...");
                            }
                        });
                        HttpHelper.loadFileFromWeb(eveDbUrl, Constants.applicationPath, Constants.eveDbArchiveFileName, progressListener);

                        ApplicationContext.queueCallback(new Runnable() {
                            @Override
                            public void run() {
                                statusLabel.setText("Extracting database...");
                            }
                        });

                        Path backup = Constants.applicationPath.resolve("bpwDB." + System.currentTimeMillis());
                        if (Files.exists(Constants.eveDbPath)) {
                            Files.move(Constants.eveDbPath, backup);
                        }
                        CompressionUtils.untargz(Constants.eveDbArchiveFilePath, Constants.applicationPath);
                    }

                    if (HttpHelper.isFileUpdated(typesZipUrl, Constants.applicationPath, Constants.typesArchiveFileName)) {
                        ApplicationContext.queueCallback(new Runnable() {
                            @Override
                            public void run() {
                                statusLabel.setText("Downloading icons...");
                            }
                        });
                        HttpHelper.loadFileFromWeb(typesZipUrl, Constants.applicationPath, Constants.typesArchiveFileName, progressListener);

                        ApplicationContext.queueCallback(new Runnable() {
                            @Override
                            public void run() {
                                statusLabel.setText("Extracting icons (this may take several minutes)...");
                            }
                        });

                        Path backup = Constants.applicationPath.resolve("Types." + System.currentTimeMillis());
                        if (Files.exists(Constants.eveItemsPath)) {
                            Files.move(Constants.eveItemsPath, backup);
                        }
                        CompressionUtils.unzip(Constants.typesArchiveFilePath, Constants.applicationPath);
                    }

                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            statusLabel.setText("Initializing...");
                        }
                    });

                    LocalDbHelper.getInstance().initializeDatabase();

                    if (applicationData.getUserSettings().getApiKeyInfo().getKey().isEmpty()) {
                        ApplicationContext.queueCallback(new Runnable() {
                            @Override
                            public void run() {
                                openSettingsDialog();
                            }
                        });
                    } else {
                        startLifecycle();
                    }

                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            statusLabel.setText("Ready");
                        }
                    });
                } catch (IOException | SQLException e) {
                    log.error("Initialization failed!", e);
                    System.exit(1);
                }
            }
        });
    }

    private void startLifecycle() {
        try {
            marketDataListener.enableComponentUpdates(false);
            assetListener.enableComponentUpdates(false);
            industryJobsListener.enableComponentUpdates(false);

            jobManager.loadJobs();
            industryJobsManager.updateIndustryJobs();
            assetManager.initializeAssets();
            marketDataManager.updateMarketData();

            marketDataListener.enableComponentUpdates(true);
            assetListener.enableComponentUpdates(true);
            industryJobsListener.enableComponentUpdates(true);

            updateTree();
            jobPaneHelper.update();

            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    if (!applicationData.getUserSettings().isUpdatePaused()) {
                        startTimer();
                    } else {
                        stopTimer();
                    }
                    enableReloadActions();
                }
            });
        } catch (IOException|SQLException|ParsingException ex) {
            log.error("Unable to start lifecycle", ex);
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    stopTimer();
                    Alert.alert(MessageType.ERROR, ex.getMessage(), window);
                }
            });
        }
    }

    private void startTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 0, 1000);
        resumeUpdatesButton.setEnabled(false);
        pauseUpdatesButton.setEnabled(true);

        final Date lastIndustryUpdate = applicationData.getIndustryJobs().getCurrentTime();
        final Date lastAssetsUpdate = applicationData.getAssets().getCurrentTime();
        timeToNextIndustryJobsUpdateLabel.setTooltipText("Last IndustryJobs update: " + DATE_FORMAT.format(lastIndustryUpdate));
        timeToNextAssetsUpdateLabel.setTooltipText("Last Assets update: " + DATE_FORMAT.format(lastAssetsUpdate));

    }

    private void stopTimer() {
        if(timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        } else {
            final Date lastIndustryUpdate = applicationData.getIndustryJobs().getCurrentTime();
            final Date lastAssetsUpdate = applicationData.getAssets().getCurrentTime();
            timeToNextIndustryJobsUpdateLabel.setTooltipText("Last IndustryJobs update: " + DATE_FORMAT.format(lastIndustryUpdate));
            timeToNextAssetsUpdateLabel.setTooltipText("Last Assets update: " + DATE_FORMAT.format(lastAssetsUpdate));
        }

        resumeUpdatesButton.setEnabled(true);
        pauseUpdatesButton.setEnabled(false);
        timeToNextIndustryJobsUpdateLabel.setText("--:--:--");
        timeToNextAssetsUpdateLabel.setText("--:--:--");
    }

    private void openImportFromClipboardDialog() {
        try {
            new ImportFromClipboardDialogHelper(window, applicationData).start(new Runnable() {
                @Override
                public void run() {
                    updateTree();
                    jobPaneHelper.update();
                    if (blueprintInfoPaneHelper.getCurrentItemId() != null) {
                        EveItemNode node = applicationData.getAssets().getNodeById(blueprintInfoPaneHelper.getCurrentItemId());
                        if (node.isBlueprint()) {
                            blueprintInfoPaneHelper.update(node);
                        }
                    }
                }
            });
        } catch (IOException|SerializationException e) {
            log.error("Unable to open copy-paste dialog", e);
        }
    }

    private void openSettingsDialog() {
        try {
            final BXMLSerializer bxmlSerializer = new BXMLSerializer();
            final Dialog settingDialog = (Dialog) bxmlSerializer.readObject(BpwApplication.class, "dialog-edit-key.bxml");
            final TextInput keyInput = (TextInput) bxmlSerializer.getNamespace().get("keyInput");
            final TextInput vCodeInput = (TextInput) bxmlSerializer.getNamespace().get("vCodeInput");

            final ApiKeyInfo keyInfo = applicationData.getUserSettings().getApiKeyInfo();
            keyInput.setText(keyInfo.getKey());
            vCodeInput.setText(keyInfo.getvCode());

            settingDialog.open(window, new DialogStateListener.Adapter() {
                @Override
                public void dialogClosed(Dialog dialog, boolean modal) {
                    if (dialog.getResult()) {
                        ApiKeyInfo newKeyInfo = new ApiKeyInfo(keyInput.getText(), vCodeInput.getText());
                        if (!newKeyInfo.equals(keyInfo)) {
                            final UserSettings userSettings = applicationData.getUserSettings();
                            userSettings.setApiKeyInfo(newKeyInfo);
                            ApplicationHelper.getExecutor().execute(new Runnable() {
                                @Override
                                public void run() {
                                    UserSettingsHelper.saveSettings(userSettings);
                                    startLifecycle();
                                    correctionInSeconds = 10; // reset correction
                                }
                            });
                        }
                    } else {
                        settingsButton.setEnabled(true);
                    }
                }
            });
        } catch (SerializationException | IOException e) {
            log.error("Unable to open dialog", e);
        }
    }

    private void enableReloadActions() {
        reloadAssetsButton.setEnabled(true);
        settingsButton.setEnabled(true);
        importFromClipboardButton.setEnabled(true);
    }

    private void disableReloadActions() {
        reloadAssetsButton.setEnabled(false);
        settingsButton.setEnabled(false);
        importFromClipboardButton.setEnabled(false);
    }


    void updateTree() {
        try (PerformanceLogger ignored = new PerformanceLogger(log, "updateTree")) {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Updating tree...");
                }
            });

            final String searchString = searchTextInput.getText().toLowerCase();
            Object selectedItem = filterListButton.getSelectedItem();
            TypeFilter filter = null == selectedItem ? TypeFilter.All : TypeFilter.valueOf(selectedItem.toString());

            final String selectedNodeId = visibleTree != null ? visibleTree.getNodeIdByPath(treeView.getSelectedPath()) : null;

            visibleTree = ItemTreeModel.create(applicationData, searchString.toLowerCase(), filter, hideScheduledBpcsCheckbox.isSelected());

            ApplicationContext.queueCallback(new Runnable() {
                public void run() {
                    if (searchTextInput.getText().equalsIgnoreCase(searchString)) {
                        treeView.setTreeData(visibleTree.getPivotTree());
                        if (selectedNodeId != null) {
                            Sequence.Tree.Path pathByItemId = visibleTree.getPathByItemId(selectedNodeId);
                            if (pathByItemId != null) {
                                treeView.setSelectedPath(pathByItemId);
                            }
                        }
                        treeView.getTreeViewBranchListeners().remove(treeViewBranchListener);
                        for (Sequence.Tree.Path expandedPath : visibleTree.getExpandedPaths()) {
                            treeView.setBranchExpanded(expandedPath, true);
                        }
                        treeView.getTreeViewBranchListeners().add(treeViewBranchListener);
                        statusLabel.setText("Ready");
                    }
                }
            });
        }
    }

    private int correctionInSeconds = 10;

    private void updateTimers() {
        if (applicationData.getUserSettings().getApiKeyInfo().getKey().isEmpty()
                || applicationData.getUserSettings().getApiKeyInfo().getvCode().isEmpty()) {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    stopTimer();
                }
            });
            return;
        }

        final Date now = new Date();
        long secondsToNextIndustryUpdate = applicationData.getIndustryJobs().secondsToNextUpdate(now) + correctionInSeconds;
        long secondsToNextAssetsUpdate = applicationData.getAssets().secondsToNextUpdate(now) + correctionInSeconds;

        final String timeToNextIndustryUpdateText = formatTimeLeft(secondsToNextIndustryUpdate);
        final String timeToNextAssetUpdateText = formatTimeLeft(secondsToNextAssetsUpdate > secondsToNextIndustryUpdate ? secondsToNextAssetsUpdate : secondsToNextIndustryUpdate);

        ApplicationContext.queueCallback(new Runnable() {
            @Override
            public void run() {
                timeToNextIndustryJobsUpdateLabel.setText(timeToNextIndustryUpdateText);
                timeToNextAssetsUpdateLabel.setText(timeToNextAssetUpdateText);
            }
        });

        try {
            boolean industryJobsUpdated = false;
            if (secondsToNextIndustryUpdate < 0) {
                if (!(industryJobsUpdated = industryJobsManager.updateIndustryJobs())) {
                    correctionInSeconds = (int) Math.max(-1*secondsToNextIndustryUpdate + 10, correctionInSeconds);
                    log.info("Updated correction in second: " + correctionInSeconds);
                } else {
                    final Date lastIndustryUpdate = applicationData.getIndustryJobs().getCurrentTime();
                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            timeToNextIndustryJobsUpdateLabel.setTooltipText("Last IndustryJobs update: " + DATE_FORMAT.format(lastIndustryUpdate));
                        }
                    });
                }
            }

            if (industryJobsUpdated && secondsToNextAssetsUpdate < 0) {
                if(assetManager.reloadAssets()) {
                    final Date lastAssetsUpdate = applicationData.getAssets().getCurrentTime();
                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            timeToNextAssetsUpdateLabel.setTooltipText("Last Assets update: " + DATE_FORMAT.format(lastAssetsUpdate));
                        }
                    });
                }
            }
        } catch (SQLException|ParsingException|IOException ex) {
            log.error("Error updating data from the server", ex);
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    stopTimer();
                }
            });
        }
    }

    private String formatTimeLeft(long totalSeconds) {
        totalSeconds = Math.max(totalSeconds, 0);

        int seconds = (int) (totalSeconds % 60);
        int minutes = (int) ((totalSeconds / 60) % 60);
        int hours = (int) ((totalSeconds / 3600) % 60);

        return MessageFormat.format("{0,number,00}:{1,number,00}:{2,number,00}", hours, minutes, seconds);
    }

    private void displayError(Exception e) {
        log.error(e);
        Alert.alert(e.getMessage(), window);
    }

    public boolean shutdown(boolean optional) {
        if (window != null) {
            window.close();
        }

        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
        return false;
    }

    public void suspend() {
    }

    public void resume() {
    }

    private class MyAssetListener implements AssetManager.AssetListener {
        private boolean componentUpdatesEnabled = true;

        @Override
        public void assetsReloadStarted() {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Fetching assets...");
                    disableReloadActions();
                }
            });
        }

        @Override
        public void assetsUpdateStarted() {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Loading assets...");
                }
            });
        }

        @Override
        public void assetsUpdated(boolean updated) {
            if(componentUpdatesEnabled && updated) {
                updateTree();
                jobPaneHelper.update();
                blueprintInfoPaneHelper.update();
            }
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Ready");
                    enableReloadActions();
                }
            });
        }

        @Override
        public void assetsReloadError(final Exception e) {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    enableReloadActions();
                    displayError(e);
                }
            });
        }

        public void enableComponentUpdates(boolean componentUpdatesEnabled) {
            this.componentUpdatesEnabled = componentUpdatesEnabled;
        }
    }

    private class MyMarketDataListener implements MarketDataManager.MarketDataListener {
        private boolean componentUpdatesEnabled = true;

        @Override
        public void marketDataUpdateStarted() {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Updating market data...");
                }
            });
        }

        @Override
        public void marketDataUpdateFinished() {
            if(componentUpdatesEnabled) {
                jobPaneHelper.update();
                blueprintInfoPaneHelper.update();
            }
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Ready");
                }
            });
        }

        @Override
        public void marketDataUpdateFailed(final Exception e) {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Ready");
                    displayError(e);
                }
            });
        }

        public void enableComponentUpdates(boolean componentUpdatesEnabled) {
            this.componentUpdatesEnabled = componentUpdatesEnabled;
        }
    }

    private class MyIndustryJobsListener implements IndustryJobsManager.IndustryJobsListener {
        private boolean componentUpdatesEnabled = true;

        @Override
        public void industryJobsUpdateStarted() {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Updating industry jobs...");
                }
            });
        }

        @Override
        public void industryJobsUpdateFinished(boolean updated) {
            if (componentUpdatesEnabled && updated) {
                jobPaneHelper.update();
            }

            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText("Ready");
                }
            });
        }

        @Override
        public void industryJobsUpdateFailed(Exception e) {
            log.error("Error updating industry jobs", e);
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    // that's right not going to display errors - short cache is going to
                    // supply us with information next time around.
                    statusLabel.setText("Ready");
                }
            });
        }

        public void enableComponentUpdates(boolean componentUpdatesEnabled) {
            this.componentUpdatesEnabled = componentUpdatesEnabled;
        }
    }

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            updateTimers();
        }
    }
}
