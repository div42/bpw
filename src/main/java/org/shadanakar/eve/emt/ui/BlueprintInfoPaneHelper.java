/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pivot.beans.BXMLSerializer;
import org.apache.pivot.serialization.SerializationException;
import org.apache.pivot.wtk.*;
import org.apache.pivot.wtk.Button;
import org.apache.pivot.wtk.Dialog;
import org.apache.pivot.wtk.Label;
import org.apache.pivot.wtk.Window;
import org.shadanakar.eve.emt.db.EveDbHelper;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.helpers.BlueprintHelper;
import org.shadanakar.eve.emt.helpers.ImageLoader;
import org.shadanakar.eve.emt.helpers.JobManager;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.controller.ManufacturingController;
import org.shadanakar.eve.emt.ui.model.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BlueprintInfoPaneHelper {
    private static final Log log = LogFactory.getLog(BlueprintInfoPaneHelper.class);
    public static final String COLOR_GREEN = "#00FF00";
    private static final String COLOR_RED = "#FF0000";
    private static final String COLOR_YELLOW = "#FFFF00";
    private static final String COLOR_BLACK = "#000000";
    private static final String COLOR_WHITE = "#FFFFFF";

    private EveItemNode currentNode = null;
    private ApplicationData applicationData = null;
    private ImageView blueprintImageView = null;
    private Label blueprintNameLabel = null;
    private ImageView productImageView = null;
    private Label productNameLabel = null;
    private Label materialLevel = null;
    private Label productivityLevel = null;
    private Label wastageFactor = null;
    private Label numberOfRuns = null;
    private TableView manufacturingTableView = null;
    private TableView inventionTableView = null;
    private TableView inventManufacturingTableView = null;
    private PushButton addInventionJobButton = null;
    private PushButton addManufacturingJobButton = null;
    private JobManager jobManager = null;
    private Label manufacturingInfoLabel = null;
    private Label profitInfoLabel = null;
    private Label inventionInfoLabel = null;
    private Label inventionProfitInfoLabel = null;
    private FillPane inventionTab = null;
    private TabPane bpTabPane = null;
    private ListButton inventionOutcomesListButton = null;
    private ListButton inventionDecryptorListButton = null;
    private ListButton inventionBaseElementListButton = null;
    private PushButton decryptorAutoSelectButton = null;
    private Label inventoryInfoLabel = null;

    private PushButton editBlueprintStats = null;
    private Window window;
    private BpwApplication parent;
    private JobPaneHelper jobPaneHelper = null;

    private final org.apache.pivot.collections.List<InventionOutcomeListItem> outcomeListItems = new org.apache.pivot.collections.ArrayList<>();
    private final org.apache.pivot.collections.List<InventionDecryptorListItem> decryptorListItems = new org.apache.pivot.collections.ArrayList<>();
    private final org.apache.pivot.collections.List<EveTypeListItem> baseTypeListItems = new org.apache.pivot.collections.ArrayList<>();

    public void init(final BpwApplication parent, Window window, BXMLSerializer bxmlSerializer, final JobPaneHelper jobPaneHelper, final JobManager jobManager, final ApplicationData data) {
        this.jobPaneHelper = jobPaneHelper;
        this.jobManager = jobManager;
        this.parent = parent;
        this.window = window;
        this.applicationData = data;
        blueprintImageView = (ImageView) bxmlSerializer.getNamespace().get("blueprintImageView");
        blueprintNameLabel = (Label) bxmlSerializer.getNamespace().get("blueprintNameLabel");
        productImageView = (ImageView) bxmlSerializer.getNamespace().get("productImageView");
        productNameLabel = (Label) bxmlSerializer.getNamespace().get("productNameLabel");
        materialLevel = (Label) bxmlSerializer.getNamespace().get("materialLevel");
        productivityLevel = (Label) bxmlSerializer.getNamespace().get("productivityLevel");
        wastageFactor = (Label) bxmlSerializer.getNamespace().get("wastageFactor");
        numberOfRuns = (Label) bxmlSerializer.getNamespace().get("numberOfRuns");
        inventionTab = (FillPane) bxmlSerializer.getNamespace().get("inventionTab");
        bpTabPane = (TabPane) bxmlSerializer.getNamespace().get("bpTabPane");
        manufacturingInfoLabel = (Label) bxmlSerializer.getNamespace().get("manufacturingInfoLabel");
        profitInfoLabel = (Label) bxmlSerializer.getNamespace().get("profitInfoLabel");
        inventionInfoLabel = (Label) bxmlSerializer.getNamespace().get("inventionInfoLabel");
        inventionProfitInfoLabel = (Label) bxmlSerializer.getNamespace().get("inventionProfitInfoLabel");
        manufacturingTableView = (TableView) bxmlSerializer.getNamespace().get("manufacturingTableView");
        inventionTableView = (TableView) bxmlSerializer.getNamespace().get("inventionTableView");
        inventManufacturingTableView = (TableView) bxmlSerializer.getNamespace().get("inventManufacturingTableView");
        addInventionJobButton = (PushButton) bxmlSerializer.getNamespace().get("addInventionJobButton");
        addManufacturingJobButton = (PushButton) bxmlSerializer.getNamespace().get("addManufacturingJobButton");
        inventionOutcomesListButton = (ListButton) bxmlSerializer.getNamespace().get("inventionOutcomesListButton");
        inventionDecryptorListButton = (ListButton) bxmlSerializer.getNamespace().get("inventionDecryptorListButton");
        inventionBaseElementListButton = (ListButton) bxmlSerializer.getNamespace().get("inventionBaseElementListButton");
        inventoryInfoLabel = (Label) bxmlSerializer.getNamespace().get("inventoryInfoLabel");
        decryptorAutoSelectButton = (PushButton) bxmlSerializer.getNamespace().get("decryptorAutoSelectButton");

        editBlueprintStats = (PushButton) bxmlSerializer.getNamespace().get("editBlueprintStats");

        addInventionJobButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            jobManager.addInventionJobsByType(currentNode.getId(),
                                    ((InventionDecryptorListItem) inventionDecryptorListButton.getSelectedItem()).getDecryptor(),
                                    ((EveTypeListItem) inventionBaseElementListButton.getSelectedItem()).getType());
                            parent.updateTree();
                            ApplicationContext.queueCallback(new Runnable() {
                                @Override
                                public void run() {
                                    jobPaneHelper.update();
                                }
                            });
                        } catch (SQLException ex) {
                            log.error("Unable to save jobs", ex);
                        }
                    }
                });
            }
        });

        addManufacturingJobButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                if (currentNode.isBlueprintOriginal()) {
                    openBpoManufacturingStatsDialog();
                } else {
                    addBlueprintCopyManufacturingJobs();
                }
            }
        });

        editBlueprintStats.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                openEditBlueprintStatsDialog();
            }
        });

        inventionDecryptorListButton.getListButtonSelectionListeners().add(new ListButtonSelectionListener.Adapter(){
            @Override
            public void selectedItemChanged(ListButton listButton, Object previousSelectedItem) {
                InventionDecryptorListItem currentSelectedItem = (InventionDecryptorListItem) listButton.getSelectedItem();
                if (currentSelectedItem != previousSelectedItem && inventionBaseElementListButton.getSelectedIndex() != -1) {
                    updateTypeBaseListItems();
                    updateInventionTab();
                }
            }
        });

        inventionBaseElementListButton.getListButtonSelectionListeners().add(new ListButtonSelectionListener.Adapter() {
            @Override
            public void selectedItemChanged(ListButton listButton, Object previousSelectedItem) {
                EveTypeListItem currentSelectedItem = (EveTypeListItem) listButton.getSelectedItem();
                if (currentSelectedItem != previousSelectedItem && inventionDecryptorListButton.getSelectedIndex() != -1) {
                    updateDecryptorListButton();
                    updateInventionTab();
                }
            }
        });

        inventionOutcomesListButton.getListButtonSelectionListeners().add(new ListButtonSelectionListener.Adapter(){
            @Override
            public void selectedItemChanged(ListButton listButton, Object previousSelectedItem) {
                InventionOutcomeListItem currentSelectedItem = (InventionOutcomeListItem) listButton.getSelectedItem();
                if (currentSelectedItem != previousSelectedItem) {
                    updateInventionTab();
                }
            }
        });

        decryptorAutoSelectButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                decryptorAutoSelectButton.setEnabled(false);
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        selectBestCombination();
                    }
                });
            }
        });
    }

    private void addBlueprintCopyManufacturingJobs() {
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    jobManager.addManufacturingJobsByCopyType(currentNode.getId());
                    parent.updateTree();
                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            jobPaneHelper.update();
                        }
                    });
                } catch (SQLException ex) {
                    log.error("Unable to save jobs", ex);
                }
            }
        });
    }

    private void selectBestCombination() {
        class Combination {
            int decryptorIndex;
            int baseItemIndex;
            float profitPerHour;

            Combination(int decryptorIndex, int baseItemIndex, float profitPerHour) {
                this.decryptorIndex = decryptorIndex;
                this.baseItemIndex = baseItemIndex;
                this.profitPerHour = profitPerHour;
            }
        }

        try {
            List<Combination> combinations = new ArrayList<>();
            for (int decryptorIndex = 0, decryptorListLength=decryptorListItems.getLength(); decryptorIndex < decryptorListLength; ++decryptorIndex) {
                InventionDecryptorListItem decryptorListItem = decryptorListItems.get(decryptorIndex);
                for (int baseItemIndex = 0, itemListLength=baseTypeListItems.getLength(); baseItemIndex < itemListLength; baseItemIndex++) {
                    EveTypeListItem baseTypeListItem = baseTypeListItems.get(baseItemIndex);
                    InventionProfitCalculator calculator = new InventionProfitCalculator(decryptorListItem.getDecryptor(), baseTypeListItem.getType(), currentNode.getType()).invoke();
                    combinations.add(new Combination(decryptorIndex, baseItemIndex, calculator.getProfitPerHourSell()));
                }
            }

            // select max
            int decryptorIndexToSelect = 0;
            int baseItemIndexToSelect = 0;
            float currentProfitPerHour = combinations.get(0).profitPerHour; // not good
            for (Combination combination : combinations) {
                if (combination.profitPerHour > currentProfitPerHour) {
                    currentProfitPerHour = combination.profitPerHour;
                    decryptorIndexToSelect = combination.decryptorIndex;
                    baseItemIndexToSelect = combination.baseItemIndex;
                }
            }

            final int finalDecryptorIndexToSelect = decryptorIndexToSelect;
            final int finalBaseItemIndexToSelect = baseItemIndexToSelect;
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    inventionBaseElementListButton.setSelectedIndex(finalBaseItemIndexToSelect);
                    inventionDecryptorListButton.setSelectedIndex(finalDecryptorIndexToSelect);
                    decryptorAutoSelectButton.setEnabled(true);
                }
            });
        } catch (SQLException e) {
            log.error("Error while calculating things... ", e);
        }
    }


    private void openEditBlueprintStatsDialog() {
//        if (currentNode.isBlueprintCopy() && currentNode.getType().getBlueprint().getTechLevel() == 2 ) {
//            openEditT2BlueprintStatsDialog();
//        } else {
        openEditT1BlueprintStatsDialog();
//        }
    }

//    private void openEditT2BlueprintStatsDialog() {
//        try {
//            final BXMLSerializer bxmlSerializer = new BXMLSerializer();
//            final Dialog editBlueprintDialog = (Dialog) bxmlSerializer.readObject(BpwApplication.class, "dialog-blueprint-stats-t2.bxml");
//
//            final LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfo(currentNode.getTypeId());
//
//        } catch (SerializationException|IOException|SQLException ex) {
//            log.error("Unable to open dialog", ex);
//        }
//    }

    private void openBpoManufacturingStatsDialog() {
        try {
            final BXMLSerializer bxmlSerializer = new BXMLSerializer();
            final Dialog editBlueprintDialog = (Dialog) bxmlSerializer.readObject(BpwApplication.class, "dialog-bpo-manufacturing-stats.bxml");
            final TextInput runsTextInput = (TextInput) bxmlSerializer.getNamespace().get("runsTextInput");

            editBlueprintDialog.open(window, new DialogStateListener.Adapter() {
                @Override
                public void dialogClosed(Dialog dialog, boolean modal) {
                    if (dialog.getResult()) {
                        try {
                            final int runs = Integer.parseInt(runsTextInput.getText().trim());
                            if (runs > 0) {
                                ApplicationHelper.getExecutor().execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            jobManager.addManufacturingJobsByOriginalType(currentNode.getId(), runs);
                                            parent.updateTree();
                                            ApplicationContext.queueCallback(new Runnable() {
                                                @Override
                                                public void run() {
                                                    jobPaneHelper.update();
                                                }
                                            });
                                        } catch (SQLException ex) {
                                            log.error("Unable to save jobs", ex);
                                        }
                                    }
                                });
                            }
                        } catch (NumberFormatException ex) {
                            log.error("Not a number: " + runsTextInput.getText().trim(), ex);
                        }
                    }
                }

            });
        } catch (SerializationException | IOException e) {
            log.error("Unable to open dialog", e);
        }
    }

    private void openEditT1BlueprintStatsDialog() {
        try {
            final BXMLSerializer bxmlSerializer = new BXMLSerializer();
            final Dialog editBlueprintDialog = (Dialog) bxmlSerializer.readObject(BpwApplication.class, "dialog-blueprint-stats.bxml");
            final TextInput materialLevelTextInput = (TextInput) bxmlSerializer.getNamespace().get("materialLevelTextInput");
            final TextInput productivityLevelTextInput = (TextInput) bxmlSerializer.getNamespace().get("productivityLevelTextInput");
            final TextInput runsTextInput = (TextInput) bxmlSerializer.getNamespace().get("runsTextInput");

            final LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfo(currentNode.getTypeId());

            final Integer materialLevel = localBlueprintInfo.getMaterialLevel();
            final Integer productivityLevel = localBlueprintInfo.getProductivityLevel();
            final Integer runs = localBlueprintInfo.getRuns();
            if(materialLevel != null) {
                materialLevelTextInput.setText(materialLevel.toString());
            }

            if(productivityLevel != null) {
                productivityLevelTextInput.setText(productivityLevel.toString());
            }

            if (currentNode.isBlueprintCopy() && currentNode.getType().getBlueprint().getTechLevel() == 2 ) {
                runsTextInput.setEnabled(true);
                if(runs != null) {
                    runsTextInput.setText(runs.toString());
                }
            }

            editBlueprintDialog.open(window, new DialogStateListener.Adapter() {
                @Override
                public void dialogClosed(Dialog dialog, boolean modal) {
                    if (dialog.getResult()) {
                        String mlValue = materialLevelTextInput.getText().trim();
                        Integer newMaterialLevel = mlValue.isEmpty() ? null : new Integer(mlValue);
                        String plValue = productivityLevelTextInput.getText().trim();
                        Integer newProductivityLevel = plValue.isEmpty() ? null : new Integer(plValue);
                        String runsValue = runsTextInput.getText().trim();
                        Integer newRuns = runsValue.isEmpty() ? null : new Integer(runsValue);

                        Integer currentMaterialLevel = localBlueprintInfo.getMaterialLevel();
                        Integer currentProductivityLevel = localBlueprintInfo.getProductivityLevel();
                        Integer currentRuns = localBlueprintInfo.getRuns();
                        if (areNotEqual(newMaterialLevel, currentMaterialLevel) ||
                                areNotEqual(newProductivityLevel, currentProductivityLevel) ||
                                areNotEqual(newRuns, currentRuns)) {
                            localBlueprintInfo.setMaterialLevel(newMaterialLevel);
                            localBlueprintInfo.setProductivityLevel(newProductivityLevel);
                            localBlueprintInfo.setRuns(newRuns);
                            ApplicationHelper.getExecutor().execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        LocalDbHelper.getInstance().saveLocalBlueprintInfo(localBlueprintInfo);
                                        parent.updateTree();
                                        ApplicationContext.queueCallback(new Runnable() {
                                            @Override
                                            public void run() {
                                                update(currentNode);
                                            }
                                        });
                                    } catch (SQLException e) {
                                        log.error("Unable to save blueprint info", e);
                                    }
                                }
                            });
                        }
                    }
                }

                private boolean areNotEqual(Integer newMaterialLevel, Integer currentMaterialLevel) {
                    return (newMaterialLevel != null && !newMaterialLevel.equals(currentMaterialLevel)) ||
                            (newMaterialLevel == null && currentMaterialLevel != null);
                }
            });
        } catch (SerializationException | IOException | SQLException e) {
            log.error("Unable to open dialog", e);
        }
    }

    public void update() {
        if (currentNode != null) {
            ApplicationContext.queueCallback(new Runnable() {
                @Override
                public void run() {
                    update(currentNode);
                }
            });
        }
    }

    public void update(EveItemNode node) {
        if (currentNode != node) {
            currentNode = node;
            baseTypeListItems.clear();
            inventionBaseElementListButton.setVisible(false);
            decryptorListItems.clear();
        }
        final EveType blueprintType = node.getType();
        final EveBlueprint blueprint = blueprintType.getBlueprint();
        final EveType productType = blueprint.getProductType();
        try {
            blueprintImageView.setImage(ImageLoader.getInstance().getImage64x64Path(blueprintType.getId()).toUri().toURL());
            productImageView.setImage(ImageLoader.getInstance().getImage64x64Path(productType.getId()).toUri().toURL());
        } catch (MalformedURLException e) {
            log.error("Unable to load image", e);
        }

        LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfoNoException(currentNode.getTypeId());

        blueprintNameLabel.setText(node.getDisplayName(false, localBlueprintInfo));
        productNameLabel.setText(productType.getName() + " [" + productType.getPortionSize() + "]");

        LevelInfo materialLevelInfo = currentNode.calculateMaterialLevel(localBlueprintInfo);
        LevelInfo productivityLevelInfo = currentNode.calculateProductivityLevel(localBlueprintInfo);
        materialLevel.setText("" + materialLevelInfo.getValue());
        materialLevel.setTooltipText(materialLevelInfo.getSource().name());
        productivityLevel.setText("" + productivityLevelInfo.getValue());
        productivityLevel.setTooltipText(productivityLevelInfo.getSource().name());
        wastageFactor.setText("" + (100*node.calculateWasteFactor(localBlueprintInfo))+"%");
        numberOfRuns.setText("" + node.calculateRuns(localBlueprintInfo));

        updateInventionToolbar(new Runnable() {
            @Override
            public void run() {
                updateManufacturingTab();
                updateInventionTab();

                addInventionJobButton.setEnabled(true);
                addManufacturingJobButton.setEnabled(true);
                editBlueprintStats.setEnabled(true);
                decryptorAutoSelectButton.setEnabled(true);
            }
        });

    }

    private void updateInventionToolbar(final Runnable doAfterUpdate) {
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                outcomeListItems.clear();
                try {
                    List<EveType> outcomes = EveDbHelper.getInstance().findInventionOutputs(currentNode.getType());
                    for (EveType outcome : outcomes) {
                        outcomeListItems.add(new InventionOutcomeListItem(
                                ImageLoader.getInstance().loadImage16x16(outcome.getBlueprint().getProductType().getId()),
                                outcome.getBlueprint().getProductType().getName(), outcome));
                    }
                } catch (SQLException e) {
                    log.error("Error loading outcomes for " + currentNode.getType().toString(), e);
                }

                ApplicationContext.queueCallback(new Runnable() {
                    @Override
                    public void run() {
                        inventionOutcomesListButton.setListData(outcomeListItems);
                        if (!outcomeListItems.isEmpty()) {
                            inventionOutcomesListButton.setSelectedIndex(0);
                            inventionOutcomesListButton.setEnabled(true);
                        }
                    }
                });

                updateDecryptorListButton();
                updateTypeBaseListItems();

                ApplicationContext.queueCallback(new Runnable() {
                    @Override
                    public void run() {
                        doAfterUpdate.run();
                    }
                });
            }
        });
    }

    private void updateTypeBaseListItems() {
        if (outcomeListItems.isEmpty()) {
            return;
        }
        try {
            String pattern = "{0}. {1} prob={2,number,#,###.00}%";

            InventionDecryptorListItem decryptorListItem = (InventionDecryptorListItem) inventionDecryptorListButton.getSelectedItem();
            EveDecryptorInfo decryptor = decryptorListItem == null ? null : decryptorListItem.getDecryptor();

            if (baseTypeListItems.isEmpty()) {

                {
                    String name = MessageFormat.format(pattern, 0, "No Base Item",
                            100 * BlueprintHelper.calculateInventionProbability(currentNode.getType(), decryptor, null));

                    baseTypeListItems.add(new EveTypeListItem(null, name, null));
                }
                List<EveType> baseItemTypes = EveDbHelper.getInstance().findMetaGroupItemsByParentTypeId(currentNode.getType().getBlueprint().getProductType().getId());

                Collections.sort(baseItemTypes, new Comparator<EveType>() {
                    @Override
                    public int compare(EveType o1, EveType o2) {
                        return new Integer(o1.getMetaLevel()).compareTo(o2.getMetaLevel());
                    }
                });

                for (EveType baseType : baseItemTypes) {
                    float probability = BlueprintHelper.calculateInventionProbability(currentNode.getType(), decryptor, baseType);
                    String name = MessageFormat.format(pattern, baseType.getMetaLevel(), baseType.getName(), probability * 100);
                    baseTypeListItems.add(new EveTypeListItem(ImageLoader.getInstance().loadImage16x16(baseType.getId()), name, baseType));
                }

                ApplicationContext.queueCallback(new Runnable() {
                    @Override
                    public void run() {
                        inventionBaseElementListButton.setListData(baseTypeListItems);
                        inventionBaseElementListButton.setSelectedIndex(0);
                        if (baseTypeListItems.getLength() > 1) {
                            inventionBaseElementListButton.setVisible(true);
                        }
                    }
                });
            } else {
                for (EveTypeListItem baseTypeListItem : baseTypeListItems) {
                    String typeName = baseTypeListItem.getType() == null ? "No Base Item" : baseTypeListItem.getType().getName();
                    int metaLevel = baseTypeListItem.getType() == null ? 0 : baseTypeListItem.getType().getMetaLevel();
                    float probability = BlueprintHelper.calculateInventionProbability(currentNode.getType(), decryptor, baseTypeListItem.getType());
                    baseTypeListItem.setText(MessageFormat.format(pattern, metaLevel, typeName, probability * 100));
                }

                ApplicationContext.queueCallback(new Runnable() {
                    @Override
                    public void run() {
                        inventionBaseElementListButton.repaint();
                    }
                });
            }
        } catch (SQLException e) {
            log.error("Error loading applicable decryptors for " + currentNode.getType().toString(), e);
        }
    }

    private void updateDecryptorListButton() {
        try {
            if (!outcomeListItems.isEmpty()) {
                String pattern = "{0} [{1}, {2}, {3}] prob={4,number,#,###.00}%";
                InventionOutcomeListItem outcomeListItem = outcomeListItems.get(0); // doesn't really matter which one so...
                EveType outcomeBlueprintType = outcomeListItem.getOutcomeBlueprintType();

                EveTypeListItem baseItemType = (EveTypeListItem) inventionBaseElementListButton.getSelectedItem();
                EveType baseType = baseItemType == null ? null : baseItemType.getType();

                if(decryptorListItems.isEmpty()) {
                    {
                        String name = MessageFormat.format(pattern, "Without Decryptor", -4, -4,
                                outcomeBlueprintType.getBlueprint().getMaxProductionLimit() / 10, 100 * BlueprintHelper.calculateInventionProbability(currentNode.getType(), null, baseType));

                        decryptorListItems.add(new InventionDecryptorListItem(null, name, null));
                    }

                    List<EveDecryptorInfo> decryptors = EveDbHelper.getInstance().findApplicableDecryptors(currentNode.getType());
                    for (EveDecryptorInfo decryptor : decryptors) {
                        LocalBlueprintInfo localBlueprintInfo = BlueprintHelper.calculateT2LocalBlueprintInfo(outcomeBlueprintType, decryptor);
                        float probability = BlueprintHelper.calculateInventionProbability(currentNode.getType(), decryptor, baseType);
                        String name = MessageFormat.format(pattern, decryptor.getDecryptorType().getName(),
                                localBlueprintInfo.getMaterialLevel(), localBlueprintInfo.getProductivityLevel(), localBlueprintInfo.getRuns(), (probability * 100));
                        decryptorListItems.add(new InventionDecryptorListItem(
                                ImageLoader.getInstance().loadImage16x16(decryptor.getDecryptorType().getId()), name, decryptor));
                    }

                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            inventionDecryptorListButton.setListData(decryptorListItems);
                            if (!decryptorListItems.isEmpty()) {
                                inventionDecryptorListButton.setSelectedIndex(0);
                                inventionDecryptorListButton.setEnabled(true);
                            }
                        }
                    });
                } else {
                    for (InventionDecryptorListItem decryptorListItem : decryptorListItems) {
                        String typeName = decryptorListItem.getDecryptor() == null ? "Without Decryptor" : decryptorListItem.getDecryptor().getDecryptorType().getName();
                        LocalBlueprintInfo localBlueprintInfo = BlueprintHelper.calculateT2LocalBlueprintInfo(outcomeBlueprintType, decryptorListItem.getDecryptor());
                        float probability = BlueprintHelper.calculateInventionProbability(currentNode.getType(), decryptorListItem.getDecryptor(), baseType);
                        String name = MessageFormat.format(pattern, typeName,
                                localBlueprintInfo.getMaterialLevel(), localBlueprintInfo.getProductivityLevel(), localBlueprintInfo.getRuns(), (probability * 100));
                        decryptorListItem.setText(name);
                    }
                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            inventionDecryptorListButton.repaint();
                        }
                    });
                }
            }
        } catch (SQLException e) {
            log.error("Error loading applicable decryptors for " + currentNode.getType().toString(), e);
        }
    }

    private void updateInventionTab() {
        final EveType blueprintType = currentNode.getType();
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    log.info("Updating invention tab....");
                    if (outcomeListItems.isEmpty()) {
                        ApplicationContext.queueCallback(new Runnable() {
                            @Override
                            public void run() {
                                inventionTableView.clear();
                                inventionTab.setEnabled(false);
                                if (bpTabPane.getSelectedIndex()==1) {
                                    bpTabPane.setSelectedIndex(0);
                                }
                            }
                        });
                        return;
                    }

                    if (inventionBaseElementListButton.getSelectedItem() == null
                            || inventionDecryptorListButton.getSelectedItem() == null) {
                        return;
                    }

                    EveDecryptorInfo selectedDecryptor = ((InventionDecryptorListItem)inventionDecryptorListButton.getSelectedItem()).getDecryptor();
                    EveType selectedBaseItemType = ((EveTypeListItem)inventionBaseElementListButton.getSelectedItem()).getType();

                    InventionProfitCalculator inventionProfitCalculator = new InventionProfitCalculator(selectedDecryptor, selectedBaseItemType, blueprintType).invoke();

                    float itemPriceSell = inventionProfitCalculator.getItemPriceSell();
                    float itemPriceBuy = inventionProfitCalculator.getItemPriceBuy();
                    float manufacturingCost = inventionProfitCalculator.getManufacturingCost();
                    float inventionCostPerItem = inventionProfitCalculator.getInventionCostPerItem();
                    float profitSellPrice = inventionProfitCalculator.getProfitSellPrice();
                    float profitBuyPrice = inventionProfitCalculator.getProfitBuyPrice();
                    float profitPerHourSell = inventionProfitCalculator.getProfitPerHourSell();
                    float profitPerHourBuy = inventionProfitCalculator.getProfitPerHourBuy();
                    final org.apache.pivot.collections.List<BlueprintTableLine> inventionBillOfMaterials = inventionProfitCalculator.getInventionBillOfMaterials();
                    final org.apache.pivot.collections.List<BlueprintTableLine> manufacturingBillOfMaterials = inventionProfitCalculator.getManufacturingBillOfMaterials();

                    final boolean isManufacturingTime = inventionProfitCalculator.isManufacturingTime();

                    final String inventionInfoText = MessageFormat.format(
                            "Product price: {0,number,#,###.00}/{1,number,#,###.00}, Manufacturing cost: {2,number,#,###.00}, Invention cost per item: {3,number,#,###.00}",
                            itemPriceSell, itemPriceBuy, manufacturingCost, inventionCostPerItem);

                    final String profitInfoText = MessageFormat.format("Per item: {0,number,#,###.00}/{1,number,#,###.00}, Per hour: {2,number,#,###.00}/{3,number,#,###.00}{4}",
                            profitSellPrice, profitBuyPrice, profitPerHourSell, profitPerHourBuy, isManufacturingTime ? "" : " (invention time is on critical path)");

                    final String backgroundColorCode;
                    final String textColorCode;


                    if(profitBuyPrice > 0f) {
                        backgroundColorCode = COLOR_GREEN;
                        textColorCode = COLOR_BLACK;
                    } else if (profitSellPrice < 0f) {
                        backgroundColorCode = COLOR_RED;
                        textColorCode = COLOR_WHITE;
                    } else {
                        backgroundColorCode = COLOR_YELLOW;
                        textColorCode = COLOR_BLACK;
                    }

                    // easy part... lets update the table
                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            inventionTableView.setTableData(inventionBillOfMaterials);
                            inventManufacturingTableView.setTableData(manufacturingBillOfMaterials);
                            inventionInfoLabel.setText(inventionInfoText);
                            inventionProfitInfoLabel.setText(profitInfoText);
                            inventionProfitInfoLabel.getStyles().put("color", textColorCode);
                            inventionProfitInfoLabel.getStyles().put("backgroundColor", backgroundColorCode);
                            inventionTab.setEnabled(true);
                        }
                    });
                } catch (SQLException e) {
                    log.error("Error loading materials and stuff.. ", e);
                }
            }
        });
    }

    private void updateManufacturingTab() {
        final EveType blueprintType = currentNode.getType();
        final EveBlueprint blueprint = blueprintType.getBlueprint();
        final EveType productType = blueprint.getProductType();

        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    log.info("Updating manufacturing tab....");
                    float itemPriceBuy = applicationData.getMarketData().estimatePrice(MarketOrderType.Buy, productType, productType.getPortionSize());
                    float itemPriceSell = applicationData.getMarketData().estimatePrice(MarketOrderType.Sell, productType, productType.getPortionSize());

                    final org.apache.pivot.collections.List<BlueprintTableLine> resultedList = new org.apache.pivot.collections.ArrayList<>();

                    LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfo(currentNode.getTypeId());

                    ManufacturingController.TimeAndPriceData manufacturingCostAndTime = ManufacturingController.compileBillOfMaterialsTableData(localBlueprintInfo, null, 1.0f, blueprintType, applicationData, resultedList);

                    float manufacturingPrice = manufacturingCostAndTime.getManufacturingPrice();
                    float manufacturingTimeSec = manufacturingCostAndTime.getManufacturingTimeSec();

                    float profitSellPrice = itemPriceSell - manufacturingPrice;
                    float profitBuyPrice = itemPriceBuy - manufacturingPrice;
                    float profitPerHourSell = profitSellPrice*(3600/manufacturingTimeSec);
                    float profitPerHourBuy = profitBuyPrice*(3600/manufacturingTimeSec);

                    final String manufacturingInfoText = MessageFormat.format(
                            "Product price: {0,number,#,###.00}/{1,number,#,###.00}, Manufacturing cost: {2,number,#,###.00}",
                            itemPriceSell, itemPriceBuy, manufacturingPrice);

                    final String profitInfoText = MessageFormat.format("Per item: {0,number,#,###.00}/{1,number,#,###.00}, Per hour: {2,number,#,###.00}/{3,number,#,###.00}",
                            profitSellPrice, profitBuyPrice, profitPerHourSell, profitPerHourBuy);

                    final String backgroundColorCode;
                    final String textColorCode;

                    if(profitBuyPrice > 0f) {
                        backgroundColorCode = COLOR_GREEN;
                        textColorCode = COLOR_BLACK;
                    } else if (profitSellPrice < 0f) {
                        backgroundColorCode = COLOR_RED;
                        textColorCode = COLOR_WHITE;
                    } else {
                        backgroundColorCode = COLOR_YELLOW;
                        textColorCode = COLOR_BLACK;
                    }
                    // easy part... lets update the table
                    ApplicationContext.queueCallback(new Runnable() {
                        @Override
                        public void run() {
                            manufacturingTableView.clear();
                            manufacturingTableView.setTableData(resultedList);
                            manufacturingInfoLabel.setText(manufacturingInfoText);
                            profitInfoLabel.setText(profitInfoText);
                            profitInfoLabel.getStyles().put("color", textColorCode);
                            profitInfoLabel.getStyles().put("backgroundColor", backgroundColorCode);
                        }
                    });

                } catch (SQLException e) {
                    log.error("Error loading materials and stuff.. ", e);
                }

            }
        });
    }

    public String getCurrentItemId() {
        return currentNode != null ? currentNode.getId() : null;
    }

    private class InventionProfitCalculator {
        private EveDecryptorInfo selectedDecryptor;
        private EveType selectedBaseItemType;
        private EveType blueprintType;
        private org.apache.pivot.collections.List<BlueprintTableLine> inventionBillOfMaterials;
        private float inventionCostPerItem;
        private org.apache.pivot.collections.List<BlueprintTableLine> manufacturingBillOfMaterials;
        private float manufacturingCost;
        private float itemPriceBuy;
        private float itemPriceSell;
        private float profitSellPrice;
        private float profitBuyPrice;
        private float profitPerHourSell;
        private float profitPerHourBuy;
        private boolean isManufacturingTime;

        public InventionProfitCalculator(EveDecryptorInfo selectedDecryptor, EveType selectedBaseItemType, EveType blueprintType) {
            this.selectedDecryptor = selectedDecryptor;
            this.selectedBaseItemType = selectedBaseItemType;
            this.blueprintType = blueprintType;
        }

        public org.apache.pivot.collections.List<BlueprintTableLine> getInventionBillOfMaterials() {
            return inventionBillOfMaterials;
        }

        public float getInventionCostPerItem() {
            return inventionCostPerItem;
        }

        public org.apache.pivot.collections.List<BlueprintTableLine> getManufacturingBillOfMaterials() {
            return manufacturingBillOfMaterials;
        }

        public float getManufacturingCost() {
            return manufacturingCost;
        }

        public float getItemPriceBuy() {
            return itemPriceBuy;
        }

        public float getItemPriceSell() {
            return itemPriceSell;
        }

        public float getProfitSellPrice() {
            return profitSellPrice;
        }

        public float getProfitBuyPrice() {
            return profitBuyPrice;
        }

        public float getProfitPerHourSell() {
            return profitPerHourSell;
        }

        public float getProfitPerHourBuy() {
            return profitPerHourBuy;
        }

        public InventionProfitCalculator invoke() throws SQLException {
            JobGroup fakeGroup = jobManager.createInventionJobGroup(blueprintType, selectedDecryptor, selectedBaseItemType);
            List<EveMaterialType> inventionMaterials = applicationData.calculateInventionMaterials(fakeGroup);

            inventionBillOfMaterials = new org.apache.pivot.collections.ArrayList<>();

            float inventionCost = 0f;

            // Just add what is left in extra collection
            for (EveMaterialType type : inventionMaterials) {
                if (type.getDamagePerJob() != 0) {
                    BlueprintTableLine line = new BlueprintTableLine();

                    line.setName(type.getMaterialType().getName());
                    line.setIcon(ImageLoader.getInstance().loadImage16x16(type.getMaterialType().getId()));
                    line.setExtraQuantity(type.getQuantity()*type.getDamagePerJob());
                    float marketPrice = applicationData.getMarketData().estimatePrice(MarketOrderType.Sell, type.getMaterialType(), line.getQuantity());
                    line.setMarketPrice(marketPrice);
                    inventionCost += marketPrice*line.getQuantity();

                    inventionBillOfMaterials.add(line);
                }
            }

            EveType selectedT2Blueprint = ((InventionOutcomeListItem) inventionOutcomesListButton.getSelectedItem()).getOutcomeBlueprintType();
            EveType t2ProductType = selectedT2Blueprint.getBlueprint().getProductType();

            LocalBlueprintInfo localBlueprintInfo = BlueprintHelper.calculateT2LocalBlueprintInfo(selectedT2Blueprint, selectedDecryptor);

            float inventionProbability = BlueprintHelper.calculateInventionProbability(blueprintType, selectedDecryptor, selectedBaseItemType);

            inventionCostPerItem = inventionCost / (localBlueprintInfo.getRuns() * inventionProbability * t2ProductType.getPortionSize());

            manufacturingBillOfMaterials = new org.apache.pivot.collections.ArrayList<>();
            ManufacturingController.TimeAndPriceData manufacturingCostAndTime = ManufacturingController.compileBillOfMaterialsTableData(localBlueprintInfo, blueprintType, inventionProbability, selectedT2Blueprint, applicationData, manufacturingBillOfMaterials);

            manufacturingCost = inventionCostPerItem + manufacturingCostAndTime.getManufacturingPrice();
            float timeSec = Math.max(manufacturingCostAndTime.getInventionAndCopyTimeSec(), manufacturingCostAndTime.getManufacturingTimeSec());

            itemPriceBuy = applicationData.getMarketData().estimatePrice(MarketOrderType.Buy, t2ProductType, t2ProductType.getPortionSize());
            itemPriceSell = applicationData.getMarketData().estimatePrice(MarketOrderType.Sell, t2ProductType, t2ProductType.getPortionSize());

            profitSellPrice = itemPriceSell - manufacturingCost;
            profitBuyPrice = itemPriceBuy - manufacturingCost;

            profitPerHourSell = profitSellPrice * (3600 / timeSec);
            profitPerHourBuy = profitBuyPrice * (3600 / timeSec);

            isManufacturingTime = manufacturingCostAndTime.getManufacturingTimeSec() >= manufacturingCostAndTime.getInventionAndCopyTimeSec();

            return this;
        }

        private boolean isManufacturingTime() {
            return isManufacturingTime;
        }
    }
}
