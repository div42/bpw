/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui;

import org.codehaus.plexus.archiver.tar.TarGZipUnArchiver;
import org.codehaus.plexus.archiver.zip.ZipUnArchiver;
import org.codehaus.plexus.logging.Logger;
import org.codehaus.plexus.logging.console.ConsoleLogger;

import java.nio.file.Path;

public class CompressionUtils {
    public static void untargz(Path archiveFilePath, Path path) {
        final TarGZipUnArchiver ua = new TarGZipUnArchiver();
        ua.enableLogging(new ConsoleLogger(Logger.LEVEL_INFO, "CompressorLog"));
        ua.setSourceFile(archiveFilePath.toFile());
        ua.setDestDirectory(path.toFile());
        ua.extract();
    }

    public static void unzip(Path archiveFilePath, Path path) {
        final ZipUnArchiver ua = new ZipUnArchiver();
        ua.enableLogging(new ConsoleLogger(Logger.LEVEL_INFO, "CompressorLog"));
        ua.setSourceFile(archiveFilePath.toFile());
        ua.setDestDirectory(path.toFile());
        ua.extract();
    }
}
