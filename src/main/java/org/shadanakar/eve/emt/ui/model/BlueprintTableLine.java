/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.model;

import org.apache.pivot.wtk.media.Image;

@SuppressWarnings("UnusedDeclaration")
public class BlueprintTableLine {
    private Image icon;
    private String name;
    private Integer baseQuantity = 0;
    private Integer wasteQuantity = 0;
    private Float extraQuantity = 0f;
    private boolean consumed = true;
    private Float marketPrice = null;
    private Float manufacturingPrice = null;


    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getQuantity() {
        return baseQuantity+wasteQuantity+extraQuantity;
    }

    public Integer getBaseQuantity() {
        return baseQuantity;
    }

    public void setBaseQuantity(Integer baseQuantity) {
        this.baseQuantity = baseQuantity;
    }

    public Integer getWasteQuantity() {
        return wasteQuantity;
    }

    public void setWasteQuantity(Integer wasteQuantity) {
        this.wasteQuantity = wasteQuantity;
    }

    public Float getExtraQuantity() {
        return extraQuantity;
    }

    public void setExtraQuantity(Float extraQuantity) {
        this.extraQuantity = extraQuantity;
    }

    public boolean isConsumed() {
        return consumed;
    }

    public void setConsumed(boolean consumed) {
        this.consumed = consumed;
    }

    public Float getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Float marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Float getManufacturingPrice() {
        return manufacturingPrice;
    }

    public void setManufacturingPrice(Float manufacturingPrice) {
        this.manufacturingPrice = manufacturingPrice;
    }

    public float getMinimalPrice() {
        return manufacturingPrice == null ? marketPrice : Math.min(marketPrice, manufacturingPrice);
    }
}
