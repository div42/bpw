/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.model;

import org.shadanakar.eve.emt.model.EveType;
import org.shadanakar.eve.emt.model.LocalBlueprintInfo;

import java.text.MessageFormat;

@SuppressWarnings("UnusedDeclaration")
public class ImportFromClipboardTableLine {
    // 0-name
    // 1-me
    // 2-pe
    // 3-runs
    private static final String pattern = "{0} [{1} {2} {3}]";
    private final EveType type;
    private final LocalBlueprintInfo oldBlueprintInfo;
    private final LocalBlueprintInfo newBlueprintInfo;
    private boolean isSelected;

    public ImportFromClipboardTableLine(EveType type, LocalBlueprintInfo oldBlueprintInfo, LocalBlueprintInfo newBlueprintInfo) {
        this.type = type;
        this.oldBlueprintInfo = oldBlueprintInfo;
        this.newBlueprintInfo = newBlueprintInfo;
        this.isSelected = true;
    }

    public boolean getApplyCheckbox() {
        return isSelected;
    }

    public void setApplyCheckbox(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getOriginalName() {
        return MessageFormat.format(pattern, type.getName(), oldBlueprintInfo.getMaterialLevel(), oldBlueprintInfo.getProductivityLevel(), oldBlueprintInfo.getRuns());
    }

    public String getModifiedName() {
        return MessageFormat.format(pattern, type.getName(), newBlueprintInfo.getMaterialLevel(), newBlueprintInfo.getProductivityLevel(), newBlueprintInfo.getRuns());
    }


    public LocalBlueprintInfo getNewBlueprintInfo() {
        return newBlueprintInfo;
    }
}
