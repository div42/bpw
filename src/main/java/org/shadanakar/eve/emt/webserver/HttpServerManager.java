/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.webserver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;
import org.shadanakar.eve.emt.webserver.servlets.BillOfMaterialsServlet;
import org.shadanakar.eve.emt.webserver.servlets.ImageServlet;
import org.shadanakar.eve.emt.webserver.servlets.LoadBillOfMaterialsServlet;
import org.shadanakar.eve.emt.webserver.servlets.RootServlet;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class HttpServerManager {
    private static final Log log = LogFactory.getLog(HttpServerManager.class);
    private static final int PORT = 7183;

    public static HttpServerManager getInstance() { return Holder._instance; }

    private Server server = null;

    private HttpServerManager() {}

    public String getUrl() {
        String host = "localhost";
        try {
            for (Enumeration<NetworkInterface> e= NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
                NetworkInterface candidate = e.nextElement(); // I know its like there was never java 2
                if (candidate.isUp() && !candidate.isLoopback()&& !candidate.getInterfaceAddresses().isEmpty()) {
                    host = candidate.getInterfaceAddresses().get(0).getAddress().getHostAddress();
                    break;
                }
            }
        } catch (SocketException e) {
            log.error("everything is wrong :/", e);
        }

        return "http://"+host+":" + PORT;
    }

    public boolean isRunning() {
        return server != null && (server.isRunning() || server.isStarted() || server.isStarting());
    }

    public void start() {
        try {
            if (server != null && server.isRunning()) {
                log.error("Server is already running... check your code!");
            }

            server = new Server(PORT);
            ServletHandler handler = new ServletHandler();
            server.setHandler(handler);

            handler.addServletWithMapping(RootServlet.class, "/");
            handler.addServletWithMapping(BillOfMaterialsServlet.class, "/bill");
            handler.addServletWithMapping(LoadBillOfMaterialsServlet.class, "/loadBill");
            handler.addServletWithMapping(ImageServlet.class, "/img");

            server.start();
        } catch (Exception e) {
            log.error("Got this while stopping web-server... ouch (not sure what to do!)", e);
        }
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            log.error("Got this while stopping web-server... ouch (not sure what to do!)", e);
        }
    }

    private static class Holder {
        private static final HttpServerManager _instance = new HttpServerManager();
    }
}
