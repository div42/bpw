/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.webserver.servlets;

import org.apache.commons.compress.utils.IOUtils;
import org.shadanakar.eve.emt.helpers.ImageLoader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

public class ImageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer typeId = new Integer(req.getParameter("id"));
        Path image = ImageLoader.getInstance().getImage32x32Path(typeId);

        try (InputStream imageInputStream = new FileInputStream(image.toFile())) {
            resp.setContentType("image/png");
            resp.setContentLength((int) image.toFile().length());
            IOUtils.copy(imageInputStream, resp.getOutputStream());
        }
    }

    @Override
    protected long getLastModified(HttpServletRequest req) {
        Integer typeId = new Integer(req.getParameter("id"));
        return ImageLoader.getInstance().getImage32x32Path(typeId).toFile().lastModified();
    }
}
