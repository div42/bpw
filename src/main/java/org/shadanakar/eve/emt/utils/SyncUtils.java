/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.utils;

import nu.xom.ParsingException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantLock;

public class SyncUtils {
    public static interface Callback<T> {
        T run() throws ParsingException, SQLException, IOException;
    }


    public static void executeLocked(ReentrantLock lock, Runnable action) {
        try {
            lock.lock();
            action.run();
        } finally {
            lock.unlock();
        }
    }

    public static <T> T executeLocked(ReentrantLock lock, Callback<T> action) throws ParsingException, SQLException, IOException {
        try {
            lock.lock();
            return action.run();
        } finally {
            lock.unlock();
        }
    }

    private SyncUtils() {
    }
}
