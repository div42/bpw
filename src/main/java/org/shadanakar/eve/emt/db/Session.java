/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * $Id$
 */
public class Session implements AutoCloseable {
    private static final Log log = LogFactory.getLog(Session.class);

    final Connection connection;

    public Session(Connection connection) {
        this(connection, true);
    }

    public Session(Connection connection, boolean autocommit) {
        this.connection = connection;
        try {
            this.connection.setAutoCommit(autocommit);
        } catch (SQLException ex) {
            log.error("Was unable to set autocommit to " + autocommit, ex);
        }
    }

    @Override
    public void close() throws SQLException {
        try { connection.rollback(); } catch (SQLException ex) { log.error("Unable to rollback: ", ex); }
        try { connection.close(); } catch (SQLException ex) { log.error("Unable to close connection: ", ex); }
    }


//    public <T> T uniqueResult(String query, Object ... params) throws SQLException {
//        try(PreparedStatement stmt = connection.prepareStatement(query)) {
//            int position = 0;
//            for (Object param : params) {
//                stmt.setObject(++position, param);
//            }
//
//            try (ResultSet rs = stmt.executeQuery()) {
//                T result = null;
//                if (rs.next()) {
//                    result = (T) rs.getObject(1);
//                }
//
//                if (rs.next()) {
//                    throw new SQLException("non-unique result from query '" + query + "'");
//                }
//                return result;
//            }
//        }
//    }

    public void executeQuery(String query, QueryCallback queryCallback, Object... params) throws SQLException {
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            int position = 0;
            for (Object param : params) {
                stmt.setObject(++position, param);
            }

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    queryCallback.processRow(rs);
                }
            }
        }
    }

    public int executeUpdate(String updateSql, Object... params) throws SQLException {
        try (PreparedStatement stmt = connection.prepareStatement(updateSql)) {
            int position = 0;
            for (Object param : params) {
                stmt.setObject(++position, param);
            }

            return stmt.executeUpdate();
        }
    }

    public <T> T uniqueResult(String query, Object... params) throws SQLException {
        final ResultHolder<T> result = new ResultHolder<>();
        executeQuery(query, new QueryCallback() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                if(result.value == null) {
                    //noinspection unchecked
                    result.value = (T) rs.getObject(1);
                } else {
                    throw new SQLException("Non-unique result", "S-NNRS");
                }
            }
        }, params);

        return result.value;
    }

    public void commit() throws SQLException {
        connection.commit();
    }

    public <T> List<T> listResult(String query, Object... params) throws SQLException {
        final List<T> result = new ArrayList<>();
        executeQuery(query, new QueryCallback() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                //noinspection unchecked
                result.add((T) rs.getObject(1));
            }
        }, params);

        return result;
    }

    public interface QueryCallback {
        void processRow(ResultSet rs) throws SQLException;
    }

    public static class ResultHolder<T> {
        public T value;
    }
}
