/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.helpers.Constants;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.utils.PerformanceLogger;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * $Id$
 */
public class EveDbHelper {
    private static final Log log = LogFactory.getLog(EveDbHelper.class);
    static {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (ClassNotFoundException e) {
            log.fatal("Derby embedded driver was not found, something wrong with the installation!", e);
        }
    }

    private EveDbHelper() {}

    public static EveDbHelper getInstance() {
        return Holder._instance;
    }

    public EveType getType(final Integer typeId) throws SQLException {
        if (!typesCache.containsKey(typeId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final Session.ResultHolder<EveType> result = new Session.ResultHolder<>();
                    session.executeQuery("select t.typeID, t.groupID, t.typeName, t.mass, t.volume, t.portionSize, t.marketGroupId, dt.valueInt as metaLevel, dt.valueFloat as metaLevelFloat " +
                            "from invTypes t " +
                            "left outer join dgmTypeAttributes dt on t.typeID=dt.typeID and dt.attributeID=633 " +
                            "where t.typeId=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            Integer id = rs.getInt(++pos);
                            Integer groupId = rs.getInt(++pos);
                            String name = rs.getString(++pos);
                            Double mass = rs.getDouble(++pos);
                            Double volume = rs.getDouble(++pos);
                            Integer portionSize = rs.getInt(++pos);
                            Integer marketGroupId = rs.getInt(++pos);
                            int metaLevel = rs.getInt(++pos);
                            int metaLevelFloat = (int) rs.getFloat(++pos);
                            EveGroup group = getGroup(groupId);
                            EveBlueprint blueprint = null;
                            if (group.isBlueprint()) {
                                blueprint = getBlueprint(typeId);
                            }
                            result.value = new EveType(id, group, name, mass, volume, portionSize, blueprint, marketGroupId, Math.max(metaLevelFloat, metaLevel));
                        }
                    }, typeId);

                    if (result.value != null) {
                        typesCache.put(typeId, result.value);
                    }
                }
            });
        }

        return typesCache.get(typeId);
    }

    private EveBlueprint getBlueprint(final Integer typeId) throws SQLException {
        if (!blueprintCache.containsKey(typeId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final Session.ResultHolder<EveBlueprint> result = new Session.ResultHolder<>();
                    session.executeQuery("select productTypeID, productionTime, techLevel, researchCopyTime, researchTechTime, productivityModifier, materialModifier, " +
                            "wasteFactor, maxProductionLimit, chanceOfInvention from invBlueprintTypes where blueprintTypeID=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            Integer productTypeId = rs.getInt(++pos);
                            Integer productionTime = rs.getInt(++pos);
                            Integer techLevel = rs.getInt(++pos);
                            Integer researchCopyTime = rs.getInt(++pos);
                            Integer researchTechTime = rs.getInt(++pos);
                            Integer productivityModifier = rs.getInt(++pos);
                            Integer materialModifier = rs.getInt(++pos);
                            Integer wasteFactor = rs.getInt(++pos);
                            Integer maxProductionLimit = rs.getInt(++pos);
                            float chanceOfInvention = rs.getFloat(++pos);

                            EveType productType = getType(productTypeId);
                            result.value = new EveBlueprint(typeId, productType, productionTime, techLevel, researchCopyTime, researchTechTime, productivityModifier,
                                    materialModifier, wasteFactor, maxProductionLimit, chanceOfInvention);
                        }
                    }, typeId);

                    if (result.value != null) {
                        blueprintCache.put(typeId, result.value);
                    }
                }
            });
        }

        return blueprintCache.get(typeId);
    }

    private EveGroup getGroup(final Integer groupId) throws SQLException {
        if (!groupsCache.containsKey(groupId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final Session.ResultHolder<EveGroup> group = new Session.ResultHolder<>();
                    session.executeQuery("select groupID, categoryID, groupName from invGroups where groupID=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            Integer groupId = rs.getInt(++pos);
                            Integer categoryId = rs.getInt(++pos);
                            String name = rs.getString(++pos);
                            EveCategory category = getCategory(categoryId);
                            group.value = new EveGroup(groupId, category, name);
                        }
                    }, groupId);

                    if (group.value != null) {
                        groupsCache.put(groupId, group.value);
                    }
                }
            });
        }

        return groupsCache.get(groupId);
    }

    private EveCategory getCategory(final Integer categoryId) throws SQLException {
        if (!categoryCache.containsKey(categoryId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final Session.ResultHolder<EveCategory> res = new Session.ResultHolder<>();
                    session.executeQuery("select categoryID, categoryName from invCategories where categoryID=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            Integer categoryId = rs.getInt(++pos);
                            String name = rs.getString(++pos);
                            res.value = new EveCategory(categoryId, name);
                        }
                    }, categoryId);

                    if (res.value != null) {
                        categoryCache.put(categoryId, res.value);
                    }
                }
            });
        }

        return categoryCache.get(categoryId);
    }

    public EveLocation getLocation(final Integer locationId) throws SQLException {
        if (!locationCache.containsKey(locationId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    String query;
                    Integer locId = locationId;
                    if (locationId > 66000000 && locationId <= 66014933) {
                        query = "select stationID, stationName from staStations where stationID=?";
                        locId = locationId-6000001;
                    } else if (locationId > 60000000 && locationId <= 61000000 ) {
                        query = "SELECT stationID, stationName FROM staStations where stationID=?";
                    } else {
                        query = "select itemID, itemName from mapDenormalize where itemID=?";
                    }

                    final Session.ResultHolder<EveLocation> res = new Session.ResultHolder<>();

                    session.executeQuery(query, new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            Long id = rs.getLong(++pos);
                            String name = rs.getString(++pos);
                            res.value = new EveLocation(id, name);
                        }
                    }, locId);

                    if (res.value != null) {
                        locationCache.put(locationId, res.value);
                    }
                }
            });
        }

        return locationCache.get(locationId);
    }

    /**
     * Please be careful here.. this method expects product type id NOT blueprint.
     * @param productTypeId product type id
     * @return list of materials
     * @throws SQLException
     */
    public List<EveMaterialType> getMaterialTypes(final Integer productTypeId) throws SQLException {
        if (!materialTypesCache.containsKey(productTypeId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final List<EveMaterialType> res = new ArrayList<>();
                    session.executeQuery("select typeID, materialTypeID, quantity from invTypeMaterials where typeID=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            Integer id = rs.getInt(++pos);
                            Integer materialTypeId = rs.getInt(++pos);
                            Integer quantity = rs.getInt(++pos);
                            EveType materialType = getType(materialTypeId);
                            res.add(new EveMaterialType(id, materialType, quantity, 1, false));
                        }
                    }, productTypeId);

                    // it might be empty
                    materialTypesCache.put(productTypeId, Collections.unmodifiableList(res));
                }
            });
        }

        return materialTypesCache.get(productTypeId);
    }

    public List<EveMaterialType> getExtraMaterialTypes(final Integer blueprintTypeId) throws SQLException {
        if (!extraMaterialTypesCache.containsKey(blueprintTypeId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final List<EveMaterialType> res = new ArrayList<>();
                    session.executeQuery("select typeID, requiredTypeID, quantity, damagePerJob, recycle from ramTypeRequirements where typeID=? and activityID=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            int id = rs.getInt(++pos);
                            Integer materialTypeId = rs.getInt(++pos);
                            int quantity = rs.getInt(++pos);
                            float damagePerJob = rs.getFloat(++pos);
                            boolean recycle = rs.getBoolean(++pos);
                            EveType materialType = getType(materialTypeId);
                            res.add(new EveMaterialType(id, materialType, quantity, damagePerJob, recycle));
                        }
                    }, blueprintTypeId, Constants.ACTIVITYID_MANUFACTURING);

                    // it might be empty
                    extraMaterialTypesCache.put(blueprintTypeId, Collections.unmodifiableList(res));
                }
            });
        }

        return extraMaterialTypesCache.get(blueprintTypeId);
    }

    public List<EveMaterialType> getInventionMaterialTypes(final Integer blueprintTypeId) throws SQLException {
        if (!inventionMaterialTypesCache.containsKey(blueprintTypeId)) {
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final List<EveMaterialType> res = new ArrayList<>();
                    session.executeQuery("select typeID, requiredTypeID, quantity, damagePerJob, recycle from ramTypeRequirements where typeID=? and activityID=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            int id = rs.getInt(++pos);
                            Integer materialTypeId = rs.getInt(++pos);
                            int quantity = rs.getInt(++pos);
                            float damagePerJob = rs.getFloat(++pos);
                            boolean recycle = rs.getBoolean(++pos);
                            EveType materialType = getType(materialTypeId);
                            res.add(new EveMaterialType(id, materialType, quantity, damagePerJob, recycle));
                        }
                    }, blueprintTypeId, Constants.ACTIVITYID_INVENTION);

                    // it might be empty
                    inventionMaterialTypesCache.put(blueprintTypeId, Collections.unmodifiableList(res));
                }
            });
        }

        return inventionMaterialTypesCache.get(blueprintTypeId);
    }

    private void loadSafely(DbCallback dbCallback) throws SQLException {
        derbyConnectionLock.lock();
        try {
            try (Session session = new Session(DriverManager.getConnection(Constants.derbyEveDbConnectionString))) {
                dbCallback.doInSession(session);
            }
        } finally {
            derbyConnectionLock.unlock();
        }
    }

    public EveType findBlueprintTypeByItemTypeId(final Integer itemTypeId) throws SQLException {
        final Session.ResultHolder<Integer> blueprintTypeId = new Session.ResultHolder<>();
        loadSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                blueprintTypeId.value = session.uniqueResult("select blueprintTypeId from invBlueprintTypes where productTypeId=?", itemTypeId);
            }
        });

        if (blueprintTypeId.value != null) {
            return getType(blueprintTypeId.value);
        } else {
            return null;
        }
    }

    public List<EveType> findInventionOutputs(final EveType blueprintType) throws SQLException {
        final List<Integer> resultedBlueprintIds = new ArrayList<>();
        loadSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                session.executeQuery("select bp.blueprintTypeID from invMetaTypes mt " +
                        "inner join invTypes pt on pt.typeID=mt.parentTypeID " +
                        "inner join invTypes t on t.typeID=mt.typeID " +
                        "inner join invBlueprintTypes bp on bp.productTypeID=mt.typeID " +
                        "where parentTypeID=? and metaGroupID=2", new Session.QueryCallback() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        resultedBlueprintIds.add(rs.getInt(1));
                    }
                }, blueprintType.getBlueprint().getProductType().getId());
            }
        });

        List<EveType> results = new ArrayList<>(resultedBlueprintIds.size());
        for (Integer blueprintId : resultedBlueprintIds) {
            results.add(getType(blueprintId));
        }

        return results;
    }

    public List<EveDecryptorInfo> findApplicableDecryptors(final EveType blueprintType) throws SQLException {
        final List<Integer> decryptorTypeIds = new ArrayList<>();
        loadSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                session.executeQuery("SELECT d.typeID " +
                        "FROM ramTypeRequirements t, dgmTypeAttributes a, invTypes d " +
                        "WHERE t.typeID=? " +
                        "AND t.activityID =8 " +
                        "AND a.typeID=t.requiredTypeID " +
                        "AND d.groupID= a.valueInt " +
                        "AND a.attributeID=1115", new Session.QueryCallback() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        decryptorTypeIds.add(rs.getInt(1));
                    }
                }, blueprintType.getId());
            }
        });

        List<EveDecryptorInfo> decryptorInfoList = new ArrayList<>(decryptorTypeIds.size());

        for (Integer decryptorTypeId : decryptorTypeIds) {
            decryptorInfoList.add(getDecryptorInfo(decryptorTypeId));
        }

        return decryptorInfoList;
    }

    public List<Integer> findAllDecryptorIds() throws SQLException {
        final List<Integer> result = new ArrayList<>();
        loadSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                session.executeQuery("select t.typeID from invTypes t\n" +
                        "inner join invGroups g on g.groupID=t.groupID\n" +
                        "where g.categoryID=?", new Session.QueryCallback() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        result.add(rs.getInt(1));
                    }
                }, Constants.CATEGORYID_DECRYPTOR);
            }
        });

        return result;
    }


    private static final int ATTRIBUTE_ID_PROBABILITY_MULTIPLIER = 1112;
    private static final int ATTRIBUTE_ID_ME_MODIFIER = 1113;
    private static final int ATTRIBUTE_ID_PE_MODIFIER = 1114;
    private static final int ATTRIBUTE_ID_MAX_RUN_MODIFIER = 1124;

    public EveType findTypeByName(final String typeName) throws SQLException {
        try (PerformanceLogger ignored= new PerformanceLogger(log, "findTypeByName("+typeName+")")) {
            final Session.ResultHolder<Integer> typeId = new Session.ResultHolder<>();
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    typeId.value = session.uniqueResult("select typeID from invTypes where typeName=?", typeName);
                }
            });

            return typeId.value != null ? getType(typeId.value) : null;
        }
    }

    public List<EveType> findMetaGroupItemsByParentTypeId(final int parentTypeId) throws SQLException {
        try (PerformanceLogger ignored = new PerformanceLogger(log, "findMetaGroupItemsByParentTypeId("+parentTypeId+")")) {
            List<EveType> result = new ArrayList<>();
            final Session.ResultHolder<List<Integer>> resultHolder = new Session.ResultHolder<>();
            // todo: should we cache the list?
            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    resultHolder.value = session.listResult("select typeID from invMetaTypes where parentTypeID=? and metaGroupID=1", parentTypeId);
                }
            });

            for (Integer id : resultHolder.value) {
                result.add(getType(id));
            }

            return result;
        }
    }

    private static class MutableDecryptorInfo {
        private EveType decryptorType;
        private float probabilityMultiplier;
        private float meModifier;
        private float maxRunModifier;
        private float peModifier;

        public MutableDecryptorInfo(EveType decryptorType) {
            this.decryptorType = decryptorType;
        }

        public EveDecryptorInfo toEveDecryptorInfo() {
            return new EveDecryptorInfo(decryptorType, probabilityMultiplier, meModifier, peModifier, maxRunModifier);
        }

        public void setProbabilityMultiplier(float probabilityMultiplier) {
            this.probabilityMultiplier = probabilityMultiplier;
        }

        public void setMeModifier(float meModifier) {
            this.meModifier = meModifier;
        }

        public void setMaxRunModifier(float maxRunModifier) {
            this.maxRunModifier = maxRunModifier;
        }

        public void setPeModifier(float peModifier) {
            this.peModifier = peModifier;
        }
    }

    public EveDecryptorInfo getDecryptorInfo(final Integer decryptorTypeId) throws SQLException {
        final EveType decryptorType = getType(decryptorTypeId);

        if (!decryptorCache.containsKey(decryptorTypeId)) {

            loadSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final MutableDecryptorInfo mutableDecryptorInfo = new MutableDecryptorInfo(decryptorType);
                    session.executeQuery("select a.attributeID, a.valueInt, a.valueFloat from dgmTypeAttributes a " +
                            "inner join dgmAttributeTypes t on a.attributeID=t.attributeID " +
                            "where a.typeID=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            switch (rs.getInt(1)) {
                                case ATTRIBUTE_ID_PROBABILITY_MULTIPLIER:
                                    mutableDecryptorInfo.setProbabilityMultiplier(rs.getFloat(3));
                                    break;
                                case ATTRIBUTE_ID_ME_MODIFIER:
                                    mutableDecryptorInfo.setMeModifier(rs.getFloat(3));
                                    break;
                                case ATTRIBUTE_ID_PE_MODIFIER:
                                    mutableDecryptorInfo.setPeModifier(rs.getFloat(3));
                                    break;
                                case ATTRIBUTE_ID_MAX_RUN_MODIFIER:
                                    mutableDecryptorInfo.setMaxRunModifier(rs.getFloat(3));
                                    break;
                                default:
                                    log.warn("Ignored attribute: id=" + rs.getInt(1) + ", intValue=" +rs.getInt(2) + ", floatValue="+rs.getFloat(2));
                            }
                        }
                    }, decryptorTypeId);

                    decryptorCache.put(decryptorTypeId, mutableDecryptorInfo.toEveDecryptorInfo());
                }
            });
        }

        return decryptorCache.get(decryptorTypeId);
    }

    private static interface DbCallback {
        void doInSession(Session session) throws SQLException;
    }

    private static class Holder {
        private static EveDbHelper _instance = new EveDbHelper();
    }

    private final ReentrantLock derbyConnectionLock = new ReentrantLock();

    private final Map<Integer, EveType> typesCache = Collections.synchronizedMap(new HashMap<Integer, EveType>());
    private final Map<Integer, EveBlueprint> blueprintCache = Collections.synchronizedMap(new HashMap<Integer, EveBlueprint>());
    private final Map<Integer, EveGroup> groupsCache = Collections.synchronizedMap(new HashMap<Integer, EveGroup>());
    private final Map<Integer, EveCategory> categoryCache = Collections.synchronizedMap(new HashMap<Integer, EveCategory>());
    private final Map<Integer, List<EveMaterialType>> materialTypesCache = Collections.synchronizedMap(new HashMap<Integer, List<EveMaterialType>>());
    private final Map<Integer, List<EveMaterialType>> extraMaterialTypesCache = Collections.synchronizedMap(new HashMap<Integer, List<EveMaterialType>>());
    private final Map<Integer, List<EveMaterialType>> inventionMaterialTypesCache = Collections.synchronizedMap(new HashMap<Integer, List<EveMaterialType>>());
    private final Map<Integer, EveDecryptorInfo> decryptorCache = Collections.synchronizedMap(new HashMap<Integer, EveDecryptorInfo>());
    private final Map<Integer, EveLocation> locationCache = Collections.synchronizedMap(new HashMap<Integer, EveLocation>());
}
