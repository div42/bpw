/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.helpers.BlueprintHelper;
import org.shadanakar.eve.emt.helpers.Constants;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.utils.PerformanceLogger;

import java.math.BigDecimal;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class LocalDbHelper {
    private static final Log log = LogFactory.getLog(LocalDbHelper.class);
    private static final int CURRENT_DB_VERSION = 9;

    private LocalDbHelper() {}

    public static LocalDbHelper getInstance() {
        return Holder._instance;
    }

    public void initializeDatabase() throws SQLException {
        try (PerformanceLogger ignored = new PerformanceLogger(log, "initializeDatabase")) {
            doSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    // Check if database already initialized
                    boolean databaseInitialized = false;
                    int version = 1;

                    try {
                        session.executeUpdate("create table item (" +
                                "id bigint not null, " +
                                "materialLevel int, " +
                                "productivityLevel int, " +
                                "expanded int not null," +
                                "ignoredContainer int not null default 0, " +
                                "customName varchar(1024), " +
                                "PRIMARY KEY(id) )");
                    } catch (SQLException ex) {
                        if(!DerbyErrorHelper.somethingAlreadyExists(ex)) {
                            throw ex;
                        }
                        databaseInitialized = true;
                    }

                    if (databaseInitialized) {
                        // check version

                        // 1. special case - the version is 1
                        try {
                            session.executeUpdate("create table version (version int not null)");
                            // oops.. no exception... version is 1
                            session.executeUpdate("insert into version (version) values (?)", version);
                        } catch (SQLException ex) {
                            if(!DerbyErrorHelper.somethingAlreadyExists(ex)) {
                                throw ex;
                            }
                            version = session.uniqueResult("select version from version");
                        }

                        if (version < CURRENT_DB_VERSION) {
                            upgradeDb(session, version);
                        }
                    } else {
                        // database is not initialized
                        // create rest of tables
                        try {
                            session.executeUpdate("create table version (version int not null)");
                            session.executeUpdate("insert into version (version) values (?)", CURRENT_DB_VERSION);

                            session.executeUpdate("create table blueprint (" +
                                    "blueprintTypeId bigint not null, " +
                                    "materialLevel int, " +
                                    "productivityLevel int, " +
                                    "runs int, " +
                                    "PRIMARY KEY(blueprintTypeId) )");
                        } catch (SQLException ex) {
                            if(!DerbyErrorHelper.somethingAlreadyExists(ex)) {
                                throw ex;
                            }
                        }

                        try {
                            session.executeUpdate("create table job (" +
                                    "id bigint not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
                                    "itemId bigint not null, " +
                                    "blueprintTypeId int not null, " +
                                    "activityType varchar(64) not null, " +
                                    "runs int not null, " +
                                    "decryptorId int not null default 0, " +
                                    "baseItemTypeId int not null default 0, " +
                                    "PRIMARY KEY(id) )");
                        } catch (SQLException ex) {
                            if(!DerbyErrorHelper.somethingAlreadyExists(ex)) {
                                throw ex;
                            }
                        }
                    }
                }
            });
        }
    }

    private void upgradeDb(Session session, int fromVersion) throws SQLException {
        log.info("Upgrading local db from version " + fromVersion + " to " + CURRENT_DB_VERSION);
        switch (fromVersion) {
            case 1:
                executeWithExceptionCatches(session, "alter table item add column ignoredContainer int not null default 0");
                executeWithExceptionCatches(session, "alter table item add column customName varchar(1024)");
            case 2:
                executeWithExceptionCatches(session,"alter table job add column decryptorId int not null default 0");
            case 4:
                executeWithExceptionCatches(session,"alter table blueprint add column runs int");
            case 5:
                executeWithExceptionCatches(session, "alter table job add column baseItemTypeId int not null default 0");
            case 8:
                executeWithExceptionCatches(session, "alter table job add column runs int not null default 0");
            case 9: // placeholder
            default:
                session.executeUpdate("update version set version=?", CURRENT_DB_VERSION);
        }
    }

    private void executeWithExceptionCatches(Session session, String query) throws SQLException {
        try {
            session.executeUpdate(query);
        } catch (SQLException e) {
            if (!DerbyErrorHelper.somethingAlreadyExists(e)) {
                throw e;
            }
        }
    }

    public LocalItemInfo loadLocalItemInfo(final Long id) throws SQLException {
        if (!itemCache.containsKey(id)) {
            doSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final Session.ResultHolder<LocalItemInfo> res = new Session.ResultHolder<>();
                    session.executeQuery("select id, materialLevel, productivityLevel, expanded, ignoredContainer, customName from item where id=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos = 0;
                            LocalItemInfo itemInfo = new LocalItemInfo(rs.getLong(++pos));
                            itemInfo.setMaterialLevel(getInteger(rs, ++pos));
                            itemInfo.setProductivityLevel(getInteger(rs, ++pos));
                            itemInfo.setExpanded(rs.getBoolean(++pos));
                            itemInfo.setContainerIgnored(rs.getBoolean(++pos));
                            itemInfo.setCustomName(rs.getString(++pos));
                            res.value = itemInfo;
                            log.debug("Loaded item: " + itemInfo.dump());
                        }
                    }, id);

                    // create default item info
                    if (res.value == null) {
                        res.value = new LocalItemInfo(id);
                    }
                    itemCache.put(id, res.value);
                }
            });
        }

        return new LocalItemInfo(itemCache.get(id));
    }

    private Integer getInteger(ResultSet rs, int pos) throws SQLException {
        rs.getObject(pos);
        return rs.wasNull() ? null : rs.getInt(pos);
    }

    public LocalBlueprintInfo loadLocalBlueprintInfo(final Integer blueprintTypeId) throws SQLException {
        if(!blueprintTypeCache.containsKey(blueprintTypeId)) {
            doSafely(new DbCallback() {
                @Override
                public void doInSession(Session session) throws SQLException {
                    final Session.ResultHolder<LocalBlueprintInfo> result = new Session.ResultHolder<>();
                    session.executeQuery("select blueprintTypeId, materialLevel, productivityLevel, runs from blueprint where blueprintTypeId=?", new Session.QueryCallback() {
                        @Override
                        public void processRow(ResultSet rs) throws SQLException {
                            int pos=0;
                            LocalBlueprintInfo info = new LocalBlueprintInfo(rs.getInt(++pos));
                            info.setMaterialLevel(getInteger(rs, ++pos));
                            info.setProductivityLevel(getInteger(rs, ++pos));
                            info.setRuns(getInteger(rs, ++pos));
                            result.value = info;
                        }
                    }, blueprintTypeId);

                    blueprintTypeCache.put(blueprintTypeId, result.value == null ? new LocalBlueprintInfo(blueprintTypeId) : result.value);
                }
            });
        }

        return new LocalBlueprintInfo(blueprintTypeCache.get(blueprintTypeId));
    }

    public void saveLocalItemInfo(final LocalItemInfo item) throws SQLException {
        doSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                log.debug("Saving item: " + item.dump());
                String sql;
                // do not remove this cast... it changes behavior!!1
                //noinspection RedundantCast
                if (0 == (Integer) session.uniqueResult("select count(1) from item where id=?", item.getId())) {
                    sql = "insert into item(materialLevel, productivityLevel, expanded, ignoredContainer, customName, id) values (?, ?, ?, ?, ?, ?)";
                } else {
                    sql = "update item set materialLevel=?, productivityLevel=?, expanded=?, ignoredContainer=?, customName=? where id=?";
                }

                session.executeUpdate(sql, item.getMaterialLevel(), item.getProductivityLevel(), item.isExpanded(), item.isContainerIgnored(), item.getCustomName(), item.getId());

                // copy to cache, only if sql operation was successful
                itemCache.put(item.getId(), new LocalItemInfo(item));
            }
        });
    }

    public void saveLocalBlueprintInfo(final LocalBlueprintInfo blueprintInfo) throws SQLException {
        doSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                String bpSql;
                // do not remove this cast... it changes behavior!!1
                //noinspection RedundantCast
                if (0 == (Integer) session.uniqueResult("select count(1) from blueprint where blueprintTypeId=?", blueprintInfo.getId())) {
                    bpSql = "insert into blueprint(materialLevel, productivityLevel, runs, blueprintTypeId) values (?, ?, ?, ?)";
                } else {
                    bpSql = "update blueprint set materialLevel=?, productivityLevel=?, runs=? where blueprintTypeId=?";
                }

                session.executeUpdate(bpSql, blueprintInfo.getMaterialLevel(), blueprintInfo.getProductivityLevel(), blueprintInfo.getRuns(), blueprintInfo.getId());

                // update cache
                blueprintTypeCache.put(blueprintInfo.getId(), new LocalBlueprintInfo(blueprintInfo));
            }
        });
    }

    public List<JobModel> loadAllJobs() throws SQLException {
        final List<JobModel> result = new ArrayList<>();
        doSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                session.executeQuery("select id, itemId, blueprintTypeId, activityType, runs, decryptorId, baseItemTypeId from job", new Session.QueryCallback() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        int pos = 0;
                        long id = rs.getLong(++pos);
                        long itemId = rs.getLong(++pos);
                        int blueprintTypeId = rs.getInt(++pos);
                        ActivityType type = ActivityType.valueOf(rs.getString(++pos));
                        int runs = rs.getInt(++pos);
                        int decryptorId = rs.getInt(++pos);
                        int baseItemTypeId = rs.getInt(++pos);
                        LocalBlueprintInfo localBlueprintInfo = loadLocalBlueprintInfo(blueprintTypeId);
                        EveType blueprintEveType = EveDbHelper.getInstance().getType(blueprintTypeId);
                        EveDecryptorInfo decryptor = decryptorId == 0 ? null : EveDbHelper.getInstance().getDecryptorInfo(decryptorId);
                        EveType baseItemType = baseItemTypeId == 0 ? null : EveDbHelper.getInstance().getType(baseItemTypeId);
                        JobModel job = new JobModel(id, itemId, blueprintEveType, type,
                                BlueprintHelper.calculateMaterialLevel(localBlueprintInfo, blueprintEveType.getBlueprint(), BlueprintType.Copy).getValue(),
                                BlueprintHelper.calculateProductivityLevel(localBlueprintInfo, blueprintEveType.getBlueprint(), BlueprintType.Copy).getValue(),
                                runs == 0 ? BlueprintHelper.calculateRuns(localBlueprintInfo, blueprintEveType.getBlueprint()) : runs, decryptor, baseItemType);
                        result.add(job);
                    }
                });
            }
        });

        return result;
    }

    public void saveJobs(final List<JobModel> jobsToSave) throws SQLException {
        doSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                for (JobModel jobModel : jobsToSave) {
                    int decryptorId = jobModel.getDecryptor() == null ? 0 : jobModel.getDecryptor().getDecryptorType().getId();
                    int baseItemTypeId = jobModel.getBaseItemType() == null ? 0 : jobModel.getBaseItemType().getId();

                    if (jobModel.getId() != null) {
                        session.executeUpdate("update job itemId=?, blueprintTypeId=?, activityType=?, runs=?, decryptorId=?, baseItemTypeId=? where id=?",
                                jobModel.getItemId(), jobModel.getBlueprintType().getId(), jobModel.getActivityType().name(), decryptorId, baseItemTypeId, jobModel.getId());
                    } else {
                        session.executeUpdate("insert into job (itemId, blueprintTypeId, activityType, runs, decryptorId, baseItemTypeId) values (?, ?, ?, ?, ?, ?)",
                                jobModel.getItemId(), jobModel.getBlueprintType().getId(), jobModel.getActivityType().name(), jobModel.getNumberOfRuns(), decryptorId, baseItemTypeId);
                        BigDecimal jobId = session.uniqueResult("values IDENTITY_VAL_LOCAL()");
                        jobModel.setId(jobId.longValue());
                    }
                }
            }
        });
    }

    public LocalBlueprintInfo loadLocalBlueprintInfoNoException(Integer blueprintTypeId) {
        try {
            return loadLocalBlueprintInfo(blueprintTypeId);
        } catch (SQLException e) {
            log.error("Unable to load blueprint info for " + blueprintTypeId);
            return null;
        }
    }

    public void removeJobs(final List<JobModel> jobsToRemove) throws SQLException {
        doSafely(new DbCallback() {
            @Override
            public void doInSession(Session session) throws SQLException {
                for (JobModel job : jobsToRemove) {
                    if (job.getId() != null) {
                        session.executeUpdate("delete from job where id=?", job.getId());
                    } else {
                        log.warn("Unable to remove job: "+job+" id is null!");
                    }
                }
            }
        });
    }

    private static class Holder {
        private static LocalDbHelper _instance = new LocalDbHelper();
    }

    private void doSafely(DbCallback dbCallback) throws SQLException {
        derbyConnectionLock.lock();
        try {
            try (Session session = new Session(DriverManager.getConnection(Constants.derbyLocalDbConnectionString), false)) {
                dbCallback.doInSession(session);
                session.commit();
            }
        } finally {
            derbyConnectionLock.unlock();
        }
    }

    private static interface DbCallback {
        void doInSession(Session session) throws SQLException;
    }

    private final ReentrantLock derbyConnectionLock = new ReentrantLock();
    private final Map<Long, LocalItemInfo> itemCache = Collections.synchronizedMap(new HashMap<Long, LocalItemInfo>());
    private final Map<Integer, LocalBlueprintInfo> blueprintTypeCache = Collections.synchronizedMap(new HashMap<Integer, LocalBlueprintInfo>());
}
