/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import nu.xom.ParsingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.model.IndustryJobModel;
import org.shadanakar.eve.emt.ui.UserSettings;
import org.shadanakar.eve.emt.ui.model.ApplicationData;
import org.shadanakar.eve.emt.utils.SyncUtils;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.concurrent.locks.ReentrantLock;

public class MarkerOrderLoader {
    private static final Log log = LogFactory.getLog(MarkerOrderLoader.class);
    private static final ReentrantLock lock = new ReentrantLock();
/*
    public static boolean checkAndLoadMarketOrders(final Path pathToFile, String marketOrdersFileName, final ApplicationData applicationData, final ProgressListener progressListener) throws ParsingException, SQLException, IOException {
        UserSettings userSettings = applicationData.getUserSettings();
        final String key = userSettings.getApiKeyInfo().getKey();
        final String vCode = userSettings.getApiKeyInfo().getvCode();
        if (key.isEmpty() || vCode.isEmpty()) {
            log.warn("There is no api key or/and vCode");
            return false;
        }

        final String fileName = marketOrdersFileName + "." + key;
        final Path marketOrdersFilePath = pathToFile.resolve(fileName);

        return SyncUtils.executeLocked(lock, new SyncUtils.Callback<Boolean>() {
            @Override
            public Boolean run() throws ParsingException, SQLException, IOException {
                try {
                    // if industry jobs are not initialized yet - preload them
                    if (!applicationData.getMarketOrders().isInitialized() && Files.exists(marketOrdersFilePath)) {
                        try (Reader reader = new FileReader(marketOrdersFilePath.toFile())) {
                            IndustryJobModel jobModel = loadIndustryJobsFromXml(reader);
                            applicationData.setIndustryJobs(jobModel);
                        } catch (Exception ex) { // at this point it doesn't matter
                            log.warn("Industry job pre-load failed (we can safely ignore it)", ex);
                        }
                    }

                    // lets see if it's still cached
                    if (!isDataUpToDate(applicationData)) {
                        MessageFormat format = new MessageFormat(Constants.apiCallCorpIndustryJobs);
                        String url = format.format(new Object[]{key, vCode});
                        HttpHelper.loadFileFromWeb(url, pathToFile, fileName, progressListener);
                    }

                    boolean updated;
                    try (Reader reader = new FileReader(marketOrdersFilePath.toFile())) {
                        IndustryJobModel jobModel =  loadIndustryJobsFromXml(reader);
                        if (updated = (applicationData.getIndustryJobs() == null || !jobModel.getCurrentTime().equals(applicationData.getIndustryJobs().getCurrentTime()))) {
                            applicationData.setIndustryJobs(jobModel);
                        }
                    }

                    return updated;
                } catch (IOException | ParsingException | SQLException e) {
                    log.error("Load failed", e);
                    throw e;
                }
            }
        });
    }*/
}
