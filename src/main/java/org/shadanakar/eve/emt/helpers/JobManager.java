/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.model.ApplicationData;

import java.sql.SQLException;
import java.util.*;

public class JobManager {
    private static final Log log = LogFactory.getLog(JobManager.class);
    private ApplicationData applicationData;

    public JobManager(ApplicationData applicationData) {
        this.applicationData = applicationData;
    }

    public void addInventionJobsByType(String nodeId, EveDecryptorInfo decryptorInfo, EveType baseItemType) throws SQLException {
        EveItemNode node = applicationData.getAssets().getNodeById(nodeId);
        LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfoNoException(node.getTypeId());
        List<EveItemNode> candidates = applicationData.getAssets().findItemsOfSameType(node, localBlueprintInfo);
        List<JobModel> jobsToSave = applicationData.getPlan().addInventionJobs(candidates, decryptorInfo, baseItemType);
        LocalDbHelper.getInstance().saveJobs(jobsToSave);
    }


    public void addManufacturingJobsByOriginalType(String nodeId, int runs) throws SQLException {
        EveItemNode node = applicationData.getAssets().getNodeById(nodeId);
        LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfoNoException(node.getTypeId());
        List<EveItemNode> original = new ArrayList<>();
        original.add(node);
        List<JobModel> jobsToSave = applicationData.getPlan().addManufacturingJobs(original,
                node.calculateMaterialLevel(localBlueprintInfo).getValue(),
                node.calculateProductivityLevel(localBlueprintInfo).getValue(),
                runs);
        LocalDbHelper.getInstance().saveJobs(jobsToSave);
    }

    public void addManufacturingJobsByCopyType(String nodeId) throws SQLException {
        EveItemNode node = applicationData.getAssets().getNodeById(nodeId);
        LocalBlueprintInfo localBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfoNoException(node.getTypeId());
        List<EveItemNode> candidates = applicationData.getAssets().findItemsOfSameType(node, localBlueprintInfo);
        List<JobModel> jobsToSave = applicationData.getPlan().addManufacturingJobs(candidates,
                node.calculateMaterialLevel(localBlueprintInfo).getValue(),
                node.calculateProductivityLevel(localBlueprintInfo).getValue(),
                node.calculateRuns(localBlueprintInfo));
        LocalDbHelper.getInstance().saveJobs(jobsToSave);
    }

    public void loadJobs() throws SQLException {
        List<JobModel> jobs = LocalDbHelper.getInstance().loadAllJobs();
        PlanModel planModel = new PlanModel();
        planModel.addJobs(jobs);
        applicationData.setPlan(planModel);
    }

    public JobGroup createInventionJobGroup(EveType blueprintType, EveDecryptorInfo selectedDecryptor, EveType selectedBaseItemType) {
        JobModel job = JobModel.createInventionJob(null, -1, blueprintType, selectedDecryptor, selectedBaseItemType);
        return JobGroup.createInventionGroup(Arrays.asList(job));
    }

    public float estimateJobPercentComplete(JobWorkflowItem value, Set<Long> notDeliveredJobIds, float productionTimeHours) {
        float percentComplete = 0;
        Date now = new Date();
        List<EveIndustryJob> jobsByType = applicationData.getIndustryJobs().getJobsByBlueprintTypeId(value.getBlueprintType().getId());
        if (productionTimeHours > 0 && !jobsByType.isEmpty()) {
            float hoursComplete = 0;
            for (EveIndustryJob eveIndustryJob : jobsByType) {
                if (eveIndustryJob.getActivityType() == value.getActivityType()) {
                    if(eveIndustryJob.getBeginProductionTime().before(now)
                            && (applicationData.getPlan().isItemScheduled(eveIndustryJob.getInstalledLocalItemInfo().getId())
                                || notDeliveredJobIds.contains(eveIndustryJob.getId()) || eveIndustryJob.getEndProductionTime().after(applicationData.getAssets().getCurrentTime()) )) {

                        // calculate net hours
                        long millis = Math.min(now.getTime(), eveIndustryJob.getEndProductionTime().getTime());
                        hoursComplete += (millis - eveIndustryJob.getBeginProductionTime().getTime())/(3600000f);
                    }
                }
            }

            if (hoursComplete/productionTimeHours > 1.0f) {
                log.error("hoursComplete>productionTimeHours, it might be because blueprint data for planned job is incorrect!");
            }
            percentComplete = Math.min(hoursComplete/productionTimeHours, 1);
        }
        return percentComplete;
    }

}
