/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import nu.xom.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.db.EveDbHelper;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.utils.PerformanceLogger;
import sun.security.ssl.Debug;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.*;

/**
 * $Id$
 */
public class EveItemLoader {
    private static final Log log = LogFactory.getLog(EveItemLoader.class);

    public static EveItemLoader getInstance() {
        return Holder._instance;
    }

    private static class CallbackImpl implements EveXmlParser.Callback<EveItemNode> {
        private Date currentTime;
        private Date cachedUntil;
        private Map<String, EveItemNode> itemsMap = new HashMap<>();
        private List<EveItemNode> rootItems;


        @Override
        public void started() {
            // nop
        }

        @Override
        public void setDocumentTimestamps(Date currentTime, Date cachedUntil) {
            this.currentTime = currentTime;
            this.cachedUntil = cachedUntil;
        }

        @Override
        public EveItemNode createRowObject(Map<String, String> attributes, boolean hasChildren) throws SQLException {
            EveDbHelper eveDbHelper = EveDbHelper.getInstance();
            LocalDbHelper localDbHelper = LocalDbHelper.getInstance();

            Long itemId = new Long(attributes.get("itemID"));
            Integer typeId = new Integer(attributes.get("typeID"));
            EveType type = eveDbHelper.getType(typeId);
            EveLocation location = null;
            Integer quantity = new Integer(attributes.get("quantity"));
            Integer rawQuantity = null;
            if (attributes.containsKey("rawQuantity")) {
                rawQuantity = new Integer(attributes.get("rawQuantity"));
            }

            if(attributes.containsKey("locationID")) {
                Long locationId = new Long(attributes.get("locationID"));
                // TODO: somehow, sometimes we get very big values for this attribute (investigate)
                if(locationId>Integer.MAX_VALUE || locationId<Integer.MIN_VALUE) {
                    log.warn("Invalid locationId="+locationId+": attributes=" + DebugUtils.dumpMap(attributes));
                } else {
                    location = eveDbHelper.getLocation(locationId.intValue());
                }
            }

            LocalItemInfo localItemInfo = localDbHelper.loadLocalItemInfo(itemId);

            AssetItemInfo assetItemInfo = new AssetItemInfo(itemId, quantity);

            return new EveItemNode(type, location, rawQuantity, Arrays.asList(new EveItemNode.Entry(localItemInfo, assetItemInfo)), hasChildren, Collections.<EveItemNode>emptyList());
        }

        @Override
        public boolean isExpandRowSupportedFor(EveItemNode value) {
            return !value.isContainerIgnored();
        }

        @Override
        public EveItemNode addChildren(EveItemNode value, List<EveItemNode> children) {
            List<EveItemNode> processedNodes = collapseSortAndAddToMap(children);
            return value.setChildren(processedNodes);
        }

        @Override
        public void finished(List<EveItemNode> rootRowsetItems) {
            this.rootItems = collapseSortAndAddToMap(rootRowsetItems);
        }

        private List<EveItemNode> collapseSortAndAddToMap(List<EveItemNode> nodes) {
            List<EveItemNode> result = new ArrayList<>();
            for (EveItemNode node : nodes) {
                if (!node.isNotEmptyContainer()) {
                    final Integer nodeTypeId = node.getTypeId();
                    EveItemNode duplicateNode = Iterables.find(result, new Predicate<EveItemNode>() {
                        @Override
                        public boolean apply(@Nullable org.shadanakar.eve.emt.model.EveItemNode aNode) {
                            return aNode != null && !aNode.isContainer() && aNode.getTypeId().equals(nodeTypeId);
                        }
                    }, null);

                    if (duplicateNode != null) {
                        String nodeId = duplicateNode.getId();
                        itemsMap.remove(nodeId);
                        result.remove(duplicateNode);
                        node = duplicateNode.merge(node);
                    }
                }

                itemsMap.put(node.getId(), node);
                result.add(node);
            }

            Collections.sort(result, new Comparator<EveItemNode>() {
                @Override
                public int compare(EveItemNode one, EveItemNode two) {
                    return one.getType().getName().toLowerCase().compareTo(two.getType().getName().toLowerCase());
                }
            });

            return result;
        }

        private Date getCurrentTime() {
            return currentTime;
        }

        private Date getCachedUntil() {
            return cachedUntil;
        }

        public List<EveItemNode> getRootItems() {
            return rootItems;
        }

        public Map<String,EveItemNode> getItemsMap() {
            return itemsMap;
        }
    }

    public EveAssets loadAssetsFromXml(Reader reader) throws ParsingException, IOException, SQLException {
        try (PerformanceLogger ignored = new PerformanceLogger(log, "loadAssetsFromXml")) {
            EveXmlParser<EveItemNode> parser = new EveXmlParser<>();
            CallbackImpl callback = new CallbackImpl();
            parser.parse(reader, callback);
            return new EveAssets(callback.getCurrentTime(), callback.getCachedUntil(), callback.getRootItems(), callback.getItemsMap());
        }
    }

    private static class Holder {
        private static EveItemLoader _instance = new EveItemLoader();
    }

    private EveItemLoader() {}

}
