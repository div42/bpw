/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import nu.xom.ParsingException;
import org.apache.pivot.util.ListenerList;
import org.apache.pivot.wtk.WTKListenerList;
import org.shadanakar.eve.emt.ui.model.ApplicationData;

import java.io.IOException;
import java.sql.SQLException;

public class MarketDataManager {
    private final ApplicationData applicationData;

    public static interface MarketDataListener {
        void marketDataUpdateStarted();
        void marketDataUpdateFinished();
        void marketDataUpdateFailed(Exception e);
    }

    private static class MarketDataListenerList extends WTKListenerList<MarketDataListener> implements MarketDataListener {
        @Override
        public void marketDataUpdateStarted() {
            for (MarketDataListener listener : this) {
                listener.marketDataUpdateStarted();
            }
        }

        @Override
        public void marketDataUpdateFinished() {
            for (MarketDataListener listener : this) {
                listener.marketDataUpdateFinished();
            }
        }

        @Override
        public void marketDataUpdateFailed(Exception e) {
            for (MarketDataListener listener : this) {
                listener.marketDataUpdateFailed(e);
            }
        }
    }

    private static class MarketDataProgressUpdateListenerList extends WTKListenerList<ProgressListener> implements ProgressListener {
        @Override
        public void progressStarted(long n) {
            for (ProgressListener listener : this) {
                listener.progressStarted(n);
            }
        }

        @Override
        public void progressUpdate(long n) {
            for (ProgressListener listener : this) {
                listener.progressUpdate(n);
            }
        }

        @Override
        public void progressFinished() {
            for (ProgressListener listener : this) {
                listener.progressFinished();
            }
        }

        @Override
        public void progressAborted(Exception e) {
            for (ProgressListener listener : this) {
                listener.progressAborted(e);
            }
        }
    }

    private MarketDataListenerList marketDataListeners = new MarketDataListenerList();
    private MarketDataProgressUpdateListenerList marketDataProgressUpdateListeners = new MarketDataProgressUpdateListenerList();

    public MarketDataManager(ApplicationData applicationData) {
        this.applicationData = applicationData;
    }

    public ListenerList<MarketDataListener> getMarketDataListeners() {
        return marketDataListeners;
    }

    public ListenerList<ProgressListener> getMarketDataProgressUpdateListeners() {
        return marketDataProgressUpdateListeners;
    }

    public void updateMarketData() {
        try {
            marketDataListeners.marketDataUpdateStarted();
            MarketDataLoader.getInstance().checkAndLoadMarketData(Constants.applicationPath, Constants.marketDataFileName, applicationData, marketDataProgressUpdateListeners);
            marketDataListeners.marketDataUpdateFinished();
        } catch (SQLException | IOException | ParsingException e) {
            marketDataListeners.marketDataUpdateFailed(e);
        }
    }
}
