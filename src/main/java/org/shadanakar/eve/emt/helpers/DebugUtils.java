package org.shadanakar.eve.emt.helpers;

import java.util.Map;

/**
 *
 */
public class DebugUtils {
    public static String dumpMap(Map<String, String> attributes) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        boolean first = true;
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            if(!first) {
                sb.append(", ");
            } else {
                first = false;
            }

            sb.append(entry.getKey()).append("=").append(entry.getValue());
        }
        sb.append("]");

        return sb.toString();
    }
}
