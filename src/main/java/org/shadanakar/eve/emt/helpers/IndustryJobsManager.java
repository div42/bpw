/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import nu.xom.ParsingException;
import org.apache.pivot.util.ListenerList;
import org.apache.pivot.wtk.WTKListenerList;
import org.shadanakar.eve.emt.ui.model.ApplicationData;

import java.io.IOException;
import java.sql.SQLException;

public class IndustryJobsManager {
    private final ApplicationData applicationData;

    public static interface IndustryJobsListener {
        void industryJobsUpdateStarted();
        void industryJobsUpdateFinished(boolean updated);
        void industryJobsUpdateFailed(Exception e);
    }

    private static class IndustryJobsListenerList extends WTKListenerList<IndustryJobsListener> implements IndustryJobsListener {
        @Override
        public void industryJobsUpdateStarted() {
            for (IndustryJobsListener listener : this) {
                listener.industryJobsUpdateStarted();
            }
        }

        @Override
        public void industryJobsUpdateFinished(boolean updated) {
            for (IndustryJobsListener listener : this) {
                listener.industryJobsUpdateFinished(updated);
            }
        }

        @Override
        public void industryJobsUpdateFailed(Exception e) {
            for (IndustryJobsListener listener : this) {
                listener.industryJobsUpdateFailed(e);
            }
        }
    }

    private static class IndustryJobsProgressUpdateListenerList extends WTKListenerList<ProgressListener> implements ProgressListener {
        @Override
        public void progressStarted(long n) {
            for (ProgressListener listener : this) {
                listener.progressStarted(n);
            }
        }

        @Override
        public void progressUpdate(long n) {
            for (ProgressListener listener : this) {
                listener.progressUpdate(n);
            }
        }

        @Override
        public void progressFinished() {
            for (ProgressListener listener : this) {
                listener.progressFinished();
            }
        }

        @Override
        public void progressAborted(Exception e) {
            for (ProgressListener listener : this) {
                listener.progressAborted(e);
            }
        }
    }

    private IndustryJobsListenerList industryJobsListeners = new IndustryJobsListenerList();
    private IndustryJobsProgressUpdateListenerList industryJobsProgressUpdateListeners = new IndustryJobsProgressUpdateListenerList();

    public IndustryJobsManager(ApplicationData applicationData) {
        this.applicationData = applicationData;
    }

    public ListenerList<IndustryJobsListener> getIndustryJobsListeners() {
        return industryJobsListeners;
    }

    public ListenerList<ProgressListener> getIndustryJobsProgressUpdateListeners() {
        return industryJobsProgressUpdateListeners;
    }

    /**
     * @return true if jobs are actually updated
     */
    public boolean updateIndustryJobs() throws ParsingException, SQLException, IOException {
        boolean result;
        try {
            industryJobsListeners.industryJobsUpdateStarted();
            result = IndustryJobLoader.checkAndLoadIndustryJobs(Constants.applicationPath, Constants.industryJobsFileName, applicationData, industryJobsProgressUpdateListeners);
            industryJobsListeners.industryJobsUpdateFinished(result);
        } catch (ParsingException|SQLException|IOException e) {
            industryJobsListeners.industryJobsUpdateFailed(e);
            throw e;
        }
        return result;
    }
}
