/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Constants {
    public static final Path homePath = Paths.get(System.getProperty("user.home"));
    public static final Path applicationPath = homePath.resolve(".bpw");
    public static final Path eveDbPath = applicationPath.resolve("bpwDB");
    public static final Path eveItemsPath = applicationPath.resolve("Types");
    public static final Path localDbPath = applicationPath.resolve("localDB");
    public static final Path settingsPath = applicationPath.resolve("user-settings.properties");
    public static final String localAssetFileName = "AssetList.xml";
    public static final String industryJobsFileName = "IndustryJobs.xml";
    public static final String marketDataFileName = "orders.xml";
    public static final String hostingPropertiesFileName = "hosting.properties";
    public static final String eveDbArchiveFileName = "eveDb.tar.gz";
    public static final String typesArchiveFileName = "Types.zip";

//    public static final Path localAssetFilePath = applicationPath.resolve(localAssetFileName);
    public static final Path eveDbArchiveFilePath = applicationPath.resolve(eveDbArchiveFileName);
    public static final Path typesArchiveFilePath = applicationPath.resolve(typesArchiveFileName);

    public static final String derbyEveDbConnectionString = "jdbc:derby:"+eveDbPath.toString()+";create=false;user=me;password=mine";
    public static final String derbyLocalDbConnectionString = "jdbc:derby:"+ localDbPath.toString()+";create=true;user=me;password=mine";

    public static final String apiCallCorpAssets = "https://api.eveonline.com/corp/AssetList.xml.aspx?keyID={0}&vCode={1}";
    public static final String apiCallCorpIndustryJobs = "https://api.eveonline.com/corp/IndustryJobs.xml.aspx?keyID={0}&vCode={1}";
    public static final String apiCallCorpMarketOrders = "https://api.eveonline.com/corp/MarketOrders.xml.aspx?keyID={0}&vCode={1}";

    public static final String hostingPropertiesUrl = "https://googledrive.com/host/0B6sp_U9gLsrneDRKYmpGdGxrdEU/hosting.properties";

    public static final int CATEGORYID_BLUEPRINT = 9;
    public static final int CATEGORYID_DECRYPTOR = 35;

    public static final int ACTIVITYID_MANUFACTURING = 1;
    public static final int ACTIVITYID_INVENTION = 8;
    public static final int GROUPID_DATAINTERFACES = 716;

    // constants for eve-marketdata.com
    public static final String marketDataUrlTemplate = "http://api.eve-marketdata.com/api/item_orders2.xml?char_name=Anessa+Smith&station_ids=60003760&buysell=a&type_ids={0}";
    public static final int STATIONID_JITATRADEHUB = 60003760;
    public static final float MIN_QUANTITY_FOR_PRICE_ESTIMATES = 100;

    public static Set<Integer> ramTypeIds = new HashSet<>(Arrays.asList(11474, 11475, 11476, 11477, 11478, 11479, 11480, 11481, 11482, 11483, 11484, 11485, 11486, 11513, 11514, 24468));

    private Constants() {} // helper class
}
