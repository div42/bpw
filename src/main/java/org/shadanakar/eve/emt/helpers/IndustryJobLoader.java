/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import nu.xom.ParsingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.db.EveDbHelper;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.UserSettings;
import org.shadanakar.eve.emt.ui.model.ApplicationData;
import org.shadanakar.eve.emt.utils.SyncUtils;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class IndustryJobLoader {
    private static final Log log = LogFactory.getLog(IndustryJobLoader.class);
    private static final ReentrantLock lock = new ReentrantLock();

    public static IndustryJobModel loadBackupIndustryJobs(Path pathToFile, String industryJobsFileName, ApplicationData applicationData) throws ParsingException, SQLException, IOException {
        UserSettings userSettings = applicationData.getUserSettings();
        final String key = userSettings.getApiKeyInfo().getKey();
        final String vCode = userSettings.getApiKeyInfo().getvCode();
        if (key.isEmpty() || vCode.isEmpty()) {
            log.warn("There is no api key or/and vCode");
            return null;
        }

        final String fileName = industryJobsFileName + "." + key + ".backup";
        final Path industryJobsBackupFilePath = pathToFile.resolve(fileName);

        return SyncUtils.executeLocked(lock, new SyncUtils.Callback<IndustryJobModel>() {
            @Override
            public IndustryJobModel run() {
                IndustryJobModel result = null;
                if (Files.exists(industryJobsBackupFilePath)) {
                    try(Reader reader = new FileReader(industryJobsBackupFilePath.toFile())) {
                        result = loadIndustryJobsFromXml(reader);
                    } catch (Exception ex) { // at this point it doesn't matter
                        log.warn("Industry job backup load failed", ex);
                    }
                }
                return result;
            }
        });
    }

    public static boolean checkAndLoadIndustryJobs(final Path pathToFile, String industryJobsFileName, final ApplicationData applicationData, final ProgressListener progressListener) throws ParsingException, SQLException, IOException {
        UserSettings userSettings = applicationData.getUserSettings();
        final String key = userSettings.getApiKeyInfo().getKey();
        final String vCode = userSettings.getApiKeyInfo().getvCode();
        if (key.isEmpty() || vCode.isEmpty()) {
            log.warn("There is no api key or/and vCode");
            return false;
        }

        final String fileName = industryJobsFileName + "." + key;
        final Path industryJobsFilePath = pathToFile.resolve(fileName);

        return SyncUtils.executeLocked(lock, new SyncUtils.Callback<Boolean>() {
            @Override
            public Boolean run() throws ParsingException, SQLException, IOException {
                try {
                    // if industry jobs are not initialized yet - preload them
                    if (!applicationData.getIndustryJobs().isInitialized() && Files.exists(industryJobsFilePath)) {
                        try(Reader reader = new FileReader(industryJobsFilePath.toFile())) {
                            IndustryJobModel jobModel = loadIndustryJobsFromXml(reader);
                            applicationData.setIndustryJobs(jobModel);
                        } catch (Exception ex) { // at this point it doesn't matter
                            log.warn("Industry job pre-load failed (we can safely ignore it)", ex);
                        }
                    }

                    // lets see if it's still cached
                    if (!isDataUpToDate(applicationData)) {
                        MessageFormat format = new MessageFormat(Constants.apiCallCorpIndustryJobs);
                        String url = format.format(new Object[]{key, vCode});
                        HttpHelper.loadFileFromWeb(url, pathToFile, fileName, progressListener);
                    }

                    boolean updated;
                    try (Reader reader = new FileReader(industryJobsFilePath.toFile())) {
                        IndustryJobModel jobModel = loadIndustryJobsFromXml(reader);
                        if (updated = (applicationData.getIndustryJobs() == null || !jobModel.getCurrentTime().equals(applicationData.getIndustryJobs().getCurrentTime()))) {
                            applicationData.setIndustryJobs(jobModel);
                        }
                    }

                    return updated;
                } catch (IOException|ParsingException|SQLException e) {
                    log.error("Load failed", e);
                    throw e;
                }
            }
        });
    }

    public static void backupLastIndustryUpdate(Path pathToFile, String industryJobsFileName, ApplicationData applicationData) {
        UserSettings userSettings = applicationData.getUserSettings();
        String key = userSettings.getApiKeyInfo().getKey();
        String vCode = userSettings.getApiKeyInfo().getvCode();
        if (key.isEmpty() || vCode.isEmpty()) {
            log.warn("There is no api key or/and vCode");
            return;
        }

        String fileName = industryJobsFileName + "." + key;
        String backupFileName = fileName + ".backup";
        final Path industryJobsFilePath = pathToFile.resolve(fileName);
        final Path industryJobsBackupFilePath = pathToFile.resolve(backupFileName);

        SyncUtils.executeLocked(lock, new Runnable() {
            @Override
            public void run() {
                if (Files.exists(industryJobsFilePath)) {
                    try {
                        Files.copy(industryJobsFilePath, industryJobsBackupFilePath, StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        log.error("Error while creating backup industry file", e);
                    }
                }
            }
        });
    }

    private static class XmlCallbackImpl extends EveXmlParser.Callback.PlainListAdapter<EveIndustryJob> {
        private Date currentTime;
        private Date cachedUntil;
        private IndustryJobModel result;

        @Override
        public void setDocumentTimestamps(Date currentTime, Date cachedUntil) {
            this.currentTime = currentTime;
            this.cachedUntil = cachedUntil;
        }

        @Override
        public EveIndustryJob createRowObject(Map<String, String> attributes, boolean hasChildren) throws SQLException {
            AttributeTypeConverter atts = new AttributeTypeConverter(attributes);
            // lets do our thing...
            long jobId = atts.getLong("jobID");
            long assemblyLineId = atts.getLong("assemblyLineID");
            long containerId = atts.getLong("containerID");
            long installedItemId = atts.getLong("installedItemID");
            long installedItemLocationId = atts.getLong("installedItemLocationID");
            // installedItemQuantity - ignored
            int installedItemMaterialLevel = atts.getInt("installedItemMaterialLevel");
            int installedItemProductivityLevel = atts.getInt("installedItemProductivityLevel");
            int installedItemLicensedProductionRunsRemaining = atts.getInt("installedItemProductivityLevel");
            long outputLocationId = atts.getLong("outputLocationID");
            long installerId = atts.getLong("installerID");
            int jobRuns = atts.getInt("runs");
            Integer licensedProductionRuns = atts.getOptionalInteger("licensedProductionRuns");
//            int installedInSolarSystemId = atts.getInt("installedInSolarSystemID"); - ignored, not sure how to use that
            // containerLocationID - ignored
            float materialMultiplier = atts.getFloat("materialMultiplier");
            // charMaterialMultiplier - ignored atm
            float timeMultiplier = atts.getFloat("timeMultiplier");
            // charTimeMultiplier - ignored atm
            int installedItemTypeId = atts.getInt("installedItemTypeID");
            int outputTypeId = atts.getInt("outputTypeID");
            int containerTypeId = atts.getInt("containerTypeID");
            boolean installedItemCopy = atts.getBoolean("installedItemCopy");
            boolean completed = atts.getBoolean("completed");
            boolean completedSuccessfully = atts.getBoolean("completedSuccessfully");
            // installedItemFlag - ignored, it is useless since AssetList does not have it.
            // outputFlag - ignored: see above.. need it in AssetList to make use of it.
            int activityId = atts.getInt("activityID");
            int completedStatus = atts.getInt("completedStatus");
            Date installTime = atts.getDate("installTime");
            Date beginProductionTime = atts.getDate("beginProductionTime");
            Date endProductionTime = atts.getDate("endProductionTime");
            Date pauseProductionTime = atts.getDate("pauseProductionTime");

            // this was a long list... now lets do some things to normalize and interconnect with existing data.

            LocalItemInfo installedLocalItemInfo = new LocalItemInfo(installedItemId); // todo: consider loading from db and than modify it

            installedLocalItemInfo.setMaterialLevel(installedItemMaterialLevel);
            installedLocalItemInfo.setProductivityLevel(installedItemProductivityLevel);

//            EveLocation installedItemLocation = EveDbHelper.getInstance().getLocation(installedItemLocationId);

            LocalBlueprintInfo outputBlueprintInfo = null;
            if (activityId == Constants.ACTIVITYID_INVENTION) {
                // ok, this might be useful (unfortunately CCP does not provide itemID, we still can try and follow up on types)
                outputBlueprintInfo = new LocalBlueprintInfo(outputTypeId);
                outputBlueprintInfo.setMaterialLevel((int) materialMultiplier);
                outputBlueprintInfo.setProductivityLevel((int) timeMultiplier);
                outputBlueprintInfo.setRuns(licensedProductionRuns);
            }

            EveType outputType = EveDbHelper.getInstance().getType(outputTypeId);
            EveType installedItemType = EveDbHelper.getInstance().getType(installedItemTypeId);
            EveType containerType = EveDbHelper.getInstance().getType(containerTypeId); // not sure why would we need it... but well. it is there if you do

            EveIndustryJob job = new EveIndustryJob(jobId, assemblyLineId, containerId, installedItemType, installedItemCopy, installedLocalItemInfo, /* installedItemLocation, */
                    installedItemLicensedProductionRunsRemaining, outputLocationId, installerId, jobRuns, licensedProductionRuns, outputType, outputBlueprintInfo, activityId,
                    containerType, completed, completedStatus, completedSuccessfully, installTime, beginProductionTime, endProductionTime, pauseProductionTime);

            if (installedItemLocationId > Integer.MAX_VALUE || installedItemLocationId < Integer.MIN_VALUE) {
                // why? how?
                log.warn("Unexpected value for installedItemLocationId=" + installedItemLocationId + ": attributes=" + DebugUtils.dumpMap(attributes));
            }

            return job;
        }

        @Override
        public void finished(List<EveIndustryJob> rootRowsetItems) {
            // maps.. maps everywhere.
            Map<Integer, List<EveIndustryJob>> listByActivityType = new HashMap<>();
            Map<Long, EveIndustryJob> jobsById = new HashMap<>();
            for (EveIndustryJob eveIndustryJob : rootRowsetItems) {
                jobsById.put(eveIndustryJob.getId(), eveIndustryJob);
                if (!listByActivityType.containsKey(eveIndustryJob.getActivityId())) {
                    listByActivityType.put(eveIndustryJob.getActivityId(), new ArrayList<EveIndustryJob>());
                }

                listByActivityType.get(eveIndustryJob.getActivityId()).add(eveIndustryJob);
            }

            this.result = new IndustryJobModel(currentTime, cachedUntil, jobsById, listByActivityType);
        }

        public IndustryJobModel getResult() {
            return result;
        }
    }

    private static boolean isDataUpToDate(ApplicationData applicationData) {
        return applicationData != null && applicationData.getIndustryJobs() != null && applicationData.getIndustryJobs().getCachedUntil().getTime() > System.currentTimeMillis();
    }

    private static IndustryJobModel loadIndustryJobsFromXml(Reader reader) throws ParsingException, SQLException, IOException {
        EveXmlParser<EveIndustryJob> parser = new EveXmlParser<>();
        XmlCallbackImpl callback = new XmlCallbackImpl();
        parser.parse(reader, callback);
        return callback.getResult();
    }
}
