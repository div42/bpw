/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.model.ApiKeyInfo;
import org.shadanakar.eve.emt.ui.UserSettings;

import java.io.*;
import java.nio.file.Files;

public class UserSettingsHelper {
    private static final Log log = LogFactory.getLog(UserSettingsHelper.class);
    public static final String API_KEY = "api.key";
    public static final String API_VCODE = "api.vcode";
    public static final String TREE_FILTER_INDEX = "tree.filter.index";
    public static final String UPDATE_PAUSED = "update.paused";

    public static void saveSettings(UserSettings settings) {
        try(PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(Constants.settingsPath.toFile())))) {
            // write key
            writer.println(API_KEY + "=" +settings.getApiKeyInfo().getKey());
            writer.println(API_VCODE + "=" +settings.getApiKeyInfo().getvCode());
            writer.println(TREE_FILTER_INDEX + "=" + settings.getFilterIndex());
            writer.println(UPDATE_PAUSED + "=" + settings.isUpdatePaused());
        } catch (IOException e) {
            log.error("Unable to save user settings", e);
        }
    }

    public static UserSettings loadSettings() {
        UserSettings settings = new UserSettings();

        if (Files.exists(Constants.settingsPath)) {
            try(BufferedReader reader = new BufferedReader(new FileReader(Constants.settingsPath.toFile()))) {
                String line;
                while(null != (line = reader.readLine())) {
                    String keyValue[] = line.split("=", 2);
                    if (keyValue.length < 2) {
                        log.warn("Unable to compile setting. line: " + line);
                        continue;
                    }
                    try {
                        String key = keyValue[0];
                        String value = keyValue[1];
                        if (API_KEY.equalsIgnoreCase(key)) {
                            settings.setApiKeyInfo(new ApiKeyInfo(value, settings.getApiKeyInfo().getvCode()));
                        } else if (API_VCODE.equalsIgnoreCase(key)) {
                            settings.setApiKeyInfo(new ApiKeyInfo(settings.getApiKeyInfo().getKey(), value));
                        } else if (TREE_FILTER_INDEX.equalsIgnoreCase(key)) {
                            settings.setFilterIndex(Integer.parseInt(value));
                        } else if (UPDATE_PAUSED.equalsIgnoreCase(key)) {
                            settings.setUpdatePaused(Boolean.parseBoolean(value));
                        }
                    } catch (Exception e) {
                        log.warn("Exception while parsing line: " + line, e);
                    }
                }
            } catch (IOException e) {
                log.error("Error loading user settings from: " + Constants.settingsPath, e);
            }
        }
        return settings;
    }
}
