/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import nu.xom.ParsingException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.db.EveDbHelper;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.model.ApplicationData;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;

public class MarketDataLoader {
    private static final Log log = LogFactory.getLog(MarketDataLoader.class);

    private static class MarketDataXmlCallback extends EveXmlParser.Callback.PlainListAdapter<Void> {
        private long snapshotTimeMillis = System.currentTimeMillis();
        private Map<Integer, TypeMarketData> marketDataMap = new HashMap<>();
        private long totalParsedItemsCount = 0;

        @Override
        public void setDocumentTimestamps(Date currentTime, Date cachedUntil) {
            snapshotTimeMillis = Math.min(snapshotTimeMillis, currentTime.getTime());
        }

        @Override
        public Void createRowObject(Map<String, String> attributes, boolean hasChildren) throws SQLException {
            int typeId = Integer.parseInt(attributes.get("typeID"));
            TypeMarketData typeMarketData = getOrCreateTypeMarketData(marketDataMap, typeId);
            // load line
            String buysell = attributes.get("buysell");
            float price = new BigDecimal(attributes.get("price")).setScale(2, RoundingMode.HALF_EVEN).floatValue();
            long quantity = Long.valueOf(attributes.get("volRemaining"));
            long minVolume = Long.valueOf(attributes.get("minVolume")); // for scam detection... tbd
            typeMarketData.addLine(MarketOrderType.resolve(buysell), price, quantity, minVolume);

            ++totalParsedItemsCount;

            return null;
        }

        private TypeMarketData getOrCreateTypeMarketData(Map<Integer, TypeMarketData> marketDataMap, int typeId) throws SQLException {
            if (!marketDataMap.containsKey(typeId)) {
                EveType type = EveDbHelper.getInstance().getType(typeId);
                marketDataMap.put(typeId, new TypeMarketData(type, MarketData.Source.EVE_MARKETDATA_COM, new Date(snapshotTimeMillis)));
            }

            return marketDataMap.get(typeId);
        }

        public long getSnapshotTimeMillis() {
            return snapshotTimeMillis;
        }

        public Map<Integer,TypeMarketData> getMarketDataMap() {
            return marketDataMap;
        }

        public long getTotalParsedItemsCount() {
            return totalParsedItemsCount;
        }
    }

    public static MarketDataLoader getInstance() { return Holder._instance; }

    public MarketData loadMarketDataFromXmlFiles(ProgressListener progressListener, File... files) throws ParsingException, IOException, SQLException {
        MarketDataXmlCallback callback = new MarketDataXmlCallback();
        try {
            progressListener.progressStarted(files.length);
            EveXmlParser<Void> parser = new EveXmlParser<>("emd");
            for (int i = 0, filesLength = files.length; i < filesLength; i++) {
                File file = files[i];
                try (Reader reader = new FileReader(file)) {
                    log.info("Parsing: " + file);
                    parser.parse(reader, callback);
                }
                progressListener.progressUpdate(i + 1);
            }
        } finally {
            progressListener.progressFinished();
        }

        log.info("Total records in all files: " + callback.getTotalParsedItemsCount());

        return new MarketData(new Date(callback.getSnapshotTimeMillis()), callback.getMarketDataMap());
    }

    private MarketDataLoader () {}

    public void checkAndLoadMarketData(Path marketDataDir, final String filePrefix, ApplicationData data, ProgressListener progressListener)
            throws SQLException, ParsingException, IOException {
        Set<Integer> typeIdsNeeded = data.findAllProductionTypeIds();
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith(filePrefix) && !name.endsWith(".tmp");
            }
        };

        File[] files = marketDataDir.toFile().listFiles(filter);

        int batchNumber = 1;

        if (files.length > 0) { // there are some files....
            Date snapshotTime = new Date(files[0].lastModified());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, -4); // todo unhardcode
            if(snapshotTime.after(cal.getTime())) {
                // load what we have
                MarketData cached = loadMarketDataFromXmlFiles(progressListener, files);
                // now check if there is missing data
                Set<Integer> typeIdsPresent = cached.getMarketDataMap().keySet();
                HashSet<Integer> typeIdsMissing = new HashSet<>(typeIdsNeeded);
                typeIdsMissing.removeAll(typeIdsPresent);

                if(typeIdsMissing.isEmpty()) { // all is well
                    log.info("Market data: Everything is present and fresh enough. Using cached data.");
                    data.setMarketData(cached);
                    return;
                } else {
                    log.info("Market data: These typeIds are not present in the current snapshot: [" + StringUtils.join(typeIdsMissing, ",") + "]... will load");
                    typeIdsNeeded = typeIdsMissing; // load only missing part
                    batchNumber = 1000000 + Collections.min(typeIdsMissing); // this should be unique
                }
            } else {
                // Full reload - remove existing files
                for (File file : files) {
                    //noinspection ResultOfMethodCallIgnored
                    file.delete();
                }
            }
        }

        // batch loading to avoid 10k limit consequences
        final int batchSize = 20;
        final int numberOfBatches = 1+typeIdsNeeded.size()/batchSize;
        int batchNumberNormalized = 0;
        try {
            progressListener.progressStarted(numberOfBatches);
            List<Integer> batch = new ArrayList<>(batchSize);

            for (Integer id : typeIdsNeeded) {
                batch.add(id);
                if (batch.size() == batchSize) {
                    loadMarketBatch(marketDataDir, filePrefix, batchNumber++, batch);
                    batch.clear();
                    progressListener.progressUpdate(++batchNumberNormalized);
                }
            }

            if (!batch.isEmpty()) {
                loadMarketBatch(marketDataDir, filePrefix, batchNumber, batch);
                progressListener.progressUpdate(++batchNumberNormalized);
            }

            data.setMarketData(loadMarketDataFromXmlFiles(progressListener, marketDataDir.toFile().listFiles(filter)));
        } finally {
            progressListener.progressFinished();
        }
    }

    private void loadMarketBatch(Path marketDataDir, String filePrefix, int batchNumber, List<Integer> batch) throws IOException {
        String resultedUrl = MessageFormat.format(Constants.marketDataUrlTemplate, StringUtils.join(batch, ","));
        log.debug("Loading batch number " + batchNumber + " from: " + resultedUrl);
        String fileName = filePrefix + "." + MessageFormat.format("{0,number,00}", batchNumber);

        HttpHelper.loadFileFromWeb(resultedUrl, marketDataDir, fileName, null);
    }

    private static class Holder {
        private static MarketDataLoader _instance = new MarketDataLoader();
    }
}
