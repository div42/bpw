/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class AttributeTypeConverter {
    private static final Log log = LogFactory.getLog(AttributeTypeConverter.class);
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.ENGLISH);

    private final Map<String, String> attributes;

    public AttributeTypeConverter(Map<String, String> attributes) {
        this.attributes = attributes;
    }


    public long getLong(String name) {
        return Long.parseLong(attributes.get(name));
    }

    public int getInt(String name) {
        return Integer.parseInt(attributes.get(name));
    }

    public Integer getOptionalInteger(String name) {
        return attributes.containsKey(name) ? Integer.parseInt(attributes.get(name)) : null;
    }

    public float getFloat(String name) {
        return Float.parseFloat(attributes.get(name));
    }

    public boolean getBoolean(String name) {
        return Integer.parseInt(attributes.get(name)) != 0;
    }

    public Date getDate(String name) {
        String stringValue = attributes.get(name) + " UTC";
        if (stringValue.equals("0001-01-01 00:00:00 UTC")) {
            return null; // it's empty
        }

        try {
            return dateFormat.parse(stringValue);
        } catch (ParseException e) {
            log.error("Date parse error: " + stringValue, e);
            return null;
        }
    }
}
